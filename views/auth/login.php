<!DOCTYPE html>
<html lang="en">
<head>
<title>EHR-Bills</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap-theme.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/custom.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/jquery-ui.css"/>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery.js" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/bootstrap.js" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-ui.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-1.3.2.min.js" language="javascript" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-blink.js" language="javscript"></script>

<script type="text/javascript">
    $(document).ready(function(){
       
      $('#identity').focus();
      $('#show2').hide();
      $('.blink').blink({delay:1000});
        
    });
</script>

<style type="text/css">
    .img {
    position: relative;
    border-radius: 4px;
   
   
}
</style>
</head>
<body >
    <div id="body-div" class="container-fluid">
        <div id="body-bar" class="row" style="padding-top: 10px;padding-bottom: 20px">
            <div class="col-xs-12 col-sm-12 col-md-12 well" id="topbar">
                <span id="welcome_word" class="blink">
                    <?php 
                        
                        echo "EHR BILLS"?>
                </span>
            </div>
        </div>
        <div id="content" class="row">
                <div class="col-xs-12 col-md-offset-4 col-md-offset-4 col-lg-offset-4 col-sm-4 col-md-4 col-lg-4 col-sm-4 col-md-4 col-lg-4" id="display_login">
                    <div style="padding: 20px">
                        <div id="login_into"><img src="<?php echo base_url();?>images/login.jpg" alt="Flower" id="login"/>Users Login</div>
                         <?php 

                            $attributes = array('class' => 'form-horizontal', 'id' => 'myform','role'=>'form');
                            echo form_open('Auth/login/');
                    ?>  
                        <div class="form-group row">
                          <div class="col-sm-offset-4 col-md-offset-5 col-lg-offset-5 col-xs-12 col-sm-8 col-md-6 col-lg-6">
                            <?php echo $message; ?>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="identity" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Username</label>
                          <div class="col-xs-12 col-sm-8 col-md-8">
                            <input type="text" class="form-control" name="identity" id="identity" placeholder="Username" value="<?php echo set_value('identity')?>">
                            <?php echo form_error('identity'); ?>
                          </div>
                        </div>
                        <div class="form-group row">
                          <label for="password" class="col-xs-12 col-sm-4 col-md-4 col-lg-4 control-label">Password</label>
                          <div class="col-xs-12 col-sm-8 col-md-8">
                              <input type="password" class="form-control" name="password" id="password" placeholder="Password"/>
                              <?php echo form_error('password'); ?>
                          </div>
                        </div>
                        <div class="form-group">
                            <div class="col-sm-offset-4 col-md-offset-6 col-lg-offset-6 col-xs-12 col-sm-8 col-md-6 col-lg-6">
                                 <button type="submit" class="btn btn-success">Login</button>
                            </div>
                        </div>
                      <?php echo form_close(); ?>
                    </div>
                </div>
            </div>
            <div id="footer-div">
                <div class="col-xs-4 col-sm-4 col-md-4">

                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">
                    &copy;&nbsp;<?php echo date('Y')?>&nbsp;EHR-Bills.&nbsp;All Rights Reserved

                </div>
                <div class="col-xs-4 col-sm-4 col-md-4">

                </div>
            </div>
   </div>
</body>
</html>
