
<style type="text/css">
div#display_data {
    overflow: scroll;
}
</style>
<div style="padding-top:10px" class="display_content">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<table class="table table-condensed table-hover table-striped table-bordered">
        <thead>
        </thead>
        <tbody>
            <?php 
               
                    $category=$this->Administration_model->groups($profile->group_id);
                    ?>
                    <tr>
                        <td>First Name</td>
                        <td>&nbsp;&nbsp;<?php echo $profile->first_name; ?></td>
                    </tr>
                    <tr>
                       <td>Middle Name</td>
                       <td>&nbsp;&nbsp;<?php echo $profile->middle_name; ?></td> 
                    </tr>
                    <tr>
                        <td>Last Name</td>
                       <td>&nbsp;&nbsp;<?php echo $profile->last_name; ?>
                           
                       </td> 
                    </tr>
                    <tr>
                        <td>Username</td>
                       <td>&nbsp;&nbsp;<?php echo $profile->username; ?></td> 
                    </tr>
                    <tr>
                        <td>User Group</td>
                        <td>&nbsp;&nbsp;<?php echo $category[0]->name; ?></td>
                    </tr>
                    <tr>
                       <td>Mobile Number</td>
                       <td>&nbsp;&nbsp;<?php echo $profile->msisdn; ?>
                       </td> 
                    </tr>
                    <tr>
                      <td>Email Address</td>
                      <td>&nbsp;&nbsp;<?php echo $profile->email; ?></td>  
                    </tr>
        </tbody>
    </table>
</div>
    
</div>
