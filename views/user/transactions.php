<script type="text/javascript">
    $(document).ready(function(){
       
            $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
             $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
            $('a').find('span#excel').css({
                                                color: '#000000'
                                            });
        });
</script>
<div style="text-align: center;padding-top: 20px">
    <?php 
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('User/transactions',$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="institution"></label>
                        <select name="institution" id="institution" class="form-control" >
                            <option value="" >All Institutions</option>
                            <?php foreach($institutions as $key=>$value){ ?>

                            <option value="<?php echo $value->code; ?>" <?php echo ($institution == $value->code )?'selected="selected"':''; ?>><?php echo $value->name; ?></option>

                                <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="sponsor"></label>
                        <select name="sponsor" id="sponsor" class="form-control" >
                                <option value="" >All Sponsors</option>
                                <?php foreach($sponsors as $key=>$value){ ?>

                                <option value="<?php echo $value->sponsorcode; ?>" <?php echo ($sponsor == $value->sponsorcode )?'selected="selected"':''; ?>><?php echo $value->shortname; ?></option>

                                    <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="paymentmode"></label>
                        <select name="paymentmode" id="paymentmode" class="form-control" >
                                <option value="" >All Payment Modes</option>
                                <?php foreach($paymentModes as $key=>$value){ ?>

                                <option value="<?php echo $value->paymentmodecode; ?>" <?php echo ($paymentmode == $value->paymentmodecode )?'selected="selected"':''; ?>><?php echo $value->name; ?></option>

                                    <?php } ?>
                        </select>
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="start"></label>
                        <input type="text" class="form-control" name="start" id="start" placeholder="Start Date" value="<?php echo $start; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="end"></label>
                        <input type="text" class="form-control" name="end" id="end" placeholder="End Date" value="<?php echo $end; ?>" />
                    </div>
                    <div class="form-group">
                       <button type="submit" class="btn btn-success">Search</button> 
                    </div>
        
    <?php echo form_close(); ?>
</div>
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
	<table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Institution</th>
                <th style="text-align:center;">Sponsor</th>
                <th style="text-align:center;">Payment Mode</th>
                <th style="text-align:center;">Service</th>
                <th style="text-align:center;">Transaction Id</th>
                <th style="text-align:center;">Receipt No</th>
                <th style="text-align:center;">Timestamp</th>
                <th style="text-align:center;">Sync Time</th>
                <th style="text-align:center;">Amount</th>
             </tr>
        </thead>
        <tbody>
            <?php if($transactions != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($transactions as $key=>$value){?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->shortname; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->pmode; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->service; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->transactionid; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->receiptno; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->billtimestamp; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->createdon; ?></td>
                        <td style="text-align: right"><?php echo number_format($value->amount, 2); ?>&nbsp;&nbsp;</td>
                    </tr>  
                <?php }
                }else{ ?>
            <tr>
                <td colspan="10" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>     
</div>
   
</div>
