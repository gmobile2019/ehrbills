<!DOCTYPE html>
<html lang="en">
<head>
<title>EHR-Bills</title>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">

<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/bootstrap-theme.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/custom.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo base_url();?>styles/css/jquery-ui.css"/>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery.js" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/bootstrap.js" ></script>
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-ui.js"></script>
<!--<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-1.3.2.min.js" language="javascript" ></script>-->
<script type="text/javascript" src="<?php echo base_url(); ?>styles/js/jquery-blink.js" language="javscript"></script>

<script type="text/javascript">
    $(document).ready(function(){
       
      
//          $(".blink").animate({opacity:0},200,"linear",function(){
//            $(this).animate({opacity:1},200);
//          }); 
       
         $('.blink').blink({delay:1000});
        
    });
</script>

<style type="text/css">
   
</style>
    
</head>
<body>
    <nav class="navbar navbar-default">
        <div class="container-fluid" id="body-div-temp">
          <!-- Brand and toggle get grouped for better mobile display -->
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
              <a class="navbar-brand" href="#"> <img class="logo" alt="HRO" src="<?php echo base_url()."logo/gmobile.jpg" ?>" /></a>
          </div>

          <!-- Collect the nav links, forms, and other content for toggling -->
          <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
            <ul class="nav navbar-nav">
              <li class="active" style="color:#163B04"><?php echo anchor('User/','Home');?></li>
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" style="color:#163B04" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Transactions <span class="caret"></span></a>
                 <ul class="dropdown-menu">
                    <li><?php echo anchor('User/transactions','Transactions');?></li>
                </ul>
              </li>
            </ul>
              
            <ul class="nav navbar-nav navbar-right">
              <li class="dropdown">
                <a href="#" class="dropdown-toggle" style="color:#163B04" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><?php echo $userInfo->first_name.' '.$userInfo->last_name; ?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                    <li><?php echo anchor('User/profile','Profile');?></li>
                    <li><?php echo anchor('Auth/logout','Logout');?></li>
                </ul>
              </li>
            </ul>
          </div><!-- /.navbar-collapse -->
        </div><!-- /.container-fluid -->
      </nav>
      <div class="container-fluid">
            <div id="body-bar" class="row">
                <div class="col-xs-6 col-sm-8 col-md-8" id="topbar">
                    <span id="welcome_word" class="blink">
                        <?php 
                        
                        echo "EHR BILLS"; ?>
                    </span>
                </div>
                <div class="col-xs-6 col-sm-4 col-md-4 pull-right" id="moduleId">
                    <span  >User Module</span>
                </div>
            </div>
            <div class="row">
                <div class="col-xs-4 col-sm-6 col-md-3 col-lg-3 well">
                        <table class="table table-hover table-bordered" style="width:100%">
                        <?php if($side_menu <> null){?>
                        
                                <?php 

                                foreach($side_menu as $key=>$value){ 

                                        ?>
                                <tr>
                                        <td>
                                                <?php echo anchor($value['link'],$value['title']);?>
                                        </td>
                                </tr>
                                <?php } 
                                ?>
                       
                        <?php } 
                                $institutions=$this->Administration_model->institutions();
                                
                                foreach($institutions as $key=>$value){ 
                                    $last_txn=$this->Administration_model->transactions(null,$value->code);
									?>
                                    <tr>
                                        <td><?php echo " $value->name ( ".$last_txn[0]->billtimestamp." )";?></td>
                                    </tr>
                                <?php } ?>
                        </table>
                </div>
                <div class="col-xs-8 col-sm-6 col-md-9 col-lg-9">
                        <div style="padding-left:0px" class="btn-group btn-group-sm">
                                <button type="button" class="btn btn-primary"><?php echo $title; ?></button>
                        </div>
                         <?php $this->load->view($content); ?>
                </div>
            </div>
            <div id="footer-div-temp" class="row">
                <div class="col-xs-12 col-sm-12 col-md-12" >
                    &copy;&nbsp;<?php echo date('Y')?>&nbsp;EHR-Bills.&nbsp;All Rights Reserved

                </div>
            </div>
        </div>
    </div>
</body>
</html>