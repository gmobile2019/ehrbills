
<div class="display_content">
    
    <?php 
                    echo $message;
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open_multipart('Administration/add_invoice_email/'.$id); 
                ?>
       
                
                <div class="form-group row">
                    <label for="email" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Email Address&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="email" id="email" placeholder="Email" value="<?php echo set_value('email',$email[0]->email); ?>"/>
                        <?php echo form_error('email'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="institution" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Institution Id &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <select name="institution" id="institution" class="form-control" >
                        <option></option>
                        <?php foreach($institutions as $key=>$value){ ?>
                        
                        <option value="<?php echo $value->code; ?>" <?php echo ($id != null && trim($value->code) == trim($email[0]->institutioncode))?'selected="selected"':set_select('institution',$value->code); ?>><?php echo $value->name; ?></option>
                        
                            <?php } ?>
                        
                    </select>
                         <?php echo form_error('institution'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
