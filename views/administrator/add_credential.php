
<div style="padding-top:10px;padding-bottom:20px; ">
    <p class="<?php echo $message_class; ?>"><?php echo $message; ?></p>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal', 'id' => 'myform','role'=>'form');
                    echo form_open('Administration/add_credential/'.$id); 
                ?>
                <div class="form-group row">
                    <label for="institution" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Institution&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <select name="institution" id="institution" class="form-control" >
                        <option value="" ></option>
                        <?php foreach($institutions as $key=>$value){ ?>

                        <option value="<?php echo $value->code; ?>" <?php echo ($id <> null && $instituiton == $value->code )?'selected="selected"':set_select('instituiton',$value->code); ?>><?php echo $value->name; ?></option>

                            <?php } ?>
                    </select>
                        <?php echo form_error('instituiton'); ?>
                    </div>
                   
                </div>
                <div class="form-group row">
                    <label for="username" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Username&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="username" class="form-control " name="username" id="username" placeholder="Username" value="<?php echo set_value('username');?>"/>
                        <?php echo form_error('username'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="password" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">New Password&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="password" class="form-control " name="password" id="password" placeholder="New Password"/>
                        <?php echo form_error('password'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="conf_password" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Confirm New Password&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="password" class="form-control " name="conf_password" id="conf_password" placeholder="Confirm New Password"/>
                        <?php echo form_error('conf_password'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Reset Credetials</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
