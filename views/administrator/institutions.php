
<style type="text/css">
div#display_data {
    overflow: scroll;
}
</style>
<div style="text-align: center;padding-top: 20px">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Administration/view_institutions',$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="name"></label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $name; ?>" />
                    </div>
                    <div class="form-group">
                       <button type="submit" class="btn btn-success">Search</button> 
                    </div>
        
    <?php echo form_close(); ?>
</div>
<div style="padding-top:10px" class="row">
    
<div style="padding-top:10px" class="display_content">
<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
	<table class="table table-condensed table-hover table-striped table-bordered">
        <thead>
            <tr>
                <th>S/No</th>
                <th>Institution Id</th>
                <th>Name</th>
                <th>Phone</th>
                <th>Email</th>
                <th>Logo</th>
            </tr>
        </thead>
        <tbody>
                <?php if($institutions != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($institutions as $key=>$value){
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->code; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->phone; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->email; ?></td>
                        <td>&nbsp;&nbsp;<img height='50px' width="100px" src="<?php echo base_url().'logo/'.$value->logoname ?>" alt="LogoName"/></td>
                    </tr>  
                <?php }
                }else{ ?>
            <tr>
                <td colspan="6" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
</div>
    
</div>
</div>
