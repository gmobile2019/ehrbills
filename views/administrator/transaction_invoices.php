<script type="text/javascript">
    $(document).ready(function(){
       
            $("#start").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
            $("#end").datepicker({
                changeMonth: true,
                changeYear: true,
                dateFormat: 'yy-mm-dd'
            });
            
             $('a').find('span#pdf').css({
                                                color: '#000000'
                                            });
            $('a').find('span#excel').css({
                                                color: '#000000'
                                            });
        });
</script>
<div class="display_content">
    <?php echo $message; ?>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Administration/transaction_invoices/'); 
                ?>
                <div class="form-group row">
                    <label for="start" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Start Date&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="start" id="start" placeholder="Start Date" value="<?php echo set_value('start'); ?>" />
                        <?php echo form_error('start'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="end" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">End Date&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="end" id="end" placeholder="End Date" value="<?php echo set_value('end'); ?>" />
                        <?php echo form_error('end'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="institution" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Institution &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                       <select name="institution" id="institution" class="form-control" >
                            <option value="" >All Institutions</option>
                            <?php foreach($institutions as $key=>$value){ ?>

                            <option value="<?php echo $value->code; ?>" <?php echo set_select('institution', $value->code); ?>><?php echo $value->name; ?></option>

                                <?php } ?>
                        </select>
                         <?php echo form_error('institution'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Generate Invoice</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
