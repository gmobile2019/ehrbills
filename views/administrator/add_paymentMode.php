
<div class="display_content">
    <?php echo $message; ?>
    <?php 
    
                    $attributes = array('class' => 'form-horizontal','id'=>'myform','role'=>'form');
                    echo form_open('Administration/add_paymentMode/'.$id); 
                ?>
       
                <div class="form-group row">
                    <label for="paymentmodecode" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Payment Mode Code &nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="paymentmodecode" id="paymentmodecode" placeholder="Payment Mode Code" value="<?php echo $id != null?$paymentMode[0]->paymentmodecode:set_value('paymentmodecode'); ?>" />
                        <?php echo form_error('paymentmodecode'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Name&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <input type="text" class="form-control" name="name" id="name" placeholder="Name" value="<?php echo $id != null?$paymentMode[0]->name:set_value('name'); ?>" />
                        <?php echo form_error('name'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="sponsor" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Sponsor&nbsp;&nbsp;<span class="form_mandatory">*</span></label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <select name="sponsor" id="sponsor" class="form-control" >
                            <option></option>
                            <?php foreach($sponsors as $key=>$value){ ?>

                            <option value="<?php echo $value->sponsorcode; ?>" <?php echo ($id != null && trim($value->sponsorcode) == trim($paymentMode[0]->sponsor_id))?'selected="selected"':set_select('sponsor',$value->sponsorcode); ?>><?php echo $value->shortname; ?></option>

                            <?php } ?>
                        
                        </select>
                        <?php echo form_error('sponsor'); ?>
                    </div>
                </div>
                <div class="form-group row">
                    <label for="description" class="col-xs-12 col-sm-3 col-md-3 col-lg-3 control-label">Description</label>
                    <div class="col-xs-12 col-sm-8 col-md-3 col-lg-3">
                        <textarea type="text" class="form-control" name="description" id="description" ><?php echo $id != null?$paymentMode[0]->description:set_value('description'); ?></textarea>
                        <?php echo form_error('description'); ?>
                    </div>
                </div>
                <div class="form-group register_width_padding">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-success">Save</button>
                    </div>
                </div>
        
        <?php echo form_close(); ?>        
</div>
