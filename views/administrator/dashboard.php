<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
	<table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Institution</th>
                <th style="text-align:center;">Last Month<br/>(collections/transactions count)</th>
                <th style="text-align:center;">This Month<br/>(collections/transactions count)</th>
                <th style="text-align:center;">Today<br/>(collections/transactions count)</th>
             </tr>
        </thead>
        <tbody>
            <?php if($summary != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($summary as $key=>$value){?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value['name']; ?></td>
                        <td>&nbsp;&nbsp;<?php echo number_format($value['l_month_col'],2).' / '.$value['l_month_txn']; ?></td>
                        <td>&nbsp;&nbsp;<?php echo number_format($value['c_month_col'],2).' / '.$value['c_month_txn']; ?></td>
                        <td>&nbsp;&nbsp;<?php echo number_format($value['today_col'],2).' / '.$value['today_txn']; ?></td>
                    </tr>  
                <?php }
                }else{ ?>
            <tr>
                <td colspan="5" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>     
</div>
</div>
