
<div style="text-align: center;padding-top: 20px">
    <?php 
    
                    $attributes = array('class' => 'form-inline','role'=>'form');
                    echo form_open('Administration/view_users',$attributes); 
                ?>
                    <div class="form-group">
                        <label class="sr-only" for="name"></label>
                        <input type="text" class="form-control" name="name" id="name" placeholder="First/Middle/Last" value="<?php echo $name; ?>" />
                    </div>
                    <div class="form-group">
                        <label class="sr-only" for="group"></label>
                        <select name="group" id="group" class="form-control" >
                                <option></option>
                            <?php foreach($groups as $key=>$value){ ?>

                            <option value="<?php echo $value->id; ?>" <?php echo $group == $value->id?'selected="selected"':""; ?>><?php echo $value->name; ?></option>

                                <?php } ?>
                          </select>
                    </div>
                    <div class="form-group">
                       <button type="submit" class="btn btn-success">Search</button> 
                    </div>
        
    <?php echo form_close(); ?>
</div>
<div style="padding-top:10px" class="row">
 <div class="col-xs-12 col-sm-12 col-md-12 col-lg-12 display_content">
	<table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;">S/NO</th>
                <th style="text-align:center;">Full name</th>
                <th style="text-align:center;">Category</th>
                <th style="text-align:center;">Mobile</th>
                <th style="text-align:center;">Email</th>
                <th style="text-align:center;">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($members != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($members as $key=>$value){
                    $category=$this->Administration_model->groups($value->group_id);
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->first_name.' '.$value->middle_name.' '.$value->last_name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $category[0]->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->msisdn; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->email; ?></td>
                        <?php 
                        
                        $active_status=$value->active == 1?'<span class="glyphicon glyphicon-minus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Deactivate"></span>':'<span class="glyphicon glyphicon-plus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Activate"></span>'; 
                        
                        ?>
                        <td>&nbsp;&nbsp;
                            <?php echo anchor('Administration/create_users/'.$value->id,'<span class="glyphicon glyphicon-pencil" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></span>'); ?>
                            &nbsp;&nbsp;
                            <?php echo anchor('Administration/activate_deactivate_users/'.$value->id.'/'.$value->active,$active_status); ?></td>
                    </tr>  
                <?php }
                }else{ ?>
            <tr>
                <td colspan="6" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?>
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>
</div>
    
</div>
