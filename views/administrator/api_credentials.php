
<div style="padding-left:0px;padding-top: 20px" >
    <?php 
    
                    $attributes = array('class' => 'form-inline custom','role'=>'form','style'=>'padding-left:200px');
                    echo form_open('Administration/access_credentials',$attributes); 
                ?>
        <div class="form-group">
            <div class="form-group">
                <label class="sr-only" for="instituiton"></label>
                <select name="instituiton" id="instituiton" class="form-control" >
                    <option value="" ></option>
                    <?php foreach($institutions as $key=>$value){ ?>

                    <option value="<?php echo $value->code; ?>" <?php echo ($institution == $value->code )?'selected="selected"':''; ?>><?php echo $value->name; ?></option>

                        <?php } ?>
                </select>
           </div>
        </div>
    <button type="submit" class="btn btn-success">Search</button>
        
    <?php echo form_close(); ?>
</div>
<div style="padding-top:10px">
    <table class="table table-condensed table-hover table-striped table-bordered">

        <thead>
            <tr>
                <th style="text-align:center;width:50px">S/NO</th>
                <th style="text-align:center;width:550px">Institution</th>
                <th style="text-align:center;width:300px">Key</th>
                <th style="text-align:center;width:200px">Username</th>
                <th style="text-align:center;width:300px">Status</th>
                <th style="text-align:center;width:300px">Action</th>
             </tr>
        </thead>
        <tbody>
            <?php if($credentials != null){
                
                if($per_page == null){
                        $i=1;
                    }else{
                        $i=$per_page+1;
                    }
                foreach($credentials as $key=>$value){
                    ?>
                    <tr>
                        <td>&nbsp;&nbsp;<?php echo $i++; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->name; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->accesskey; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->username; ?></td>
                        <td>&nbsp;&nbsp;<?php echo $value->status == 1?'Active':'Inactive'; ?></td>
                        <td style="text-align: center">
                            <?php 
                            $active_status=$value->status == 1?'<span class="glyphicon glyphicon-minus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Deactivate"></span>':
                                                                '<span class="glyphicon glyphicon-plus" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Activate"></span>';
                            $edit=anchor('Administration/add_credential/'.$value->id,'<span class="glyphicon glyphicon-edit" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Edit"></span>');
                            $regenerate=anchor('Administration/regenerate_access_key/'.$value->id,'<span class="glyphicon glyphicon-refresh" aria-hidden="true" data-toggle="tooltip" data-placement="top" title="Regenerate Token"></span>');
                            $act_dectivate=anchor('Administration/activate_deactivate_api_access/'.$value->id.'/'.$value->status,$active_status);
                            
                            echo $edit."&nbsp;&nbsp;&nbsp;".$regenerate."&nbsp;&nbsp;&nbsp;".$act_dectivate;
                            ?>
                        </td>
                       </tr>  
                <?php }
                }else{ ?>
            <tr>
                <td colspan="6" style="text-align:center"> NO DATA FOUND</td>
            </tr>  
                <?php } ?> 
        </tbody>
    </table>
    <div align="center">
        <?php echo $links; ?>
    </div>
</div>
