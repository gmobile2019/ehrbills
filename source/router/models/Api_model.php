<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Api_model extends CI_Model
{
   
    private $main;
    
    public function __construct()
    {
            parent::__construct();
            $this->main=$this->load->database('main',TRUE);//load main database configuration
            
    }
    
    function check_access_credentials($accesscode,$timekey,$username,$password){
        
        $accesskey=$this->Administration_model->api_access_key($accesscode,$timekey);
        
        $data=$this->main->query("SELECT id FROM api_credentials WHERE accesskey='$accesskey' AND username='$username' AND password='$password'")->row();
        
        if($data <> null){
            
            return TRUE;
        }
        
        return FALSE;
    }
    
    
    function get_response_details($errorcode){
        
        return $this->main->query("SELECT id,code,status,description FROM api_response_codes WHERE code='$errorcode'")->row();
    }
    
    function save_incoming_request($request){
        
        return $this->main->insert('api_requests',array('request'=>$request));
    }
    
	function check_transaction($transactionid){
        
        if($transactionid <> null){
            
           $where .=" AND transactionid ='$transactionid' "; 
        }
        
         
        $check=$this->main->query("SELECT transactionid FROM bills WHERE id is not null $where")->row();
        
        return $check <> null?TRUE:FALSE;
    }
	
	function check_receipt_transaction($transactionid){
        
        if($transactionid <> null){
            
           $where .=" AND transactionid ='$transactionid' "; 
        }
        
         
        $check=$this->main->query("SELECT transactionid FROM cancelled_receipts WHERE id is not null $where")->row();
        
        return $check <> null?TRUE:FALSE;
    }
	
    function save_transaction($txn){
        
		//$txn['service']=$this->main->escape($txn['service']);
        return $this->main->insert('bills',$txn);
    }
	
	function save_receipt_transaction($txn){
        
        $ref_txn=$this->transactions(null,null, null, null, null, null, $txn['transactionid']);
        
        if($ref_txn == null){
            
            return FALSE;
        }
        
        $txn['billtimestamp']=$ref_txn->billtimestamp;
        $txn['sponsor']=$ref_txn->sponsor;
        $txn['paymentmode']=$ref_txn->paymentmode;
        $txn['amount']=$ref_txn->amount;
        
        $sve=$this->main->insert('cancelled_receipts',$txn);
        
        if($sve){
            
            return $this->main->delete('bills',array('id'=>$ref_txn->id));
        }
    }
	
	function save_data($data){
        
        return $this->main->insert('invoice_monitor',$data);
    }
    
    function pending_invoices(){
        
        return $this->main->query("SELECT id,startdate,enddate,status,institution FROM invoice_monitor WHERE status='2'")->result();
    }
    
    function update_status($id,$document){
        
        return $this->main->update('invoice_monitor',array('status'=>1,'document'=>$document),array('id'=>$id));
    }
    
    function transactions($id,$institution,$sponsor,$paymentmode,$start,$end,$transactionid){
        if($id <> null){
            
           $where .=" AND b.id='$id' "; 
        }
        
       if($institution <> null){
            
           $where .=" AND b.institutioncode='$institution' "; 
        }
        
        if($sponsor <> null){
            
           $where .=" AND b.sponsor='$sponsor' "; 
        }
        
        if($paymentmode <> null){
            
           $where .=" AND b.paymentmode='$paymentmode' "; 
        }
        
        if($start <> null){
            
           $where .=" AND b.billtimestamp >='$start 00:00:00' "; 
        }
        
        if($end <> null){
            
           $where .=" AND b.billtimestamp <='$end 23:59:59' "; 
        }
        
        if($transactionid <> null){
            
           $where .=" AND b.transactionid ='$transactionid' "; 
        }
       
        return $this->main->query("SELECT b.id,b.institutioncode,"
                . "b.sponsor,b.paymentmode,b.billtimestamp,b.createdon,b.amount,"
                . "b.service,b.transactionid,b.receiptno,s.shortname,p.name as pmode ,i.name "
                . "FROM bills as b "
                . "INNER JOIN sponsors as s ON s.sponsorcode=b.sponsor "
                . "INNER JOIN payment_modes as p ON p.paymentmodecode=b.paymentmode "
                . "INNER JOIN institutions as i ON i.code=b.institutioncode WHERE b.id is not null $where "
                . "ORDER BY b.billtimestamp DESC")->row();
    }
        
}