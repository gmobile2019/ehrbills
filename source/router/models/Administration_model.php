<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class Administration_model extends CI_Model
{
   
    private $main;
    
    public function __construct()
    {
            parent::__construct();
            $this->main=$this->load->database('main',TRUE);//load main database configuration
            $this->store_salt      = $this->config->item('store_salt', 'ion_auth');
            $this->salt_length     = $this->config->item('salt_length', 'ion_auth');
    }
    
    function current_user_info(){

           return $this->main->query("SELECT users.first_name,users.middle_name,users.last_name "
                   . "FROM users "
                   . "WHERE users.id='".$this->session->userdata('user_id')."'")->row();
    }
    
    function profile_data(){
        
        return $this->main->query("SELECT users.id,users.password,users.salt,users.first_name,users.middle_name,"
                . "users.last_name,users.username,users.email,"
                . "users.msisdn,users_groups.group_id "
                . "FROM users INNER JOIN users_groups "
                . "ON users.id=users_groups.user_id "
                . "WHERE users.id='".$this->session->userdata('user_id')."'")->row();
    }
    
    function member_info_count($name,$group){
        
        if($name <> null){
           $where .=" AND (users.first_name LIKE '%$name%' OR users.middle_name LIKE '%$name%' OR users.last_name LIKE '%$name%')"; 
        }
        
        if($group <> null){
            
            $where .=" AND users_groups.group_id='$group'"; 
        }
        
        return count($this->main->query("SELECT users.id,users.first_name,"
                . "users.middle_name,users.last_name,users.email,"
                . "users.msisdn,users.username,users_groups.group_id "
                . "FROM users INNER JOIN users_groups "
                . "ON users.id=users_groups.user_id $where "
                . "ORDER BY users.first_name ASC")->result());
    }
    
    function member_info($name,$group,$page,$limit){
        
        if($name <> null){
           $where .=" AND (users.first_name LIKE '%$name%' OR users.middle_name LIKE '%$name%' OR users.last_name LIKE '%$name%')"; 
        }
        
        if($group <> null){
            
            $where .=" AND users_groups.group_id='$group'"; 
        }
        
        return $this->main->query("SELECT users.id,users.first_name,"
                . "users.middle_name,users.last_name,users.email,"
                . "users.msisdn,users.username,users_groups.group_id,users.active "
                . "FROM users INNER JOIN users_groups "
                . "ON users.id=users_groups.user_id $where "
                . "ORDER BY users.first_name ASC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function get_member_info($id){
        
        if($id <> null){
            
            $where =" WHERE users.id='$id'";
            
        }
       
        return $this->main->query("SELECT users.id,users.first_name,"
                . "users.middle_name,users.last_name,users.email,"
                . "users.msisdn,users.username,users_groups.group_id,users.active "
                . "FROM users INNER JOIN users_groups "
                . "ON users.id=users_groups.user_id $where "
                . "ORDER BY users.first_name ASC ")->result();
    }
    
    function system_user_registration($user,$user_group,$id,$password){
        
       if($password <> null){
           $salt= $this->store_salt ? $this->salt() : FALSE;
       
            

            $password= $this->hash_password($password, $salt);
            
            $user['password']=$password;
            $user['salt']=$salt;
       }
            
      if($id == null){
          
          $user['createdon']=date('Y-m-d H:i:s');
          $user['createdby']=$this->session->userdata('user_id');
          $usr=$this->main->insert('users',$user);
          
          if($user){
             
              $id=$this->main->insert_id();
              $user_group['user_id']=$id;
              return $this->main->insert('users_groups',$user_group);
          }
          return FALSE;
      } 
              $user['modifiedon']=date('Y-m-d H:i:s');
              $user['modifiedby']=$this->session->userdata('user_id');
              
              $this->main->update('users',$user,array('id'=>$id));
       return $this->main->update('users_groups',$user_group,array('user_id'=>$id));
    }
    
    function groups($id){
        
        if($id <> null){
            $where=" WHERE id='$id'";
        }
        
        return $this->main->query("SELECT id,name,description FROM groups $where")->result();
    }
    
    function get_registration_groups($group_id){
        
        if($group_id == 1){
            $where =" WHERE id IN ('1','2')";
        }
        
        if($group_id == 2){
            
            $where =" WHERE id NOT IN ('1','2')";
        }
        
        return $this->main->query("SELECT id,name FROM groups $where ORDER BY name ASC")->result();
    }
    
    function change_password($password){
        
        $salt=$this->profile_data();
        $password=$this->hash_password($password,$salt->salt);
        
        return $this->main->update('users',array('password'=>$password),array('id'=>$this->session->userdata('user_id')));
    }
    
    public function salt()
    {
            return substr(sha1(uniqid(rand(), true)), 0, $this->salt_length);
    }
    
    function hash_password($password, $salt=false)
    {
        
           if ($this->store_salt && $salt)
            {
               return  sha1($password . $salt);
            }
            else
            {
                
                    $salt = $this->salt();
                    return  $salt.substr(sha1($salt.$password), 0, -$this->salt_length);
            }
    }
    
    function hash_db_password($password,$hash_password_db){
       
        if ($this->store_salt)
		{
			return sha1($password .$hash_password_db->salt);
		}
		else
		{
			$salt = substr($hash_password_db->password,0,$this->salt_length);
                return  $salt.substr(sha1($salt.$password), 0, -$this->salt_length);
		}
    }
        
    function check_username($username,$id){
        
        if($id <> null){
            
            $usernames=$this->main->query("SELECT COUNT(id) username_count FROM users WHERE username LIKE '$username' AND id<>'$id'")->row();
        }else{
            
            $usernames=$this->main->query("SELECT COUNT(id) username_count FROM users WHERE username LIKE '$username'")->row();
        }
        
        if($usernames->username_count > 0){
            
            return FALSE;
        }
        
        return TRUE;
        
    }
    
    function activate_deactivate_users($id,$status){
        $new_status=$status == 1?0:1;
        
        return $this->main->update('users',array('active'=>$new_status),array('id'=>$id));
    }
    
    function save_institution($data){
        
        return $this->main->insert('institutions',$data);
    }
    
    function institution_info_count($name){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        
        
        return count($this->main->query("SELECT id,code,"
                . "name,phone,email,postal,website,city,logoname "
                . "FROM institutions WHERE id is not null $where "
                . "ORDER BY code ASC")->result());
    }
    
    function institution_info($name,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        
        return $this->main->query("SELECT id,code,"
                . "name,phone,email,postal,website,city,logoname "
                . "FROM institutions WHERE id is not null $where "
                . "ORDER BY code ASC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function institutions($id,$code,$auto_invoice){
        
        if($id <> null){
            
            $where .=" AND id='$id'";
        }
        
        if($code <> null){
            
            $where .=" AND code='$code'";
        }
        
		if($auto_invoice <> null){
            
            $where .=" AND auto_invoice='1'";
        }
		
        return $this->main->query("SELECT id,code,"
                        . "name,phone,email,postal,website,city,logoname,logostring "
                        . "FROM institutions WHERE id is not null $where "
                        . "ORDER BY code ASC")->result();
    }
    
    function sponsor_info_count($name){
        
        if($name <> null){
            
           $where .=" AND (shortname LIKE '%$name%' OR fullname LIKE '%$name%') "; 
        }
        
        
        
        return count($this->main->query("SELECT id,shortname,"
                . "fullname,sponsorcode,description,status "
                . "FROM sponsors WHERE id !=0 $where "
                . "ORDER BY fullname ASC")->result());
    }
    
    function sponsor_info($name,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND (shortname LIKE '%$name%' OR fullname LIKE '%$name%') "; 
        }
        
        
        return $this->main->query("SELECT id,shortname,"
                . "fullname,sponsorcode,description,status "
                . "FROM sponsors WHERE id !=0 $where "
                . "ORDER BY fullname ASC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function sponsors($id){
        
        if($id <> null){
            $where =" WHERE id='$id'";
        }
        
        return $this->main->query("SELECT id,shortname,"
                                . "fullname,sponsorcode,description,status "
                                . "FROM sponsors $where "
                                . "ORDER BY fullname ASC")->result();
    }
    
    function get_sponsor_by_code($code){
        
        return $this->main->query("SELECT id,shortname,"
                                . "fullname,sponsorcode,description,status "
                                . "FROM sponsors WHERE sponsorcode='$code' "
                                . "ORDER BY fullname ASC")->result();
    }
    
    function paymentMode_info_count($name,$sponsor){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        if($sponsor <> null){
            
           $where .=" AND sponsor_id='$sponsor' "; 
        }
        
        return count($this->main->query("SELECT id,name,"
                . "paymentmodecode,sponsor_id,description,status "
                . "FROM payment_modes WHERE id !=0 $where "
                . "ORDER BY name ASC")->result());
    }
    
    function paymentMode_info($name,$sponsor,$page,$limit){
        
        if($name <> null){
            
           $where .=" AND (name LIKE '%$name%') "; 
        }
        
        if($sponsor <> null){
            
           $where .=" AND sponsor_id='$sponsor' "; 
        }
        
        return $this->main->query("SELECT id,name,"
                . "paymentmodecode,sponsor_id,description,status "
                . "FROM payment_modes WHERE id !=0 $where "
                . "ORDER BY name ASC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function paymentModes($id){
        
        if($id <> null){
            $where =" WHERE id='$id'";
        }
        
        return $this->main->query("SELECT id,name,"
                                . "paymentmodecode,sponsor_id,description,status "
                                . "FROM payment_modes $where "
                                . "ORDER BY name ASC")->result();
    }
    
     function add_credential($array,$id){
        
         if($id <> null){
             
             $array['modifiedby']=$this->session->userdata('user_id');
             $array['modifiedon']=date('Y-m-d H:i:s');
             return $this->main->update('api_credentials',$array,array('id'=>$id));
         }
          $array['accesskey']=$this->api_access_key($array['institutioncode'],$array['timekey']);
          $array['createdby']=$this->session->userdata('user_id');
        return $this->main->insert('api_credentials',$array);
    }
    
    function reset_access_key($array,$id){
        
        return $this->main->update('api_credentials',$array,array('id'=>$id));
    }
    
    function activate_deactivate_api_access($id,$status){
        $new_status=$status == 1?0:1;
        
        return $this->main->update('api_credentials',array('status'=>$new_status),array('id'=>$id));
    }
    
    function access_credentials_count($institutioncode){
        
        if($institutioncode <> null){
            
            $where .=" AND api.institutioncode='$institutioncode'";
        }
        
       
        return count($this->main->query("SELECT api.id,api.institutioncode,api.accesskey,api.username,api.password,"
                . "api.status,ist.name "
                . "FROM api_credentials as api "
                . "INNER JOIN institutions AS ist "
                . "ON api.institutioncode=ist.code "
                . "$where "
                . "ORDER BY ist.code ASC")->result());
    }
    
    function access_credentials($institutioncode,$page,$limit){
       
      if($institutioncode <> null){
            
            $where .=" AND api.institutioncode='$institutioncode'";
        }
         
        return $this->db->query("SELECT api.id,api.institutioncode,api.accesskey,api.username,api.password,"
                . "api.status,ist.name "
                . "FROM api_credentials as api "
                . "INNER JOIN institutions AS ist "
                . "ON api.institutioncode=ist.code "
                . "$where "
                . "ORDER BY ist.code ASC "
                . "LIMIT $page,$limit")->result();
    }
    
    function access_credential($id,$institutioncode){
       
      if($id <> null){
            
            $where .=" AND api.id='$id'";
        }
        
        if($institutioncode <> null){
            
            $where .=" AND api.institutioncode='$institutioncode'";
        }
        
        return $this->db->query("SELECT api.id,api.institutioncode,api.accesskey,api.username,"
                . "api.password,api.timekey,api.accesskey,"
                . "api.status,ist.name "
                . "FROM api_credentials as api "
                . "INNER JOIN institutions AS ist "
                . "ON api.institutioncode=ist.code "
                . "$where "
                . "ORDER BY ist.code ASC")->result();
    }
    
    function api_access_key($institutioncode,$timekey){
        
        return md5($institutioncode.$timekey.$this->config->item('api_salt'));
    }
    
	function invoice_emails_info_count($email,$institution){
        
        if($email <> null){
            
            $where .=" AND email LIKE '%$email%'";
        }
        
        if($institution <> null){
            
            $where .=" AND institutioncode='$institution'";
        }
        
        
        
        return count($this->main->query("SELECT id,email,"
                                . "institutioncode,status,createdby,createdon,"
                                . "modifiedby,modifiedon "
                                . "FROM invoice_emails WHERE id is not null $where")->result());
    }
    
    function invoice_emails_info($email,$institution,$page,$limit){
        
        if($email <> null){
            
            $where .=" AND email LIKE '%$email%'";
        }
        
        if($institution <> null){
            
            $where .=" AND institutioncode='$institution'";
        }
        
        
        return $this->main->query("SELECT id,email,"
                                . "institutioncode,status,createdby,createdon,"
                                . "modifiedby,modifiedon "
                                . "FROM invoice_emails WHERE id is not null $where "
                                . "ORDER BY institutioncode,email ASC "
                                . "LIMIT $page,$limit")->result();
    } 
    
    function invoice_emails($id,$email,$institutioncode,$status){
        
        if($id <> null){
            
            $where .=" AND id='$id'";
        }
        
        if($email <> null){
            
            $where .=" AND email LIKE '%$email%'";
        }
        
        if($institutioncode <> null){
            
            $where .=" AND institutioncode='$institutioncode'";
        }
        
        if($status){
            
            $where .=" AND status='$status'";
        }
       
        return $this->main->query("SELECT id,email,"
                                . "institutioncode,status,createdby,createdon,"
                                . "modifiedby,modifiedon "
                                . "FROM invoice_emails WHERE id is not null $where "
                                . "ORDER BY institutioncode,email ASC")->result();
    }
	
    function transactions_info_count($institution,$sponsor,$paymentmode,$start,$end){
        
        if($institution <> null){
            
           $where .=" AND b.institutioncode='$institution' "; 
        }
        
        if($sponsor <> null){
            
           $where .=" AND b.sponsor='$sponsor' "; 
        }
        
        if($paymentmode <> null){
            
           $where .=" AND b.paymentmode='$paymentmode' "; 
        }
        
        if($start <> null){
            
           $where .=" AND b.billtimestamp >='$start 00:00:00' "; 
        }
        
        if($end <> null){
            
           $where .=" AND b.billtimestamp <='$end 23:59:59' "; 
        }
        
        return count($this->main->query("SELECT b.id,b.institutioncode,"
                . "b.sponsor,b.paymentmode,b.billtimestamp,b.createdon,b.amount,"
                . "b.service,b.transactionid,b.receiptno,s.shortname,p.name as pmode,i.name "
                . "FROM bills as b "
                . "INNER JOIN sponsors as s ON s.sponsorcode=b.sponsor "
                . "INNER JOIN payment_modes as p ON p.paymentmodecode=b.paymentmode "
                . "INNER JOIN institutions as i ON i.code=b.institutioncode WHERE b.id is not null $where "
                . "ORDER BY b.billtimestamp DESC")->result());
    }
    
    function transactions_info($institution,$sponsor,$paymentmode,$start,$end,$page,$limit){
        
        if($institution <> null){
            
           $where .=" AND b.institutioncode='$institution' "; 
        }
        
        if($sponsor <> null){
            
           $where .=" AND b.sponsor='$sponsor' "; 
        }
        
        if($paymentmode <> null){
            
           $where .=" AND b.paymentmode='$paymentmode' "; 
        }
        
        if($start <> null){
            
           $where .=" AND b.billtimestamp >='$start 00:00:00' "; 
        }
        
        if($end <> null){
            
           $where .=" AND b.billtimestamp <='$end 23:59:59' "; 
        }
        
        
        return $this->main->query("SELECT b.id,b.institutioncode,"
                . "b.sponsor,b.paymentmode,b.billtimestamp,b.createdon,b.amount,"
                . "b.service,b.transactionid,b.receiptno,s.shortname,p.name as pmode,i.name "
                . "FROM bills as b "
                . "INNER JOIN sponsors as s ON s.sponsorcode=b.sponsor "
                . "INNER JOIN payment_modes as p ON p.paymentmodecode=b.paymentmode "
                . "INNER JOIN institutions as i ON i.code=b.institutioncode WHERE b.id is not null $where "
                . "ORDER BY b.billtimestamp DESC "
                . "LIMIT $page,$limit")->result();
    }
	
    function transactions($id,$institution,$sponsor,$paymentmode,$start,$end,$transactionid){
        
        if($id <> null){
            
           $where .=" AND b.id='$id' "; 
        }
        
       if($institution <> null){
            
           $where .=" AND b.institutioncode='$institution' "; 
        }
        
        if($sponsor <> null){
            
           $where .=" AND b.sponsor='$sponsor' "; 
        }
        
        if($paymentmode <> null){
            
           $where .=" AND b.paymentmode='$paymentmode' "; 
        }
        
        if($start <> null){
            
           $where .=" AND b.billtimestamp >='$start 00:00:00' "; 
        }
        
        if($end <> null){
            
           $where .=" AND b.billtimestamp <='$end 23:59:59' "; 
        }
        
        if($transactionid <> null){
            
           $where .=" AND b.transactionid ='$transactionid' "; 
        }
        
        return $this->main->query("SELECT b.id,b.institutioncode,"
                . "b.sponsor,b.paymentmode,b.billtimestamp,b.createdon,b.amount,"
                . "b.service,b.transactionid,b.receiptno,s.shortname,p.name as pmode ,i.name "
                . "FROM bills as b "
                . "INNER JOIN sponsors as s ON s.sponsorcode=b.sponsor "
                . "INNER JOIN payment_modes as p ON p.paymentmodecode=b.paymentmode "
                . "INNER JOIN institutions as i ON i.code=b.institutioncode WHERE b.id is not null $where "
                . "ORDER BY b.billtimestamp DESC")->result();
    }
    
     function save_sponsor($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('sponsors',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('sponsors',$data);
    }
    
    function activate_deactivate_sponsor($status,$id){
        $status=$status == 1?0:1;
        return $this->main->update('sponsors',array('status'=>$status),array('id'=>$id));
    }
    
    function save_paymentMode($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('payment_modes',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        
        return $this->main->insert('payment_modes',$data);
    }
    
    function activate_deactivate_paymentMode($status,$id){
        $status=$status == 1?0:1;
        return $this->main->update('payment_modes',array('status'=>$status),array('id'=>$id));
    }
	
	 function save_email_address($data,$id){
        
        if($id <> null){
            $data['modifiedby']=$this->session->userdata('user_id');
            $data['modifiedon']=date('Y-m-d H:i:s');
            return $this->main->update('invoice_emails',$data,array('id'=>$id));
        }
        
        $data['createdby']=$this->session->userdata('user_id');
        $data['createdon']=date('Y-m-d H:i:s');
        
        return $this->main->insert('invoice_emails',$data);
    }
    
    function activate_deactivate_invoice_email($id,$status){
        $status=$status == 1?0:1;
        return $this->main->update('invoice_emails',array('status'=>$status),array('id'=>$id));
    }
    
    function invoice_transactions($start,$end,$institution){
        
        if($start <> null){
            
           $where .=" AND billtimestamp >='$start 00:00:00' "; 
        }
        
        if($end <> null){
            
           $where .=" AND billtimestamp <='$end 23:59:59' "; 
        }
        
        if($institution <> null){
            
           $where .=" AND institutioncode='$institution' "; 
        }
        
        
        return $this->main->query("SELECT SUM(amount) AS amount_sum,COUNT(transactionid) AS txn_count,institutioncode,"
                . "sponsor "
                . "FROM bills "
                . "WHERE sponsor NOT IN (".$this->config->item('invoice_excluded_sponsors').") $where "
                . "GROUP BY institutioncode,sponsor "
                . "ORDER BY institutioncode,sponsor ASC")->result();
    }

	function summary_collections($start_date,$end_date){
        $array=array();
        
        $institutions=$this->institutions();
       
        foreach ($institutions as $key=>$value){
            
            $prev_month=$this->main->query("SELECT SUM(amount) AS amount_sum,COUNT(transactionid) AS txn_count "
                . "FROM bills "
                . "WHERE sponsor NOT IN (".$this->config->item('invoice_excluded_sponsors').") "
                . "AND billtimestamp >='$start_date 00:00:00' "
                . "AND billtimestamp <='$end_date 23:59:59' AND institutioncode='$value->code' ")->row();
        
            $cur_month=$this->main->query("SELECT SUM(amount) AS amount_sum,COUNT(transactionid) AS txn_count "
                    . "FROM bills "
                    . "WHERE sponsor NOT IN (".$this->config->item('invoice_excluded_sponsors').") "
                    . "AND billtimestamp >='".date('Y-m')."-01 00:00:00' "
                    . "AND billtimestamp <='".date('Y-m-d H:i:s')."' AND institutioncode='$value->code'")->row();

            $today=$this->main->query("SELECT SUM(amount) AS amount_sum,COUNT(transactionid) AS txn_count "
                    . "FROM bills "
                    . "WHERE sponsor NOT IN (".$this->config->item('invoice_excluded_sponsors').") "
                    . "AND billtimestamp >='".date('Y-m-d')." 00:00:00' "
                    . "AND billtimestamp <='".date('Y-m-d H:i:s')."' "
                    . "AND institutioncode='$value->code'")->row();
            
            $array[]=array(
                'name'=>$value->name,
                'l_month_col'=>$prev_month->amount_sum,
                'l_month_txn'=>$prev_month->txn_count,
                'c_month_col'=>$cur_month->amount_sum,
                'c_month_txn'=>$cur_month->txn_count,
                'today_col'=>$today->amount_sum,
                'today_txn'=>$today->txn_count
                
            );
        }
        
        return $array;
    }
	
	function cancelled_receipts_info_count($institution,$sponsor,$paymentmode,$start,$end){
        
        if($institution <> null){
            
           $where .=" AND b.institutioncode='$institution' "; 
        }
        
        if($sponsor <> null){
            
           $where .=" AND b.sponsor='$sponsor' "; 
        }
        
        if($paymentmode <> null){
            
           $where .=" AND b.paymentmode='$paymentmode' "; 
        }
        
        if($start <> null){
            
           $where .=" AND b.cancelltimestamp >='$start 00:00:00' "; 
        }
        
        if($end <> null){
            
           $where .=" AND b.cancelltimestamp <='$end 23:59:59' "; 
        }
        
        return count($this->main->query("SELECT b.id,b.institutioncode,"
                . "b.sponsor,b.paymentmode,b.billtimestamp,b.cancelltimestamp,b.amount,"
                . "b.service,b.transactionid,b.receiptno,s.shortname,p.name as pmode,i.name "
                . "FROM cancelled_receipts as b "
                . "INNER JOIN sponsors as s ON s.sponsorcode=b.sponsor "
                . "INNER JOIN payment_modes as p ON p.paymentmodecode=b.paymentmode "
                . "INNER JOIN institutions as i ON i.code=b.institutioncode WHERE b.id is not null $where "
                . "ORDER BY b.billtimestamp DESC")->result());
    }
    
    function cancelled_receipts_info($institution,$sponsor,$paymentmode,$start,$end,$page,$limit){
        
        if($institution <> null){
            
           $where .=" AND b.institutioncode='$institution' "; 
        }
        
        if($sponsor <> null){
            
           $where .=" AND b.sponsor='$sponsor' "; 
        }
        
        if($paymentmode <> null){
            
           $where .=" AND b.paymentmode='$paymentmode' "; 
        }
        
        if($start <> null){
            
           $where .=" AND b.cancelltimestamp >='$start 00:00:00' "; 
        }
        
        if($end <> null){
            
           $where .=" AND b.cancelltimestamp <='$end 23:59:59' "; 
        }
        
        
        return $this->main->query("SELECT b.id,b.institutioncode,"
                . "b.sponsor,b.paymentmode,b.billtimestamp,b.cancelltimestamp,b.amount,"
                . "b.service,b.transactionid,b.receiptno,s.shortname,p.name as pmode,i.name "
                . "FROM cancelled_receipts as b "
                . "INNER JOIN sponsors as s ON s.sponsorcode=b.sponsor "
                . "INNER JOIN payment_modes as p ON p.paymentmodecode=b.paymentmode "
                . "INNER JOIN institutions as i ON i.code=b.institutioncode WHERE b.id is not null $where "
                . "ORDER BY b.cancelltimestamp DESC "
                . "LIMIT $page,$limit")->result();
    } 
    
    function cancelled_receipts($id,$institution,$sponsor,$paymentmode,$start,$end,$transactionid){
        if($id <> null){
            
           $where .=" AND b.id='$id' "; 
        }
        
       if($institution <> null){
            
           $where .=" AND b.institutioncode='$institution' "; 
        }
        
        if($sponsor <> null){
            
           $where .=" AND b.sponsor='$sponsor' "; 
        }
        
        if($paymentmode <> null){
            
           $where .=" AND b.paymentmode='$paymentmode' "; 
        }
        
        if($start <> null){
            
           $where .=" AND b.cancelltimestamp >='$start 00:00:00' "; 
        }
        
        if($end <> null){
            
           $where .=" AND b.cancelltimestamp <='$end 23:59:59' "; 
        }
        
        if($transactionid <> null){
            
           $where .=" AND b.transactionid ='$transactionid' "; 
        }
       
        return $this->main->query("SELECT b.id,b.institutioncode,"
                . "b.sponsor,b.paymentmode,b.billtimestamp,b.cancelltimestamp,b.amount,"
                . "b.service,b.transactionid,b.receiptno,s.shortname,p.name as pmode ,i.name "
                . "FROM cancelled_receipts as b "
                . "INNER JOIN sponsors as s ON s.sponsorcode=b.sponsor "
                . "INNER JOIN payment_modes as p ON p.paymentmodecode=b.paymentmode "
                . "INNER JOIN institutions as i ON i.code=b.institutioncode WHERE b.id is not null $where "
                . "ORDER BY b.cancelltimestamp DESC")->result();
    }
}