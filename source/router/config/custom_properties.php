<?php

defined('BASEPATH') OR exit('No direct script access allowed');

$config['default_token']='ABCDEFGHIJK123456';

$config['default_username']='api';

$config['default_password']='12345678';

$config['api_salt']='allsee2016';

$config['offset']=3;

$config['api_min_password_length']=8;

$config['api_max_password_length']=15;

$config['source_token_string']='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';

$config['spcl_characters_replace']=array('&quot;','&apos;','&lt;','&gt;','&amp;');

$config['spcl_characters']=array('"','\'','<','>','&');

$config['bank_name']='KCB BANK (T) LTD';

$config['bank_account']='3300107334';

$config['account_name']='ALLSEE TECHNOLOGIES TANZANIA LIMITED';

$config['Rate101']=0.08;

$config['Rate102']=0.08;

$config['invoice_excluded_sponsors']="'204'";
    
$config['cc_email_addresses']=array('rhoda.mchunga@gmobile.co.tz','liliankafu@yahoo.com','nteboya@gmail.com','ansel.missango@gmobile.co.tz','rrrubenge@gmail.com','safiely@gmobile.co.tz');

$config['from_email_address']='anselm@allseetech.co.tz';

$config['from_name']='Ansel Missango';

$config['reply_to_email_address']='rhoda.mchunga@gmobile.co.tz';

$config['reply_to_name']='Rhoda Mchunga';

$config['failure_reply_email_address']='safiely@gmobile.co.tz';
/*
 * 
 * 
 * 
 * request sample
    <?xml version="1.0" encoding="UTF-8"?>
    <request>
        <credential>
            <accesscode></accesscode>
            <username></username>
            <password></password>
        </credential>
        <transaction>
            <transactionid></transactionid>
            <service></service>
            <sponsor></sponsor>
            <paymentmode></paymentmode>
            <amount></amount>
            <billtimestamp></billtimestamp>
        </transaction>
    </request>
 * 
 * response sample
    <?xml version="1.0" encoding="UTF-8"?>
    <response>
        <code></code>
        <status><status>
        <description></description>
    </response>
 */
