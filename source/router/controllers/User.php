<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class User extends CI_Controller {

    function __construct() {
        parent::__construct();
       
        $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
        $this->output->set_header('Pragma: no-cache');
    }
    
    function index(){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $infodate=date('Y-F', strtotime(date('Y-m')." -1 month"));
            $infodate=explode("-", $infodate);
            $fulltextualmonth=$infodate[1];
            $year=$infodate[0];

            $month=date('Y-m', strtotime(date('Y-m')." -1 month"));
            $month=explode("-", $month);
            $month=$month[1];

            $start_date=$this->firstDay($month,null);
            $end_date=$this->lastday($month,null);
            $this->data['summary']=$this->Administration_model->summary_collections($start_date,$end_date);
            $this->data['userInfo']=$this->Administration_model->current_user_info();
            $this->data['title']='Collections Summary';
            $this->data['content']='user/dashboard';
            $this->load->view('user/template',$this->data);
    }
    
    public function transactions(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $this->data['side_menu']=array(
                                      array('title'=>'Transactions','link'=>'User/transactions'),
                                  );

           if ($this->input->post('institution')) {
               $key['institution'] = $this->input->post('institution');
               $this->data['institution']=$this->input->post('institution');
           }
           
            if ($this->input->post('sponsor')) {
               $key['sponsor'] = $this->input->post('sponsor');
               $this->data['sponsor']=$this->input->post('sponsor');
           }
           
           if ($this->input->post('paymentmode')) {
               $key['paymentmode'] = $this->input->post('paymentmode');
               $this->data['paymentmode']=$this->input->post('paymentmode');
           }
           
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           
          

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));

               $key['institution'] = $exp[1];
               $this->data['institution']=$key['institution'];
               
               $key['sponsor'] = $exp[3];
               $this->data['sponsor']=$key['sponsor'];
               
               $key['paymentmode'] = $exp[5];
               $this->data['paymentmode']=$key['paymentmode'];
               
               $key['start'] = $exp[7];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[9];
               $this->data['end']=$key['end'];
               
               $docType = $exp[11];
               $this->data['docType']=$docType;
           }

           $institution =$key['institution'];
           $sponsor =$key['sponsor'];
           $paymentmode =$key['paymentmode'];
           $start =$key['start'];
           $end =$key['end'];

           $config["base_url"] = base_url() . "index.php/User/transactions/institution_".$key['institution']."_sponsor_".$key['sponsor']."_paymentmode_".$key['paymentmode']."_start_".$key['start']."_end_".$key['end']."/";
           $config["total_rows"] =$this->Administration_model->transactions_info_count($institution,$sponsor,$paymentmode,$start,$end);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['transactions'] = $this->Administration_model->transactions_info($institution,$sponsor,$paymentmode,$start,$end,$page,$limit);

           $this->data['title']="Transactions";
           $this->data['institutions']=$this->Administration_model->institutions();
           $this->data['sponsors']=$this->Administration_model->sponsors();
           $this->data['paymentModes']=$this->Administration_model->paymentModes();
           $this->data['userInfo']=$this->Administration_model->current_user_info();
           $this->data['content']='user/transactions';
           $this->load->view('user/template',$this->data);
   }
   
    public function profile(){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'User/edit_profile'),
                                           array('title'=>'Change Password','link'=>'User/change_password'),
                                        );
                
                $this->data['title']="User Profile";
                $this->data['content']='user/profile';
                $this->data['userInfo']=$this->Administration_model->current_user_info();
                $this->data['profile']=$this->Administration_model->profile_data();
		$this->load->view('user/template',$this->data);
        }
        
    public function edit_profile(){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }

            $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'User/edit_profile'),
                                           array('title'=>'Change Password','link'=>'User/change_password'),
                                        );
            
            $this->form_validation->set_rules('first_name','First Name','required|xss_clean');
            $this->form_validation->set_rules('middle_name','Middle Name','xss_clean');
            $this->form_validation->set_rules('last_name','Last Name','required|xss_clean');
            $this->form_validation->set_rules('username','Username','required|xss_clean|min_length[6]');
            $this->form_validation->set_rules('email', 'Email Address', 'valid_email|xss_clean');
            $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|xss_clean');

            if($this->form_validation->run() == true){

                 $firstname=$this->input->post('first_name');
                 $middlename=$this->input->post('middle_name');
                 $lastname=$this->input->post('last_name');
                 $username=$this->input->post('username');
                 $email=$this->input->post('email');
                 $mobile=$this->input->post('mobile');

                $user=array(
                            'FIRST_NAME'=>$firstname,
                            'MIDDLE_NAME'=>$middlename<>null?$middlename:null,
                            'LAST_NAME'=>$lastname,
                            'USERNAME'=>$username,
                            'EMAIL'=>$email<>null?$email:null,
                            'MSISDN'=>$mobile,
                        );

                $user_group=array(
                            'GROUP_ID'=>$this->session->userdata('group')
                        );

                $username_check=$this->username_uniqueness($username,$this->session->userdata('user_id'));
                if($username_check){


                    //do registration
                     $resp=$this->Administration_model->system_user_registration($user,$user_group,$this->session->userdata('user_id'),null);

                       if($resp){

                           redirect('User/profile','refresh');
                       }else{

                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">registration failed!</div>';
                       }
                }else{
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">username exists already!</div>';
                }
            }

            $this->data['title']="Edit Profile";
            $this->data['content']='user/edit_profile';
            $this->data['userInfo']=$this->Administration_model->current_user_info();
            $this->data['profile']=$this->Administration_model->profile_data();
            $this->load->view('user/template',$this->data);
    }
        
    public function change_password(){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }

            $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'User/edit_profile'),
                                           array('title'=>'Change Password','link'=>'User/change_password'),
                                        );

            $this->form_validation->set_rules('cur_password', 'Current Password', 'required|xss_clean|callback_cur_password'); 
            $this->form_validation->set_rules('password', 'New Password', 'xss_clean|required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[conf_password]');
            $this->form_validation->set_rules('conf_password', 'New Password Confirmation', 'required'); 

            if($this->form_validation->run() == true){

                $password=$this->input->post('password');
                $pchange=$this->Administration_model->change_password($password);

                if($pchange){
                           
                        $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">password change successfull!</div>';
                }else{
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">password change failed!</div>';
                }
            }

            $this->data['title']="Change Password";
            $this->data['content']='user/change_password';
            $this->data['userInfo']=$this->Administration_model->current_user_info();
            $this->data['profile']=$this->Administration_model->profile_data();
            $this->load->view('user/template',$this->data);
    }
    
    function cur_password($password){
            
            $pf=$this->Administration_model->profile_data();
            $hashed_old=$this->Administration_model->hash_db_password($password,$pf);
           
             if($pf->password <> $hashed_old){
                 
                 $this->form_validation->set_message("cur_password","The %s is incorrect");
                  return FALSE;
             }
            
            return TRUE;
    }
    
    function check_session_account_validity(){
            
             if (!$this->ion_auth->logged_in())
		{
			return FALSE;
		}
                
                if(!$this->ion_auth->in_group('User')){
                    
                    return FALSE;
                }
                
                return TRUE;
        }
    
    function username_uniqueness($username,$id){
            
        return $this->Administration_model->check_username($username,$id);
    }
    
    function firstDay($month,$year){
        if (empty($month)) {
            
          $month = date('m');
       }
       
       if (empty($year)) {
           
          $year = date('Y');
       }
       
       $result = strtotime("{$year}-{$month}-01");
       return date('Y-m-d', $result);
    } 
    
    function lastday($month, $year){
        if (empty($month)) {
            
           $month = date('m');
        }
        
        if (empty($year)) {
            
           $year = date('Y');
        }
        
        $result = strtotime("{$year}-{$month}-01");
        $result = strtotime('-1 second', strtotime('+1 month', $result));
        return date('Y-m-d', $result);
    }
}