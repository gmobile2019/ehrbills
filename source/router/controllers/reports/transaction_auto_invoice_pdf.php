<?php
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
//heading
$html  = '<h3 align="center">Collections Invoice</h3>';

$inst_name=$this->Administration_model->institutions(null,$institution);
$html .= '<h4 align="left">Institution : '.$inst_name[0]->name.'</h4>';
$html .= '<h4 align="left">Institution ID : '.$institution.'</h4>';
$html .= '<h4 align="left">Institution Address : '.$inst_name[0]->postal.' '.$inst_name[0]->city.'</h4>';
$html .= '<h4 align="left">Institution Phone : '.$inst_name[0]->phone.'</h4>';

$html .= '<h4 align="right">Bank Name : <b>'.$this->config->item('bank_name').'</b></h4>';
$html .= '<h4 align="right">Bank Account : <b>'.$this->config->item('bank_account').'</b></h4>';
$html .= '<h4 align="right">Account Name : <b>'.$this->config->item('account_name').'</b></h4>';

$html .= '<h4 align="left">Start Date : '.$start_date.'</h4>';
$html .= '<h4 align="left">End Date : '.$end_date.'</h4>';

$html .= '<h4 align="right">Invoice # : '.$invoiceNo.'</h4>';

$html.='<table border="1">
                <tr>
                    <td style="width:200px;text-align:center"><b> &nbsp;S/No</b></td>
                    <td style="width:400px;text-align:center"><b> &nbsp;Description</b></td>
                    <td style="width:400px;text-align:center"><b> &nbsp;Rate</b></td>
                    <td style="width:500px;text-align:center"><b> &nbsp;Collections (Tsh)</b></td>
                    <td style="width:500px;text-align:center"><b> &nbsp;Commission (Tsh)</b></td>
                </tr>';
$i = 1;

    foreach ($data as $ky => $val) {
        
        $sp=$this->Administration_model->get_sponsor_by_code($val->sponsor);
        
        $total_col +=$val->amount_sum;
        $com =$val->amount_sum*$this->config->item('Rate'.$institution);
        $total_com +=$com;
        
        
        $html .='<tr>
                    <td>&nbsp;&nbsp;' . $i++ .'</td>
                    <td>&nbsp;&nbsp;' . $sp[0]->shortname . '</td>
                    <td>&nbsp;&nbsp;'.$this->config->item('Rate'.$institution).'</td>
                    <td align="right">'.number_format($val->amount_sum,2).'&nbsp;&nbsp;</td>
                    <td align="right">'.number_format($com,2).'&nbsp;&nbsp;</td>
                </tr>';
      }

$html.='<tr>'
        . '<td align="right" colspan="3"><b>Total : &nbsp;&nbsp;&nbsp;&nbsp;</b></td>'
        . '<td align="right"><b>'.  number_format($total_col,2).'&nbsp;&nbsp;</b> </td>'
        . '<td align="right"><b>'.  number_format($total_com,2).'&nbsp;&nbsp;</b> </td>'
        . '</tr>'
        . '</table>';

$this->pdf->writeHTML($html);
ob_end_clean();
$real_path='./invoices/'.$filename;
$this->pdf->Output($real_path, 'F');
?>