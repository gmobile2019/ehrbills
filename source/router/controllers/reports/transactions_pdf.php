<?php
ini_set('max_execution_time',0);
$this->pdf->start_pdf();
$this->pdf->SetSubject('allsee');
$this->pdf->SetKeywords('allsee');

//start pdf page
$this->pdf->AddPage();
$this->pdf->SetY(35);
$this->pdf->SetX(2);
$this->pdf->SetFont('', '', 8);
//heading
$html  = '<h3 align="center">Transactions</h3>';

if($start <> null){
    $html .= '<h3 align="left">Start : '.$start.'</h3>';
}

if($end <> null){
    
    $html .= '<h3 align="left">End : '.$end.'</h3>';
}

    $html.='<table border="1">
                <tr>
                    <td style="width:100px;text-align:center"><b> &nbsp;S/No</b></td>
                    <td style="width:300px;text-align:center"><b> &nbsp;Institution</b></td>
                    <td style="width:300px;text-align:center"><b> &nbsp;Sponsor</b></td>
                    <td style="width:300pxtext-align:center;"><b> &nbsp;Payment Mode</b></td>
                    <td style="width:350px;text-align:center"><b> &nbsp;Service</b></td>
                    <td style="width:300px;text-align:center"><b> &nbsp;Transaction Id</b></td>
                    <td style="width:350px;text-align:center"><b> &nbsp;Timestamp</b></td>
                    <td style="width:200px;text-align:center"><b> &nbsp;Amount</b></td>
                </tr>';
$i = 1;
//echo $html;exit;
    foreach ($data as $key => $value) {
        $total +=$value->amount;
        $html .='<tr>
                    <td>&nbsp;&nbsp;' . $i++ .'</td>
                    <td>&nbsp;&nbsp;' .$value->name. ' &nbsp; </td>
                    <td>&nbsp;&nbsp;' . $value->shortname . '</td>
                    <td>&nbsp;&nbsp;' . $value->pmode.'</td>
                    <td>&nbsp;&nbsp;'.$value->service.'</td>
                    <td>&nbsp;&nbsp;'.$value->transactionid.'</td>
                    <td>&nbsp;&nbsp;'.$value->billtimestamp.'</td>
                    <td align="right">'.number_format($value->amount,2).'&nbsp;&nbsp;</td>
                </tr>';
      }

$html.='<tr>'
        . '<td align="right" colspan="7"><b>Total : &nbsp;&nbsp;&nbsp;&nbsp;</b></td>'
        . '<td align="right"><b>'.  number_format($total,2).'&nbsp;&nbsp;</b> </td>'
        . '</tr>'
        . '</table>';

$this->pdf->writeHTML($html);
ob_end_clean();
$this->pdf->Output('Transactions.pdf', 'D');
exit;
?>