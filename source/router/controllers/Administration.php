<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Administration extends CI_Controller {

    function __construct() {
        parent::__construct();
       
        $this->load->library('upload');
        ini_set("memory_limit","512M");
        $this->output->set_header('Last-Modified:'.gmdate('D, d M Y H:i:s').'GMT');
        $this->output->set_header('Cache-Control: no-store, no-cache, must-revalidate');
        $this->output->set_header('Cache-Control: post-check=0, pre-check=0',false);
        $this->output->set_header('Pragma: no-cache');
    }
    
    function index(){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
            
            $infodate=date('Y-F', strtotime(date('Y-m')." -1 month"));
            $infodate=explode("-", $infodate);
            $fulltextualmonth=$infodate[1];
            $year=$infodate[0];

            $month=date('Y-m', strtotime(date('Y-m')." -1 month"));
            $month=explode("-", $month);
            $month=$month[1];

            $start_date=$this->firstDay($month,null);
            $end_date=$this->lastday($month,null);
            
            $this->data['summary']=$this->Administration_model->summary_collections($start_date,$end_date);
            $this->data['userInfo']=$this->Administration_model->current_user_info();
            $this->data['title']='Collections Summary';
            $this->data['content']='administrator/dashboard';
            $this->load->view('administrator/template',$this->data);
    }
    
    public function view_users()
	{
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                 $this->data['side_menu']=array(
                                            array('title'=>'Create Users','link'=>'Administration/create_users'),
                                            array('title'=>'View Users','link'=>'Administration/view_users'),
                                        );
                
                if ($this->input->post('name')) {
                    $key['name'] = $this->input->post('name');
                    $this->data['name']=$this->input->post('name');
                }
                
                if ($this->input->post('group')) {
                    $key['group'] = $this->input->post('group');
                    $this->data['group']=$this->input->post('group');
                }

               if ($this->uri->segment(3)) {
                    $exp = explode("_", $this->uri->segment(3));
                    
                    $key['name'] = $exp[1];
                    $this->data['name']=$key['name'];
                    
                    $key['group'] = $exp[3];
                    $this->data['group']=$key['group'];
                }
                
                $name =$key['name'];
                $group =$key['group'];
                
                $config["base_url"] = base_url() . "index.php/Administration/view_users/name_".$key['name']."_group_".$key['group']."/";
                $config["total_rows"] =$this->Administration_model->member_info_count($name,$group);
                $config["per_page"] =30;
                $config['full_tag_open'] = '<ul class="pagination">';
                $config['full_tag_close'] = '</ul>';

                $config['num_tag_open'] = '<li>';
                $config['num_tag_close'] = '</li>';


                $config['prev_tag_open'] = '<li>';
                $config['prev_tag_close'] = '</li>';

                $config['next_tag_open'] = '<li>';
                $config['next_tag_close'] = '</li>';

                $config['next_link']='&raquo;';
                $config['prev_link']='&laquo;';
                $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
                $config['cur_tag_close'] = '</span>';
                $config["uri_segment"] =4;
                $config["num_links"] = 3; // round($choice);
               
                $this->pagination->initialize($config);
                $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
                $limit=$config["per_page"];
                $this->data['per_page']=$page;
                $this->data['links'] = $this->pagination->create_links();
                $this->data['members'] = $this->Administration_model->member_info($name,$group,$page,$limit);
                
                $this->data['title']="System Users";
                $this->data['groups']=$this->Administration_model->get_registration_groups();
                $this->data['userInfo']=$this->Administration_model->current_user_info();
                $this->data['content']='administrator/view_users';
		$this->load->view('administrator/template',$this->data);
	}
    
    public function create_users($id=null)
   {
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                            array('title'=>'Create Users','link'=>'Administration/create_users'),
                                            array('title'=>'View Users','link'=>'Administration/view_users'),
                                        );
           $error=FALSE;
           $this->form_validation->set_rules('first_name','First Name','required|xss_clean');
           $this->form_validation->set_rules('middle_name','Middle Name','xss_clean');
           $this->form_validation->set_rules('last_name','Last Name','required|xss_clean');
           $this->form_validation->set_rules('username','Username','required|xss_clean|min_length[6]');
           $this->form_validation->set_rules('email', 'Email Address', 'valid_email|xss_clean');
           $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|xss_clean');
           $this->form_validation->set_rules('group','User Group','required|xss_clean');
           
            if($id <> null){

               if($this->input->post('password') <> null){

                 $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[conf_password]');
                 $this->form_validation->set_rules('conf_password', 'Password Confirmation', 'required');   
               }
           }else{

             $this->form_validation->set_rules('password', 'Password', 'required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[conf_password]');
             $this->form_validation->set_rules('conf_password', 'Password Confirmation', 'required');  
           }

           $group=$this->input->post('group');
           
           
            if ($this->form_validation->run() == TRUE)
           {

                $nurse_incharge=FALSE;
                $firstname=$this->input->post('first_name');
                $middlename=$this->input->post('middle_name');
                $lastname=$this->input->post('last_name');
                $username=$this->input->post('username');
                $email=$this->input->post('email');
                $mobile=$this->input->post('mobile');
                
                $password=$this->input->post('password');

                

                       $user=array(
                           'first_name'=>$firstname,
                           'middle_name'=>$middlename<>null?$middlename:null,
                           'ip_address'=>$this->input->ip_address(),
                           'last_name'=>$lastname,
                           'username'=>$username,
                           'email'=>$email<>null?$email:null,
                           'msisdn'=>$mobile,
                       );

                       $user_group=array(
                           'group_id'=>$group
                       );
                       
                       
                       
                       //check username uniqueness
                       $username_check=$this->username_uniqueness($username,$id);
                       if($username_check){


                           //do registration
                            $resp=$this->Administration_model->system_user_registration($user,$user_group,$id,$password);

                              if($resp){
                                  
                                  
                                  redirect(current_url());
                              }else{
                                  $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">registration failed!</div>';
                              }
                       }else{
                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">username exists already!</div>';
                        }
               
           }
           
           
           $this->data['title']="Create Users";
           $this->data['id']="$id";
           $this->data['member']=$this->Administration_model->get_member_info($id);
           $this->data['groups']=$this->Administration_model->get_registration_groups();
           $this->data['userInfo']=$this->Administration_model->current_user_info();
           $this->data['content']='administrator/create_users';
           $this->load->view('administrator/template',$this->data);
    }
     
    public function activate_deactivate_users($id,$status){
        if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }
                
        $this->Administration_model->activate_deactivate_users($id,$status);
        redirect('Administration/view_users','refresh');
    }
    
    public function add_institution()
   {
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                            array('title'=>'Add Institution','link'=>'Administration/add_institution'),
                                            array('title'=>'View Institutions','link'=>'Administration/view_institutions'),
                                        );
           $error=FALSE;
           $this->form_validation->set_rules('code','Institution Id','required|xss_clean');
           $this->form_validation->set_rules('name','Name','required|xss_clean');
           $this->form_validation->set_rules('phone','Phone','required|xss_clean|numeric');
           $this->form_validation->set_rules('postal','Postal Address','xss_clean|numeric');
           $this->form_validation->set_rules('email', 'Email Address', 'valid_email|xss_clean');
           $this->form_validation->set_rules('city', 'City', 'required|xss_clean');
           $this->form_validation->set_rules('website','Website','xss_clean');
            

           

            if ($this->form_validation->run() == TRUE)
           {
                
                $name=$this->input->post('name');
                $institution_id=$this->input->post('code');
                $phone=$this->input->post('phone');
                $email=$this->input->post('email');
                $postal=$this->input->post('postal');
                $city=$this->input->post('city');
                $website=$this->input->post('website');
                
                $institution=array(
                   'name'=>$name,
                   'code'=>$institution_id,
                   'phone'=>$phone,
                   'postal'=>$postal<>null?$postal:null,
                   'city'=>$city,
                   'website'=>$website<>null?$website:null,
                   'email'=>$email,
               );
                        
                if($_FILES['logo']['name'] <> null){
                        $config['upload_path'] = './logo/';
                        $config['allowed_types'] = 'jpeg|jpg|png';
                        $filename=explode('.',$_FILES['logo']['name']);
                        $photo_ext=end($filename);
                        $logoname=$institution_id;
                        $filename=$logoname.".".$photo_ext;
                        $config['file_name']=$filename;
                        $config['overwrite']=true;
                        $this->upload->initialize($config);

                       if($this->upload->do_upload('logo')){



                           $logo=file_get_contents('./logo/'.$filename);
                           $logostring=base64_encode($logo);

                           $institution['logoname']=$filename;
                           $institution['logostring']=$logostring;
                        }else{
                          $this->data['message']=$this->upload->display_errors('<div class="alert alert-danger " role="alert" style="text-align:center;">','</div>');
                          $error=TRUE;
                       }
                   }

                   if(!$error){


                       //do registration
                        $resp=$this->Administration_model->save_institution($institution);

                          if($resp){

                              redirect(current_url());
                          }else{

                              $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">not saved!</div>';
                          }
                   }
           }

           $this->data['title']="Add Institution";
           $this->data['id']="$id";
           $this->data['userInfo']=$this->Administration_model->current_user_info();
           $this->data['content']='administrator/add_Institution';
           $this->load->view('administrator/template',$this->data);
    }
    
    
    public function view_institutions(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           $this->data['side_menu']=array(
                                            array('title'=>'Add Institution','link'=>'Administration/add_institution'),
                                            array('title'=>'View Institutions','link'=>'Administration/view_institutions'),
                                        );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));

               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
           }

           $name =$key['name'];

           $config["base_url"] = base_url() . "index.php/Administration/view_institutions/name_".$key['name']."/";
           $config["total_rows"] =$this->Administration_model->institution_info_count($name);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['institutions'] = $this->Administration_model->institution_info($name,$page,$limit);

           $this->data['title']="Institutions";
           $this->data['content']='administrator/institutions';
           $this->data['userInfo']=$this->Administration_model->current_user_info();
           $this->load->view('administrator/template',$this->data);
   }
   
    public function add_sponsor($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                           array('title'=>'Add Sponsor','link'=>'Administration/add_sponsor'),
                                           array('title'=>'Add Payment Mode','link'=>'Administration/add_paymentMode'),
                                           array('title'=>'View Sponsor(s)','link'=>'Administration/view_sponsors'),
                                           array('title'=>'View Payment Mode(s)','link'=>'Administration/view_paymentModes'),
                                       );
           
           $this->form_validation->set_rules('sponsorcode','Sponsor Code','required|xss_clean|numeric|exact_length[3]');
           $this->form_validation->set_rules('shortname','Short Name','required|xss_clean');
           $this->form_validation->set_rules('fullname','Full Name','xss_clean|required');
           $this->form_validation->set_rules('description','Description','xss_clean');
           


            if ($this->form_validation->run() == TRUE)
           {


                $sponsorcode=$this->input->post('sponsorcode');
                $shortname=$this->input->post('shortname');
                $fullname=$this->input->post('fullname');
                $description=nl2br(trim($this->input->post('description')));

                

                       $sponsor=array(
                           'sponsorcode'=>$sponsorcode,
                           'shortname'=>$shortname,
                           'fullname'=>$fullname,
                           'description'=>$description
                       );


                      //do saving
                        $resp=$this->Administration_model->save_sponsor($sponsor,$id);

                          if($resp){

                              redirect(current_url());
                          }else{

                              $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                          }
               
           }

           $this->data['title']="Add Sponsor";
           $this->data['id']="$id";
           $this->data['sponsor']=$this->Administration_model->sponsors($id);
           $this->data['userInfo']=$this->Administration_model->current_user_info();
           $this->data['content']='administrator/add_sponsor';
           $this->load->view('administrator/template',$this->data);
    }
    
    public function view_sponsors(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
          $this->data['side_menu']=array(
                                        array('title'=>'Add Sponsor','link'=>'Administration/add_sponsor'),
                                        array('title'=>'Add Payment Mode','link'=>'Administration/add_paymentMode'),
                                        array('title'=>'View Sponsor(s)','link'=>'Administration/view_sponsors'),
                                        array('title'=>'View Payment Mode(s)','link'=>'Administration/view_paymentModes'),
                                    );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));

               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];
           }

           $name =$key['name'];

           $config["base_url"] = base_url() . "index.php/Administration/view_sponsors/name_".$key['name']."/";
           $config["total_rows"] =$this->Administration_model->sponsor_info_count($name);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['sponsors'] = $this->Administration_model->sponsor_info($name,$page,$limit);

           $this->data['title']="Sponsors";
           $this->data['userInfo']=$this->Administration_model->current_user_info();
           $this->data['content']='administrator/view_sponsors';
           $this->load->view('administrator/template',$this->data);
   }
        
    public function add_paymentMode($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                        array('title'=>'Add Sponsor','link'=>'Administration/add_sponsor'),
                                        array('title'=>'Add Payment Mode','link'=>'Administration/add_paymentMode'),
                                        array('title'=>'View Sponsor(s)','link'=>'Administration/view_sponsors'),
                                        array('title'=>'View Payment Mode(s)','link'=>'Administration/view_paymentModes'),
                                    );
           
           $this->form_validation->set_rules('paymentmodecode','Payment Mode Code','required|xss_clean|numeric|exact_length[3]');
           $this->form_validation->set_rules('name','Name','required|xss_clean');
           $this->form_validation->set_rules('sponsor','Sponsor','xss_clean|required');
           $this->form_validation->set_rules('description','Description','xss_clean');
           


            if ($this->form_validation->run() == TRUE)
           {


                $paymentmodecode=$this->input->post('paymentmodecode');
                $name=$this->input->post('name');
                $sponsor=$this->input->post('sponsor');
                $description=nl2br(trim($this->input->post('description')));

                

                       $paymentMode=array(
                           'paymentmodecode'=>$paymentmodecode,
                           'name'=>$name,
                           'sponsor_id'=>$sponsor,
                           'description'=>$description
                       );


                       //do saving
                        $resp=$this->Administration_model->save_paymentMode($paymentMode,$id);

                          if($resp){

                              redirect(current_url());
                          }else{

                              $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                          }
               
           }

           $this->data['title']="Add Payment Mode";
           $this->data['id']="$id";
           $this->data['sponsors']=$this->Administration_model->sponsors();
           $this->data['paymentMode']=$this->Administration_model->paymentModes($id);
           $this->data['userInfo']=$this->Administration_model->current_user_info();
           $this->data['content']='administrator/add_paymentMode';
           $this->load->view('administrator/template',$this->data);
    }
    
    public function view_paymentModes(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           $this->data['side_menu']=array(
                                        array('title'=>'Add Sponsor','link'=>'Administration/add_sponsor'),
                                        array('title'=>'Add Payment Mode','link'=>'Administration/add_paymentMode'),
                                        array('title'=>'View Sponsor(s)','link'=>'Administration/view_sponsors'),
                                        array('title'=>'View Payment Mode(s)','link'=>'Administration/view_paymentModes'),
                                    );

           if ($this->input->post('name')) {
               $key['name'] = $this->input->post('name');
               $this->data['name']=$this->input->post('name');
           }

           if ($this->input->post('sponsor')) {
               $key['sponsor'] = $this->input->post('sponsor');
               $this->data['sponsor']=$this->input->post('sponsor');
           }

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));

               $key['name'] = $exp[1];
               $this->data['name']=$key['name'];

               $key['sponsor'] = $exp[3];
               $this->data['sponsor']=$key['sponsor'];
           }

           $name =$key['name'];
           $sponsor =$key['sponsor'];

           $config["base_url"] = base_url() . "index.php/Administration/view_paymentModes/name_".$key['name']."_sponsor_".$key['sponsor']."/";
           $config["total_rows"] =$this->Administration_model->paymentMode_info_count($name,$sponsor);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['paymentModes'] = $this->Administration_model->paymentMode_info($name,$sponsor,$page,$limit);

           $this->data['title']="Payment Modes";
           $this->data['sponsors']=$this->Administration_model->sponsors();
           $this->data['userInfo']=$this->Administration_model->current_user_info();
           $this->data['content']='administrator/view_paymentModes';
           $this->load->view('administrator/template',$this->data);
   }
    
    public function view_credentials(){
            if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
            $this->data['side_menu']=array(
                                        array('title'=>'New Credential','link'=>'Administration/add_credential'),
                                        array('title'=>'View Credentials','link'=>'Administration/view_credentials'),
                                    ); 
                
            if ($this->input->post('instituiton')) {
                $key['instituiton'] =$this->input->post('instituiton');
                $this->data['instituiton']=$this->input->post('instituiton');
            }


            

           if ($this->uri->segment(3)) {
                $exp = explode("_", $this->uri->segment(3));

                $key['instituiton'] =$this->input->post('instituiton');
                $this->data['instituiton']=$key['instituiton'];

            }

            $instituiton =$key['instituiton'];

            $config["base_url"] = base_url() . "index.php/Administration/view_credentials/instituiton_".$key['instituiton']."/";
            $config["total_rows"] =$this->Administration_model->access_credentials_count($instituiton);
            $config["per_page"] =14;
            $config['full_tag_open'] = '<ul class="pagination">';
            $config['full_tag_close'] = '</ul>';

            $config['num_tag_open'] = '<li>';
            $config['num_tag_close'] = '</li>';


            $config['prev_tag_open'] = '<li>';
            $config['prev_tag_close'] = '</li>';

            $config['next_tag_open'] = '<li>';
            $config['next_tag_close'] = '</li>';

            $config['next_link']='&raquo;';
            $config['prev_link']='&laquo;';
            $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
            $config['cur_tag_close'] = '</span>';
            $config["uri_segment"] =4;
            $config["num_links"] = 3; // round($choice);

            $this->pagination->initialize($config);
            $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
            $limit=$config["per_page"];
            $this->data['per_page']=$page;
            $this->data['links'] = $this->pagination->create_links();
            $this->data['credentials'] = $this->Administration_model->access_credentials($instituiton,$page,$limit);
            
            $this->data['title']="API Credentials";
            $this->data['content']='administrator/api_credentials';
            $this->data['institutions']=$this->Administration_model->institutions();
            $this->data['userInfo']=$this->Administration_model->current_user_info();
            $this->load->view('administrator/template',$this->data);
       }
       
    public function add_credential($id){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
            if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
           $this->data['side_menu']=array(
                                        array('title'=>'New Credential','link'=>'Administration/add_credential'),
                                        array('title'=>'View Credentials','link'=>'Administration/view_credentials'),
                                    );
            
            $this->form_validation->set_rules('username','Username','required|xss_clean|alpha_numeric|max_length[20]');
            $this->form_validation->set_rules('password', 'Password', 'xss_clean|required|alpha_numeric|min_length[' . $this->config->item('api_min_password_length') . ']|max_length[' . $this->config->item('api_max_password_length') . ']|matches[conf_password]');
            $this->form_validation->set_rules('conf_password', 'Password Confirmation', 'required');
            $this->form_validation->set_rules('institution', 'Institution', 'xss_clean|required');
            
            if($this->form_validation->run() == true){
                
                $password=$this->input->post('password');
                $username=$this->input->post('username');
                $institutioncode=$this->input->post('institution');
                $password=$this->hash_api_password($password);
                $timekey=date('YmdHis');
               
                $array=array(
                    'username'=>$username,
                    'institutioncode'=>$institutioncode,
                    'timekey'=>$timekey,
                    'password'=>$password,
                );
               
                $crt=$this->Administration_model->add_credential($array,$id);
                
                if($crt){
                                       
                    $this->data['message']="creation successfull!";
                    $this->data['message_class']="form_success";
                    redirect('Administration/view_credentials','refresh');
                }else{

                    $this->data['message']="creation failed!";
                    $this->data['message_class']="form_error";
                }
            }
           
        $this->data['title']="Add/Change Credentials";
        $this->data['id']=$id;
        $this->data['institutions']=$this->Administration_model->institutions();
        $this->data['content']='administrator/add_credential';
        $this->data['userInfo']=$this->Administration_model->current_user_info();
        $this->load->view('administrator/template',$this->data);
    }
       
    public function regenerate_access_key($id){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
             {
                     //redirect them to the login page
                     redirect('auth/logout', 'refresh');
             }

             $prev_credential=$this->Administration_model->access_credential($id);
             $array=array(
                 'accesskey'=>$this->Administration_model->api_access_key($prev_credential[0]->institutioncode,$prev_credential[0]->timekey),
                 'modifiedby'=>$this->session->userdata('user_id'),
                 'modifiedon'=>date('Y-m-d H:i:s')
             );

            $this->Administration_model->reset_access_key($array,$id);
            redirect('Administration/view_credentials','refresh');
    }
    
    public function activate_deactivate_api_access($id,$status){
           if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }

               $this->Administration_model->activate_deactivate_api_access($id,$status);
               redirect('Administration/view_credentials','refresh');
       }
       
     public function add_invoice_email($id=null){
           $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
           if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }

           $this->data['side_menu']=array(
                                        array('title'=>'Add Invoice Email','link'=>'Administration/add_invoice_email'),
                                        array('title'=>'Emails','link'=>'Administration/view_emails'),
                                    );
           
           $this->form_validation->set_rules('email','Email Address','required|xss_clean|valid_email');
           $this->form_validation->set_rules('institution','Institution','xss_clean|required');
           


            if ($this->form_validation->run() == TRUE)
           {


                $email=$this->input->post('email');
                $institution=$this->input->post('institution');

                

                       $email=array(
                           'email'=>$email,
                           'institutioncode'=>$institution,
                       );


                       //do saving
                        $resp=$this->Administration_model->save_email_address($email,$id);

                          if($resp){

                              redirect(current_url());
                          }else{

                              $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">saving failed!</div>';
                          }
               
           }

           $this->data['title']="Add/Edit Email Address";
           $this->data['id']="$id";
           $this->data['email']=$this->Administration_model->invoice_emails($id);
           $this->data['institutions']=$this->Administration_model->institutions();
           $this->data['userInfo']=$this->Administration_model->current_user_info();
           $this->data['content']='administrator/add_invoice_email';
           $this->load->view('administrator/template',$this->data);
    }
    
    public function view_emails(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           $this->data['side_menu']=array(
                                        array('title'=>'Add Invoice Email','link'=>'Administration/add_invoice_email'),
                                        array('title'=>'Emails','link'=>'Administration/view_emails'),
                                    );

           if ($this->input->post('email')) {
               $key['email'] = $this->input->post('email');
               $this->data['email']=$this->input->post('email');
           }

           if ($this->input->post('institution')) {
               $key['institution'] = $this->input->post('institution');
               $this->data['institution']=$this->input->post('institution');
           }

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));

               $key['email'] = $exp[1];
               $this->data['email']=$key['email'];

               $key['institution'] = $exp[3];
               $this->data['institution']=$key['institution'];
           }

           $email =$key['email'];
           $institution =$key['institution'];

           $config["base_url"] = base_url() . "index.php/Administration/view_emails/email_".$key['email']."_institution_".$key['institution']."/";
           $config["total_rows"] =$this->Administration_model->invoice_emails_info_count($email,$institution);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['institutions_info'] = $this->Administration_model->invoice_emails_info($email,$institution,$page,$limit);

           $this->data['title']="Email Addresses";
           $this->data['institutions']=$this->Administration_model->institutions();
           $this->data['userInfo']=$this->Administration_model->current_user_info();
           $this->data['content']='administrator/view_invoice_emails';
           $this->load->view('administrator/template',$this->data);
   }
   
   public function activate_deactivate_invoice_email($id,$status){
           if (!$this->check_session_account_validity())
               {
                       //redirect them to the login page
                       redirect('auth/logout', 'refresh');
               }

               $this->Administration_model->activate_deactivate_invoice_email($id,$status);
               redirect('Administration/view_emails','refresh');
    }
	
    public function transactions(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
           $this->data['side_menu']=array(
                                      array('title'=>'Transactions','link'=>'Administration/transactions'),
                                      array('title'=>'Invoices','link'=>'Administration/transaction_invoices'),
                                      array('title'=>'Cancelled Receipts','link'=>'Administration/cancelled_receipts'),
                                  );

           if ($this->input->post('institution')) {
               $key['institution'] = $this->input->post('institution');
               $this->data['institution']=$this->input->post('institution');
           }
           
            if ($this->input->post('sponsor')) {
               $key['sponsor'] = $this->input->post('sponsor');
               $this->data['sponsor']=$this->input->post('sponsor');
           }
           
           if ($this->input->post('paymentmode')) {
               $key['paymentmode'] = $this->input->post('paymentmode');
               $this->data['paymentmode']=$this->input->post('paymentmode');
           }
           
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           
          

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));

               $key['institution'] = $exp[1];
               $this->data['institution']=$key['institution'];
               
               $key['sponsor'] = $exp[3];
               $this->data['sponsor']=$key['sponsor'];
               
               $key['paymentmode'] = $exp[5];
               $this->data['paymentmode']=$key['paymentmode'];
               
               $key['start'] = $exp[7];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[9];
               $this->data['end']=$key['end'];
               
               $docType = $exp[11];
               $this->data['docType']=$docType;
           }

           $institution =$key['institution'];
           $sponsor =$key['sponsor'];
           $paymentmode =$key['paymentmode'];
           $start =$key['start'];
           $end =$key['end'];

           $config["base_url"] = base_url() . "index.php/Administration/transactions/institution_".$key['institution']."_sponsor_".$key['sponsor']."_paymentmode_".$key['paymentmode']."_start_".$key['start']."_end_".$key['end']."/";
           $config["total_rows"] =$this->Administration_model->transactions_info_count($institution,$sponsor,$paymentmode,$start,$end);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['transactions'] = $this->Administration_model->transactions_info($institution,$sponsor,$paymentmode,$start,$end,$page,$limit);

           $this->data['title']="Transactions";
           $this->data['institutions']=$this->Administration_model->institutions();
           $this->data['sponsors']=$this->Administration_model->sponsors();
           $this->data['paymentModes']=$this->Administration_model->paymentModes();
           $this->data['userInfo']=$this->Administration_model->current_user_info();
           $this->data['content']='administrator/transactions';
           $this->load->view('administrator/template',$this->data);
   }
   
	public function transaction_invoices(){
        $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $this->data['side_menu']=array(
                                      array('title'=>'Transactions','link'=>'Administration/transactions'),
                                      array('title'=>'Invoices','link'=>'Administration/transaction_invoices'),
                                      array('title'=>'Cancelled Receipts','link'=>'Administration/cancelled_receipts'),
                                  );
           
           $this->form_validation->set_rules('start','Start Date','required|xss_clean|callback_start');
           $this->form_validation->set_rules('end','End Date','required|xss_clean|callback_end');
           $this->form_validation->set_rules('institution','Institution','xss_clean|required');
           
            if ($this->form_validation->run() == TRUE)
           {

                $start=$this->input->post('start');
                $end=$this->input->post('end');
                $institution=$this->input->post('institution');
                
                $data=$this->Administration_model->invoice_transactions($start,$end,$institution);
               
                require_once 'reports/transaction_invoice_pdf.php';
            }
           
           $this->data['title']="Transaction Invoices";
           $this->data['institutions']=$this->Administration_model->institutions();
           $this->data['userInfo']=$this->Administration_model->current_user_info();
           $this->data['content']='administrator/transaction_invoices';
           $this->load->view('administrator/template',$this->data);
   }
   
   public function cancelled_receipts(){
        if (!$this->check_session_account_validity())
           {
                   //redirect them to the login page
                   redirect('auth/logout', 'refresh');
           }
           
            $this->data['side_menu']=array(
                                      array('title'=>'Transactions','link'=>'Administration/transactions'),
                                      array('title'=>'Invoices','link'=>'Administration/transaction_invoices'),
                                      array('title'=>'Cancelled Receipts','link'=>'Administration/cancelled_receipts'),
                                  );

           if ($this->input->post('institution')) {
               $key['institution'] = $this->input->post('institution');
               $this->data['institution']=$this->input->post('institution');
           }
           
            if ($this->input->post('sponsor')) {
               $key['sponsor'] = $this->input->post('sponsor');
               $this->data['sponsor']=$this->input->post('sponsor');
           }
           
           if ($this->input->post('paymentmode')) {
               $key['paymentmode'] = $this->input->post('paymentmode');
               $this->data['paymentmode']=$this->input->post('paymentmode');
           }
           
           if ($this->input->post('start')) {
               $key['start'] = $this->input->post('start');
               $this->data['start']=$this->input->post('start');
           }
           
           if ($this->input->post('end')) {
               $key['end'] = $this->input->post('end');
               $this->data['end']=$this->input->post('end');
           }
           
          

           if ($this->uri->segment(3)) {
               $exp = explode("_", $this->uri->segment(3));

               $key['institution'] = $exp[1];
               $this->data['institution']=$key['institution'];
               
               $key['sponsor'] = $exp[3];
               $this->data['sponsor']=$key['sponsor'];
               
               $key['paymentmode'] = $exp[5];
               $this->data['paymentmode']=$key['paymentmode'];
               
               $key['start'] = $exp[7];
               $this->data['start']=$key['start'];
               
               $key['end'] = $exp[9];
               $this->data['end']=$key['end'];
               
               $docType = $exp[11];
               $this->data['docType']=$docType;
           }

           $institution =$key['institution'];
           $sponsor =$key['sponsor'];
           $paymentmode =$key['paymentmode'];
           $start =$key['start'];
           $end =$key['end'];

           $config["base_url"] = base_url() . "index.php/Administration/cancelled_receipts/institution_".$key['institution']."_sponsor_".$key['sponsor']."_paymentmode_".$key['paymentmode']."_start_".$key['start']."_end_".$key['end']."/";
           $config["total_rows"] =$this->Administration_model->cancelled_receipts_info_count($institution,$sponsor,$paymentmode,$start,$end);
           $config["per_page"] =30;
           $config['full_tag_open'] = '<ul class="pagination">';
           $config['full_tag_close'] = '</ul>';

           $config['num_tag_open'] = '<li>';
           $config['num_tag_close'] = '</li>';


           $config['prev_tag_open'] = '<li>';
           $config['prev_tag_close'] = '</li>';

           $config['next_tag_open'] = '<li>';
           $config['next_tag_close'] = '</li>';

           $config['next_link']='&raquo;';
           $config['prev_link']='&laquo;';
           $config['cur_tag_open'] = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="btn btn-primary">';
           $config['cur_tag_close'] = '</span>';
           $config["uri_segment"] =4;
           $config["num_links"] = 3; // round($choice);

           $this->pagination->initialize($config);
           $page =($this->uri->segment(4)) ? $this->uri->segment(4) : 0;
           $limit=$config["per_page"];
           $this->data['per_page']=$page;
           $this->data['links'] = $this->pagination->create_links();
           $this->data['transactions'] = $this->Administration_model->cancelled_receipts_info($institution,$sponsor,$paymentmode,$start,$end,$page,$limit);

           $this->data['title']="Cancelled Receipts";
           $this->data['institutions']=$this->Administration_model->institutions();
           $this->data['sponsors']=$this->Administration_model->sponsors();
           $this->data['paymentModes']=$this->Administration_model->paymentModes();
           $this->data['userInfo']=$this->Administration_model->current_user_info();
           $this->data['content']='administrator/cancelled_receipts';
           $this->load->view('administrator/template',$this->data);
   }
   
	public function profile(){
             if (!$this->check_session_account_validity())
		{
			//redirect them to the login page
			redirect('auth/logout', 'refresh');
		}
                
                $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Administration/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Administration/change_password'),
                                        );
                
                $this->data['title']="User Profile";
                $this->data['content']='administrator/profile';
                $this->data['userInfo']=$this->Administration_model->current_user_info();
                $this->data['profile']=$this->Administration_model->profile_data();
		$this->load->view('administrator/template',$this->data);
        }
       
    public function edit_profile(){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }

            $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Administration/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Administration/change_password'),
                                        );
            
            $this->form_validation->set_rules('first_name','First Name','required|xss_clean');
            $this->form_validation->set_rules('middle_name','Middle Name','xss_clean');
            $this->form_validation->set_rules('last_name','Last Name','required|xss_clean');
            $this->form_validation->set_rules('username','Username','required|xss_clean|min_length[6]');
            $this->form_validation->set_rules('email', 'Email Address', 'valid_email|xss_clean');
            $this->form_validation->set_rules('mobile', 'Mobile Number', 'required|xss_clean');

            if($this->form_validation->run() == true){

                 $firstname=$this->input->post('first_name');
                 $middlename=$this->input->post('middle_name');
                 $lastname=$this->input->post('last_name');
                 $username=$this->input->post('username');
                 $email=$this->input->post('email');
                 $mobile=$this->input->post('mobile');

                $user=array(
                            'FIRST_NAME'=>$firstname,
                            'MIDDLE_NAME'=>$middlename<>null?$middlename:null,
                            'LAST_NAME'=>$lastname,
                            'USERNAME'=>$username,
                            'EMAIL'=>$email<>null?$email:null,
                            'MSISDN'=>$mobile,
                        );

                $user_group=array(
                            'GROUP_ID'=>$this->session->userdata('group')
                        );

                $username_check=$this->username_uniqueness($username,$this->session->userdata('user_id'));
                if($username_check){


                    //do registration
                     $resp=$this->Administration_model->system_user_registration($user,$user_group,$this->session->userdata('user_id'),null);

                       if($resp){

                           redirect('Administration/profile','refresh');
                       }else{

                           $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">registration failed!</div>';
                       }
                }else{
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">username exists already!</div>';
                }
            }

            $this->data['title']="Edit Profile";
            $this->data['content']='administrator/edit_profile';
            $this->data['userInfo']=$this->Administration_model->current_user_info();
            $this->data['profile']=$this->Administration_model->profile_data();
            $this->load->view('administrator/template',$this->data);
    }
        
    public function change_password(){
         $this->form_validation->set_error_delimiters('<div style="color:red" >', '</div>');
         if (!$this->check_session_account_validity())
            {
                    //redirect them to the login page
                    redirect('auth/logout', 'refresh');
            }

            $this->data['side_menu']=array(
                                           array('title'=>'Edit Profile','link'=>'Administration/edit_profile'),
                                           array('title'=>'Change Password','link'=>'Administration/change_password'),
                                        );

            $this->form_validation->set_rules('cur_password', 'Current Password', 'required|xss_clean|callback_cur_password'); 
            $this->form_validation->set_rules('password', 'New Password', 'xss_clean|required|min_length[' . $this->config->item('min_password_length', 'ion_auth') . ']|max_length[' . $this->config->item('max_password_length', 'ion_auth') . ']|matches[conf_password]');
            $this->form_validation->set_rules('conf_password', 'New Password Confirmation', 'required'); 

            if($this->form_validation->run() == true){

                $password=$this->input->post('password');
                $pchange=$this->Administration_model->change_password($password);

                if($pchange){
                           
                        $this->data['message']='<div class="alert alert-success " role="alert" style="text-align:center;">password change successfull!</div>';
                }else{
                    $this->data['message']='<div class="alert alert-danger " role="alert" style="text-align:center;">password change failed!</div>';
                }
            }

            $this->data['title']="Change Password";
            $this->data['content']='administrator/change_password';
            $this->data['userInfo']=$this->Administration_model->current_user_info();
            $this->data['profile']=$this->Administration_model->profile_data();
            $this->load->view('administrator/template',$this->data);
    }
    
    function cur_password($password){
            
            $pf=$this->Administration_model->profile_data();
            $hashed_old=$this->Administration_model->hash_db_password($password,$pf);
           
             if($pf->password <> $hashed_old){
                 
                 $this->form_validation->set_message("cur_password","The %s is incorrect");
                  return FALSE;
             }
            
            return TRUE;
    }
    
    function check_session_account_validity(){
            
             if (!$this->ion_auth->logged_in())
		{
			return FALSE;
		}
                
                if(!$this->ion_auth->in_group('Administrator')){
                    
                    return FALSE;
                }
                
                return TRUE;
        }
    
    function username_uniqueness($username,$id){
            
        return $this->Administration_model->check_username($username,$id);
    }
    
	function start($date){
        
            if (preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $date)) {
                $date_array = explode("-", $date);
                if (checkdate($date_array[1], $date_array[2], $date_array[0])) {
                    return TRUE;
                } else {
                    $this->form_validation->set_message('start', "The %s must contain YYYY-MM-DD");
                    return FALSE;
                }
            } else {
                $this->form_validation->set_message('start', "The %s must contain YYYY-MM-DD");
                return FALSE;
            }
    }
    
    function end($date){
        
            if (preg_match("/^[0-9]{4}-[0-9]{1,2}-[0-9]{1,2}$/", $date)) {
                $date_array = explode("-", $date);
                if (checkdate($date_array[1], $date_array[2], $date_array[0])) {
                    return TRUE;
                } else {
                    $this->form_validation->set_message('end', "The %s must contain YYYY-MM-DD");
                    return FALSE;
                }
            } else {
                $this->form_validation->set_message('end', "The %s must contain YYYY-MM-DD");
                return FALSE;
            }
    }
	
    function hash_api_password($password){
        
        $strlen=strlen($password);
        $length=$strlen-$this->config->item('offset');
        
        $salt=substr($this->config->item('salt'),$this->config->item('offset'), $length);
        
        return md5($password.$salt);
    }
    
    function firstDay($month,$year){
        if (empty($month)) {
            
          $month = date('m');
       }
       
       if (empty($year)) {
           
          $year = date('Y');
       }
       
       $result = strtotime("{$year}-{$month}-01");
       return date('Y-m-d', $result);
    } 
    
    function lastday($month, $year){
        if (empty($month)) {
            
           $month = date('m');
        }
        
        if (empty($year)) {
            
           $year = date('Y');
        }
        
        $result = strtotime("{$year}-{$month}-01");
        $result = strtotime('-1 second', strtotime('+1 month', $result));
        return date('Y-m-d', $result);
    }
}