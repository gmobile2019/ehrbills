<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Api extends CI_Controller {

    function __construct() {
        parent::__construct();
       
        $this->load->model('Api_model');
        
    }
    
    function receive_transaction(){
        $destination="./logs/Transaction_".date('Y-m-d').'.log'; 
        $request =file_get_contents('php://input');
        error_log("Request : $request ".date('Y-m-d H:i:s')."\n", 3, $destination);
        
        $method=$_SERVER['REQUEST_METHOD'];
        
        if(strtolower($method) === 'post'){
            
            $sve=$this->Api_model->save_incoming_request($request);
            
            if(!$sve){
                
                //internal server error
                error_log("Database Error : Unable to save ".date('Y-m-d H:i:s')."\n", 3, $destination);
                $errorcode=105;
                
            }else{
                
                $errorcode=$this->process_request($request);
            }
              
        }else{
            
            error_log("Invalid Http Method : $method ".date('Y-m-d H:i:s')."\n", 3, $destination);
            $errorcode=101;
            
        }
        
        
            $details=$this->Api_model->get_response_details($errorcode);
            
            $response="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
            $response.="<response><code>".$details->code."</code><status>".$details->status."</status><description>".$details->description."</description></response>";
            
            error_log("Response : $response ".date('Y-m-d H:i:s')."\n", 3, $destination);
            ob_end_clean();
            
            echo $response;
            exit;
    }
    
	public function process_request($request){
        
        $destination="./logs/Transaction_".date('Y-m-d').'.log'; 
        $entityBody =file_get_contents('php://input');
        
        $xml=$this->validate_xml($request);
        
        
        if(!$xml){
            //invalid xml request
            return 102;
        }
        
        $accesscode=(string)$xml->credential->accesscode;
        $username=(string)$xml->credential->username;
        $password=(string)$xml->credential->password;
        $transactionid=(string)$xml->transaction->transactionid;
        $receiptno=(string)$xml->transaction->receiptno;
        $sponsor=(string)$xml->transaction->sponsor;
        $paymentmode=(string)$xml->transaction->paymentmode;
        $amount=(float)$xml->transaction->amount;
        $billtimestamp=(string)$xml->transaction->billtimestamp;
        
        //get timekey
        $timekey=$this->Administration_model->access_credential(null,$accesscode);
        
        if($timekey == null){
            //invalid access credentials
            error_log("time key unavailable ".date('Y-m-d H:i:s')."\n", 3, $destination);
            return 103;
        }
        
        //verify access token
        $access=$this->Api_model->check_access_credentials(trim($accesscode),$timekey[0]->timekey,trim($username),$this->hash_password(trim($password)));
        
        if(!$access){
            //invalid access credentials
            error_log("invalid access credentials ".date('Y-m-d H:i:s')."\n", 3, $destination);
            return 103;
        }
        
        $service=str_replace($this->config->item('spcl_characters_replace'),$this->config->item('spcl_characters'),(string)$xml->transaction->service);
             
        $duplicate=$this->Api_model->check_transaction($transactionid);
        
        if($duplicate){
            
            //internal server error
            error_log("Duplicate transaction : Unable to save ".date('Y-m-d H:i:s')."\n", 3, $destination);
            return 107;
        }
        
		$txn=array(
            'institutioncode'=>$accesscode,
            'transactionid'=>$transactionid,
            'receiptno'=>$receiptno,
            'service'=>$service,
            'sponsor'=>$sponsor,
            'paymentmode'=>$paymentmode,
            'amount'=>$amount,
            'billtimestamp'=>$billtimestamp,
            'createdon'=>date('Y-m-d H:i:s'),
        );
		
        $sve=$this->Api_model->save_transaction($txn);
        
        
        if(!$sve){
            
            //internal server error
            error_log("Database Error : Unable to save ".date('Y-m-d H:i:s')."\n", 3, $destination);
            return 105;
        }
        
        //success
        error_log("successfull ".date('Y-m-d H:i:s')."\n", 3, $destination);
        return 100;
    }
    
    function hash_password($password){
        
        $strlen=strlen($password);
        $length=$strlen-$this->config->item('offset');
        
        $salt=substr($this->config->item('salt'),$this->config->item('offset'), $length);
        
        return md5($password.$salt);
    }
    
    function validate_xml($request){
        $destination="./logs/Transaction_".date('Y-m-d').'.log';
        libxml_use_internal_errors(true);
       
        $xml = simplexml_load_string($request);
      
        if (!$xml) {
            error_log("Xml invalid ".date('Y-m-d H:i:s')."\n", 3, $destination);
            $errors = libxml_get_errors();
            
            foreach ($errors as $error) {
                
                error_log("Error : $error->message ".date('Y-m-d H:i:s')."\n", 3, $destination);
               
            }

            libxml_clear_errors();
            return FALSE;
        }
        error_log("Xml Valid ".date('Y-m-d H:i:s')."\n", 3, $destination);
        return $xml;
    }
	
	/**************************************cancelled receipt sync**********************************************************************************************/
    
     function receive_receipt_transaction(){
        $destination="./logs/Transaction_".date('Y-m-d').'.log'; 
        $request =file_get_contents('php://input');
        error_log("Request : $request ".date('Y-m-d H:i:s')."\n", 3, $destination);
       
        $method=$_SERVER['REQUEST_METHOD'];
        
        if(strtolower($method) === 'post'){
            
            $sve=$this->Api_model->save_incoming_request($request);
            
            if(!$sve){
                
                //internal server error
                error_log("Database Error : Unable to save ".date('Y-m-d H:i:s')."\n", 3, $destination);
                $errorcode=105;
                
            }else{
                
                $errorcode=$this->process_receipt_request($request);
            }
              
        }else{
            
            error_log("Invalid Http Method : $method ".date('Y-m-d H:i:s')."\n", 3, $destination);
            $errorcode=101;
            
        }
        
        
            $details=$this->Api_model->get_response_details($errorcode);
            
            $response="<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
            $response.="<response><code>".$details->code."</code><status>".$details->status."</status><description>".$details->description."</description></response>";
            
            error_log("Response : $response ".date('Y-m-d H:i:s')."\n", 3, $destination);
            ob_end_clean();
            
            echo $response;
            exit;
    }
    
    public function process_receipt_request($request){
        
        $destination="./logs/Transaction_".date('Y-m-d').'.log'; 
        $entityBody =file_get_contents('php://input');
        
        $xml=$this->validate_xml($request);
        
        
        if(!$xml){
            //invalid xml request
            return 102;
        }
        
        $accesscode=(string)$xml->credential->accesscode;
        $username=(string)$xml->credential->username;
        $password=(string)$xml->credential->password;
        $transactionid=(string)$xml->transaction->transactionid;
        $receiptno=(string)$xml->transaction->receiptno;
        $timestamp=(string)$xml->transaction->timestamp;
        
        //get timekey
        $timekey=$this->Administration_model->access_credential(null,$accesscode);
        
        if($timekey == null){
            //invalid access credentials
            error_log("time key unavailable ".date('Y-m-d H:i:s')."\n", 3, $destination);
            return 103;
        }
        
        //verify access token
        $access=$this->Api_model->check_access_credentials(trim($accesscode),$timekey[0]->timekey,trim($username),$this->hash_password(trim($password)));
        
        if(!$access){
            //invalid access credentials
            error_log("invalid access credentials ".date('Y-m-d H:i:s')."\n", 3, $destination);
            return 103;
        }
        
        $service=str_replace($this->config->item('spcl_characters_replace'),$this->config->item('spcl_characters'),(string)$xml->transaction->service);
             
        $duplicate=$this->Api_model->check_receipt_transaction($transactionid);
        
        if($duplicate){
            
            //internal server error
            error_log("Duplicate transaction : Unable to save ".date('Y-m-d H:i:s')."\n", 3, $destination);
            return 107;
        }
        
        $txn=array(
            'institutioncode'=>$accesscode,
            'transactionid'=>$transactionid,
            'receiptno'=>$receiptno,
            'service'=>$service,
            'cancelltimestamp'=>$timestamp,
            'createdon'=>date('Y-m-d H:i:s'),
        );
        
        $sve=$this->Api_model->save_receipt_transaction($txn);
        
        
        if(!$sve){
            
            //internal server error
            error_log("Database Error : Unable to save ".date('Y-m-d H:i:s')."\n", 3, $destination);
            return 105;
        }
        
        //success
        error_log("successfull ".date('Y-m-d H:i:s')."\n", 3, $destination);
        return 100;
    }
    
/*********************************************************************************************************************************************************/
	
    function populate_invoice_table(){
        
        $month=date('Y-m', strtotime(date('Y-m')." -1 month"));
        $month=explode("-", $month);
        $month=$month[1];

        $start_date=$this->firstDay($month,null);
        $end_date=$this->lastday($month,null);
        
        $institutions=$this->Administration_model->institutions(null,null,1);
        
        foreach($institutions as $key=>$value){
            
            $array=array(
                'startdate'=>$start_date.' 00:00:00',
                'enddate'=>$end_date.' 23:59:59',
                'institution'=>$value->code,
                'lastupdate'=>date('Y-m-d H:i:s'),
                'status'=>2
            );
            
            $this->Api_model->save_data($array);
        }
    }
    
    function generate_invoice(){
        $this->load->library('email'); 
            
        $pendings=$this->Api_model->pending_invoices();
        
        foreach($pendings as $key=>$value){
            
            $start_date=$value->startdate;
            $end_date=$value->enddate;
            $institution=$value->institution;
            
            $invoiceNo='ATL-'.$institution.'-'.date('YmdHis');
            $filename='Invoice-'.$invoiceNo.'.pdf';
            $institution=$value->institution;
            
            $lst_txn=$this->Api_model->transactions(null,$institution,null,null,$start_date,null,null);
            
            
            if($lst_txn->billtimestamp < $value->enddate){
                continue;
            }
            
           
            $data=$this->Administration_model->invoice_transactions($start_date,$end_date,$institution);
            
            require_once 'reports/transaction_auto_invoice_pdf.php';
			
            $this->email->clear();
            $recepients=array();
           
            $emails=$this->Administration_model->invoice_emails(null,null,$institution,1);
           
            if($emails == null){
                continue;
            }
            
            foreach($emails as $em=>$email){
                
                $recepients[]=$email->email;
            }
           
            $infodate=date('Y-F', strtotime(date('Y-m')." -1 month"));
            $infodate=explode("-", $infodate);
            $fulltextualmonth=$infodate[1];
            $year=$infodate[0];
            
            $this->email->from($this->config->item('from_email_address'),$this->config->item('from_name'),$this->config->item('failure_reply_email_address'));
            $this->email->reply_to($this->config->item('reply_to_email_address'),$this->config->item('reply_to_name'));
            $this->email->to($recepients);
            $this->email->cc($this->config->item('cc_email_addresses'));
            $this->email->subject("ALLSEE ELECTRONIC HEALTH RECORD COLLECTIONS INVOICE");
            $this->email->message("Hello,\nAttached is an invoice for collections  of $fulltextualmonth $year.\nPlease do the payment before 7th ".date('F')." ".date('Y').".\nThank you.\n\n\nRegards,\nAnsel Missango,\nCEO,\nMobile:+255683369996.");
            $this->email->attach('./invoices/'.$filename);
            //$this->email->print_debugger(array('headers'));
            $send=$this->email->send(FALSE);
            
            if($send){
                
                $this->Api_model->update_status($value->id,$filename);
            }
            
            exit;
        }
    }
    
    function firstDay($month,$year){
        if (empty($month)) {
            
          $month = date('m');
       }
       
       if (empty($year)) {
           
          $year = date('Y');
       }
       
       $result = strtotime("{$year}-{$month}-01");
       return date('Y-m-d', $result);
    } 
    
    function lastday($month, $year){
        if (empty($month)) {
            
           $month = date('m');
        }
        
        if (empty($year)) {
            
           $year = date('Y');
        }
        
        $result = strtotime("{$year}-{$month}-01");
        $result = strtotime('-1 second', strtotime('+1 month', $result));
        return date('Y-m-d', $result);
    }
}