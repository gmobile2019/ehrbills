-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jul 06, 2016 at 09:05 PM
-- Server version: 5.6.21
-- PHP Version: 5.5.19

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `allsee_ehr_billing`
--
CREATE DATABASE IF NOT EXISTS `allsee_ehr_billing` DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci;
USE `allsee_ehr_billing`;

-- --------------------------------------------------------

--
-- Table structure for table `api_credentials`
--

DROP TABLE IF EXISTS `api_credentials`;
CREATE TABLE IF NOT EXISTS `api_credentials` (
`id` int(11) NOT NULL,
  `accesskey` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `username` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `institutioncode` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `timekey` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `createdby` int(11) NOT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `modifiedby` int(11) DEFAULT NULL
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `api_credentials`
--

INSERT INTO `api_credentials` (`id`, `accesskey`, `username`, `password`, `institutioncode`, `status`, `timekey`, `createdon`, `createdby`, `modifiedon`, `modifiedby`) VALUES
(1, '0ebb1529d4a2d1aa4643d93ce8fcdd02', 'ndolage', 'c8d6fd48afee2cb19475b36f81f3f9ae', '101', 1, '20160706115712', '2016-07-05 09:53:41', 1, '2016-07-06 12:07:56', 1);

-- --------------------------------------------------------

--
-- Table structure for table `api_requests`
--

DROP TABLE IF EXISTS `api_requests`;
CREATE TABLE IF NOT EXISTS `api_requests` (
`id` int(11) NOT NULL,
  `request` text COLLATE utf8_unicode_ci NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=10 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `api_requests`
--

INSERT INTO `api_requests` (`id`, `request`, `createdon`) VALUES
(1, '<?xml version="1.0" encoding="UTF-8"?>\n    <request>\n        <credential>\n            <accesscode>101</accesscode>\n            <username>ndolage</username>\n            <password>21121990</password>\n        </credential>\n        <transaction>\n            <transactionid>A1</transactionid>\n            <service>consultation</service>\n            <sponsor>201</sponsor>\n            <paymentmode>301</paymentmode>\n            <amount>1000</amount>\n            <billtimestamp>2016-07-06 11:58:58</billtimestamp>\n        </transaction>\n    </request>', '2016-07-06 08:59:06'),
(2, '<?xml version="1.0" encoding="UTF-8"?>\n    <request>\n        <credential>\n            <accesscode>101</accesscode>\n            <username>ndolage</username>\n            <password>21121990</password>\n        </credential>\n        <transaction>\n            <transactionid>A1</transactionid>\n            <service>consultation</service>\n            <sponsor>201</sponsor>\n            <paymentmode>301</paymentmode>\n            <amount>1000</amount>\n            <billtimestamp>2016-07-06 11:58:58</billtimestamp>\n        </transaction>\n    </request>', '2016-07-06 09:02:04'),
(3, '<?xml version="1.0" encoding="UTF-8"?>\n    <request>\n        <credential>\n            <accesscode>101</accesscode>\n            <username>ndolage</username>\n            <password>21121990</password>\n        </credential>\n        <transaction>\n            <transactionid>A1</transactionid>\n            <service>consultation</service>\n            <sponsor>201</sponsor>\n            <paymentmode>301</paymentmode>\n            <amount>1000</amount>\n            <billtimestamp>2016-07-06 11:58:58</billtimestamp>\n        </transaction>\n    </request>', '2016-07-06 09:03:27'),
(4, '<?xml version="1.0" encoding="UTF-8"?>\n    <request>\n        <credential>\n            <accesscode>101</accesscode>\n            <username>ndolage</username>\n            <password>21121990</password>\n        </credential>\n        <transaction>\n            <transactionid>A1</transactionid>\n            <service>consultation</service>\n            <sponsor>201</sponsor>\n            <paymentmode>301</paymentmode>\n            <amount>1000</amount>\n            <billtimestamp>2016-07-06 11:58:58</billtimestamp>\n        </transaction>\n    </request>', '2016-07-06 09:04:16'),
(5, '<?xml version="1.0" encoding="UTF-8"?>\n    <request>\n        <credential>\n            <accesscode>101</accesscode>\n            <username>ndolage</username>\n            <password>21121990</password>\n        </credential>\n        <transaction>\n            <transactionid>A1</transactionid>\n            <service>consultation</service>\n            <sponsor>201</sponsor>\n            <paymentmode>301</paymentmode>\n            <amount>1000</amount>\n            <billtimestamp>2016-07-06 11:58:58</billtimestamp>\n        </transaction>\n    </request>', '2016-07-06 09:08:29'),
(6, '<?xml version="1.0" encoding="UTF-8"?>\n    <request>\n        <credential>\n            <accesscode>101</accesscode>\n            <username>ndolage</username>\n            <password>21121990</password>\n        </credential>\n        <transaction>\n            <transactionid>A1</transactionid>\n            <service>consultation</service>\n            <sponsor>201</sponsor>\n            <paymentmode>301</paymentmode>\n            <amount>1000</amount>\n            <billtimestamp>2016-07-06 11:58:58</billtimestamp>\n        </transaction>\n    </request>', '2016-07-06 09:09:05'),
(7, '<?xml version="1.0" encoding="UTF-8"?>\n    <request>\n        <credential>\n            <accesscode>101</accesscode>\n            <username>ndolage</username>\n            <password>21121990</password>\n        </credential>\n        <transaction>\n            <transactionid>A1</transactionid>\n            <service>consultation</service>\n            <sponsor>201</sponsor>\n            <paymentmode>301</paymentmode>\n            <amount>1000</amount>\n            <billtimestamp>2016-07-06 11:58:58</billtimestamp>\n        </transaction>\n    </request>', '2016-07-06 09:10:15'),
(8, '<?xml version="1.0" encoding="UTF-8"?>\n    <request>\n        <credential>\n            <accesscode>101</accesscode>\n            <username>ndolage</username>\n            <password>21121990</password>\n        </credential>\n        <transaction>\n            <transactionid>A1</transactionid>\n            <service>consultation</service>\n            <sponsor>201</sponsor>\n            <paymentmode>301</paymentmode>\n            <amount>1000</amount>\n            <billtimestamp>2016-07-06 11:58:58</billtimestamp>\n        </transaction>\n    </request>', '2016-07-06 09:16:12'),
(9, '<?xml version="1.0" encoding="UTF-8"?>\n    <request>\n        <credential>\n            <accesscode>101</accesscode>\n            <username>ndolage</username>\n            <password>21121990</password>\n        </credential>\n        <transaction>\n            <transactionid>A2</transactionid>\n            <service>consultation</service>\n            <sponsor>201</sponsor>\n            <paymentmode>301</paymentmode>\n            <amount>1000</amount>\n            <billtimestamp>2016-07-06 11:58:58</billtimestamp>\n        </transaction>\n    </request>', '2016-07-06 09:16:22');

-- --------------------------------------------------------

--
-- Table structure for table `api_response_codes`
--

DROP TABLE IF EXISTS `api_response_codes`;
CREATE TABLE IF NOT EXISTS `api_response_codes` (
`id` int(11) NOT NULL,
  `code` int(3) NOT NULL,
  `status` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `description` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `api_response_codes`
--

INSERT INTO `api_response_codes` (`id`, `code`, `status`, `description`) VALUES
(1, 100, 'success', 'successfully received'),
(2, 101, 'invalid_method', 'invalid http method'),
(3, 102, 'bad_request', 'bad xml request'),
(4, 103, 'invalid_credentials', 'invalid token,username or password'),
(5, 104, 'invalid_sender_code', 'invalid or inactive sender code'),
(6, 105, 'server_error', 'internal server error'),
(7, 106, 'credit_not_enough', 'sms balance not enough'),
(8, 107, 'duolicate_entry', 'Duplicate Transaction id');

-- --------------------------------------------------------

--
-- Table structure for table `bills`
--

DROP TABLE IF EXISTS `bills`;
CREATE TABLE IF NOT EXISTS `bills` (
`id` int(11) NOT NULL,
  `institutioncode` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `transactionid` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `service` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `sponsor` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `paymentmode` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `amount` double NOT NULL,
  `billtimestamp` datetime NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bills`
--

INSERT INTO `bills` (`id`, `institutioncode`, `transactionid`, `service`, `sponsor`, `paymentmode`, `amount`, `billtimestamp`, `createdon`) VALUES
(1, '101', 'A1', 'consultation', '201', '301', 1000, '0000-00-00 00:00:00', '2016-07-06 09:10:15'),
(2, '101', 'A2', 'consultation', '201', '301', 1000, '2016-07-06 11:58:58', '2016-07-06 09:16:22');

-- --------------------------------------------------------

--
-- Table structure for table `groups`
--

DROP TABLE IF EXISTS `groups`;
CREATE TABLE IF NOT EXISTS `groups` (
`id` mediumint(8) unsigned NOT NULL,
  `name` varchar(20) NOT NULL,
  `description` varchar(100) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `groups`
--

INSERT INTO `groups` (`id`, `name`, `description`) VALUES
(1, 'Administrator', 'Administrator'),
(2, 'User', 'User');

-- --------------------------------------------------------

--
-- Table structure for table `institutions`
--

DROP TABLE IF EXISTS `institutions`;
CREATE TABLE IF NOT EXISTS `institutions` (
`id` int(11) NOT NULL,
  `code` varchar(3) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal` varchar(50) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `website` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) DEFAULT NULL,
  `modifiedon` datetime DEFAULT NULL,
  `logoname` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `logostring` text COLLATE utf8_unicode_ci
) ENGINE=MyISAM AUTO_INCREMENT=2 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `institutions`
--

INSERT INTO `institutions` (`id`, `code`, `name`, `phone`, `postal`, `email`, `website`, `city`, `createdby`, `createdon`, `modifiedby`, `modifiedon`, `logoname`, `logostring`) VALUES
(1, '101', 'Ndolage', '719186759', '67414', 'kemixenock@gmail.com', NULL, 'Dar es salaam', 0, '2016-07-05 07:46:55', NULL, NULL, '101.jpg', '/9j/4AAQSkZJRgABAQEAYABgAAD/4QBaRXhpZgAATU0AKgAAAAgABQMBAAUAAAABAAAASgMDAAEAAAABAABBAFEQAAEAAAABAQBhAFERAAQAAAABAAAOw1ESAAQAAAABAAAOwwAAAAAAAYagAACxj//bAEMAAgEBAgEBAgICAgICAgIDBQMDAwMDBgQEAwUHBgcHBwYHBwgJCwkICAoIBwcKDQoKCwwMDAwHCQ4PDQwOCwwMDP/bAEMBAgICAwMDBgMDBgwIBwgMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDAwMDP/AABEIAHcCUwMBIgACEQEDEQH/xAAfAAABBQEBAQEBAQAAAAAAAAAAAQIDBAUGBwgJCgv/xAC1EAACAQMDAgQDBQUEBAAAAX0BAgMABBEFEiExQQYTUWEHInEUMoGRoQgjQrHBFVLR8CQzYnKCCQoWFxgZGiUmJygpKjQ1Njc4OTpDREVGR0hJSlNUVVZXWFlaY2RlZmdoaWpzdHV2d3h5eoOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4eLj5OXm5+jp6vHy8/T19vf4+fr/xAAfAQADAQEBAQEBAQEBAAAAAAAAAQIDBAUGBwgJCgv/xAC1EQACAQIEBAMEBwUEBAABAncAAQIDEQQFITEGEkFRB2FxEyIygQgUQpGhscEJIzNS8BVictEKFiQ04SXxFxgZGiYnKCkqNTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqCg4SFhoeIiYqSk5SVlpeYmZqio6Slpqeoqaqys7S1tre4ubrCw8TFxsfIycrS09TV1tfY2dri4+Tl5ufo6ery8/T19vf4+fr/2gAMAwEAAhEDEQA/AP38ooqO6m+z20kmM7FLYyBnA9TQA6YsIXKkBgpwSpYA/QdfpWJ/b06xyCctAyFcHaAuMsdpbBy2xcsAABnqOtcH+0j+1j4I/ZB+Flz4s+J3jXw54N0CFHQXmpziEXkwimnWCBMF5rh44JSkESvK+zCo54r8ev2t/wDgpN4o/wCCj3gtLj4q+MNa/Yt/ZJ8Xq+hWJudEn1Pxv8ZoXube4la1hit3ntLF7FYFe4iSW2WW+EDtfiXy4knd2G7J6n3V+1b/AMF4/AHwN+L2vfDL4ceEviN+0X8V/D9lfXF94Y+HGhvqv9jTWzpF5WoXCBvIQ3EiQu8Mdw8BDrJEr+WHwtC/bA/b3+F+nH4hfEX9nj4a654Dubu4N34J8G6/JP458M2EcrP9tPmMbPUittEdtrayedPNcQKvlEyRR+9fsi/s5/s9fsKfs9S6p8JfD3w48EfDu80tdbvPEdlexy2+o6aizXMN5d6nM7Pc28cc0zrLJNIqRuSrLGeOe/Z0/wCCwv7Nf7WP7Rt38KfAfxX8OeJvG9mty8enxwXCQ6gbV2M32W7eFba8KD95m3ll3IjyrmON5Cx1Em7xOl/Y2/4KcfDD9uCfVNK8H+JvI8Z6CJYtb8G61avo/inw5PCIhcQ3enzL5ga3knjhkmh823875Fcng+/W2sT3moKkYPkZGXMecgqpDAg/MjAsM8YYcivxq/4Lt/thfs56B+3r8M/hrqPgT4t6h8YLPTfskHjP4N64mleNfCP25mFppdpbpzc3F0fMX7PcqvlWupNJD5jXRWvUv2M/+Cy/iP8AZ08OeErH9pjxDpfi/wCG/wASdce3+Hn7QOi2VlY+HPEMM3mTLDrVrG6Po93FIVgdZIQqyRXSvtWymu3CT9WKKwoNcvpbyJPLUKWAkJUPtG8Aj5TwRnHJ7Nx8vzbtJNPYAooopgFFFFABRRRQAUUUUAFFFFABRRRQAUUUjHCnFAC0VG7kIDSO7bCQccUkweiuS0VXMj4Hz/pStI+Cd3SodRJX/AlyRPRVP7VJ/e/SnRzOyNlue3FVzbXHfVLuWqKqiZyw+an7mwfnpOdk/wCr+g1q2iemXEnlQO+VG1Sct0HHf2qJ3cLneBx7UI7lfv0o1E7W/wCG9RJ62Oe8MeI9bub/AFCLVLTT4IYbyKKxkgu2lkubcwQF5JkMSiCUTySJ5aNMmxY280F2SLqaopbLAq+WNpQYByc49z1P49alS5KkBjkk1d76LUSld2RZoqNnI70sbFlJJpKV20DkPoqMM3c0rOQDzTbDmH0VA0rbTg84qN5pVVTu/Skpapdylq7It0VDJIwjUhuvtSzORGSDggVQMloqklzKGG5s/hUgmcsMNx9KlSTbXYmMk1cs0VCJWLDn9KQTNk5PSqBy2J6KhSVmYc8U5XY4+tRGomm+xT0aXckoopGOFP0p82oC0VA0rdM4NBkfj5qUZ8za7A9HZk9FQ+a3rTZJnEbYODg44pqV9g9SxRVQSS+cmZMqecYHNOW4bpu6timpImMr9CzRRSMcKTTKFoqu8z+WxDdPakaVwkZ3YyRn3qVL3eYnm1syzRUBdkyS456cUoZthO/t6VKqJlPRXJqKYpO0E+lNLsq9elWpJtLuBLRVUzv2b9KdA75yz5B6cU5aEwkpepYoqCWZlPB/SlSVmQnPNLm1SHfqTUVAZmBHPH0pyysWHPelzq9hvTUloooqgCiiigBlzI8VtI0aGV1UlUBALnHA5IHPuRXm3x+/aO8L/sx/CTXvHfxA8R6b4c8GeG7VptV1C8LeVCMBcAqhJZ3wsaLueSR1jRHkdEHpF3IYbOVwyoyISGZSwU46kDkj2FfiN/wUl/aV8D/8FLP2z9W8I+O9bk0b9jT9jzUrS++L969rNBf6j4mkludOs7CB7WOe6mgWWZ7Z1t0gHOoMJXH2OSgDwX9tjxL8RP2jfB+q/t3/AB28B+IPGXwp0XVW034F/DTUdONro0djeSMLXXfEMKykRafIyQSMplL6ncPbwlo7IwFub+Ptz8N/+Cw/7Kuj/Gb9orxx/wAKT8d+AYl0vVviDoumf2/4Qv7LUby4vtK8NLZQyxTtqltbXUkoitPtPkWgU39wLljFbe9eDLn4w/CH9ozWfj3aaV8OvDHwV07xrNZL8a4vH+i6p4cg+EOn3iW8Xg/T9Es2nSMSvaxJE1pDHctdzCFgjyPIPa/+CQv/AATh0L44al4R/aO8V6Nd2Pw48LS38/7P3w21GO5it/AejPeTzprN4l08hm1S8cNdCV5pEO+KZJRstVtc4ud7suKh1Pk346fCHxb8H9d/YV+HWry+Kvh38DfHXxN07w/oPwR12J7m51zRI9asLm71fxVK0qRy6ld3V5FMNN+zPBaRzBVMTxyofvL/AIOCvgX4R8Q/Av8AZL+Gkvh+xg8Ba1+0N4M8LXOhWKmytF0uSzv7U2cKwlPIjEPyIIinl4BUqQCPKv8Ag4VgFt/wU9/4JreRC0Bl+KGSwbBdv7W8OAg5OWYbV3FgCfl5OCB6b/wcOfDHxH+2TpH7O/wN+HXi6y8IfFrxd8RG8WeFb+4uLuzjs00jRtRuJL1bu1ilaKSJp7VEClZPMk3qCitJHal3E2fL/wC0T+yN4J/ZQ/4KAeBtR8Ya5J8dfCvwU1a0+IviDxlYa3YXvxj8A29s2mSRN4hgRftGs6CjtBIJBa/bILWRwkkUFqkd56t/wT6+MGu/tH/tD/H3wn+178R9cvtfuPAOoz2+gafqiWnwk1r4b30sYXW9MuLcRb3TzbiEXs8iTG2uY03F4p2h6v8A4Nn/ANmrwh8L/hF8b/F+leAPB+lLqXxX8R6V4Y13SCNaS68OxixSO30/W5Y1l1DS0uLeRVlbEckkLOwSRZFTyb/gqr/wT28G/sR+APiM1hr3jXwn+yX+0deQab418P8AhzS3v4fhd4je5t7iw8SW1q8Rxp7S2kdteWiPBKTcxRxNKxtYLV2voSe0fs6/tBaR/wAE19asI/C3xC0j4yfsc+J/F0emWfjU+On15fgReNaR28Gi3MpM6/2SzNYxQSySwLapcuZ9xeOSX9NNL1i4vpYSnmtCwVi8sezOcDbtIDBhg5DBSN3Q4wPxek+JfwU/4I6fs8eHT8ULT4LeKPGX7VmnaFoHxP0v4c6u2n+Fp/DKrqlqviu0s7ezLiKayuoxKkEdvDcThzbKrby301/wRM+JviT9nTxXqH7JfjXX9I8aJ4I8O23jD4X+NrHUpL5fHfgu6uXSG43DzI4zaO9taqnmIDE8KxI8UKyPzUqfLUdtjepFKkmfpDRRRXSYBRRRQAUUUUAFFFFABRRRQAUUUUAFI33TS0N0NJuyuCKWswyXOj3UUVxNayvC6pPFs8yFipw671ZMg8jcpXI5BHFfgH/wXE/4KI/trf8ABK/9peDw34W/aG1vxV4N13TIdV0u81HwPoH2m03SSRG1neLTVjlYmJ3WRBGGGQEUod/9AskKzQMrbgHBBKsVP4Ecj6ivhz9oT9mXwX+1h/wUZ8efDvxxosGs+FvFXwOtba9tm4Yf8Tm4ZJI26pJG8cckbDGx41YYIzXJUm004mtJxT9/Y4z/AIIWf8FrdO/4KcfAqTSPENzZW3xd8GWoOuwFRbprECsEXU4I1BxG7FFkRR+6kbGNjxlr/wC2bp/7QXhP9tz4OaJ4J/aT8XaD4G+KHiDUodY0yXwp4dv5dMht7Oa9EFjO2nhgCImhAm+0yr+7ctMSY2/Bn9pn4D/GH/g3k/4KRaXqPh7VvJ+w3Lal4Y1/ayWHiDTgTHJbzKDtJAPlXEOcq2ChCtE5/bP4Lf8ABQ/wN/wUx+J/7HfxE8IySW1zLr+u2Gu6Jd3AkvND1BPD9yJbdwuC4IYSLJgCSNlc7SpirrrQU5U61He2qOGrTnR56cutmn0s/wBTtf8Agr/4h+Mv7J37BXiX4l/Dz9oHx5pfiTwHpVlE8V/oPhm9tdYkNzHHJNco2nIYrqQT8mJ0gRYQRDknPwd/wRc/bN/bb/4K+TfEX/jLc/DdfAKabJ8/ws0HVV1Bbs3XUGOAQ7Rbr1d93mcYwK/Qj/g4itIrL/gjt8aBFGkYGm24AA4A/tG24Hp07V+d3/BkxGt1fftCFlXP2fQ+ihf4tQ9PrWmXqFZ4mNTpFW9dLm+ITjTpTh9rQ6vx3/wW6/a7/wCCO37Tek/Dr9qXSfCvxj8J6xv1Gz8U6Hpw03UtRsWZVD2/lJFAWhYMr281rE+9hmYRvHKf2F+Cv7Q2h/tCfBnQ/Hfg3VIte8MeJbFdR028iQBpo3B2l4mKlMEbWRirpICrhCrKv5df8HjXwk03WP2GPh74plgT7fofjRNPhlHDCK7tLqSZT6hntoW5zgxjGOc+Df8ABDj9rzxF8OP+CAP7Uc1pe3sN98NF1G90SeKby20/7dYrtMRXBQR3KvPx0ZyRiuGlXcsNVml79N2XY3qU71KFRbSbj811Pef+Civ/AAcWeONY/anT9nP9kvQdF8V+P72/bw9P4mvoXubW3vnaWFhZxHbG/kMA7XE2+I+TJiGSMiUereJP2QP+CjWkfBqXUNN/bV8L6l4+W2SYaG3wy0aLSjcgZltlvvs7TlBk4kazVmKplI1dmj/LH/g0j8D2Pjr/AIKoXN/ewJPJ4a8HX9/bh41YJI0lpZs2CMK2y4kAI52sw6Eiv6eobOH7PsQbApx949MEYPPIx6/XrXRUpfVoxvpKeruczrOdWdO3uxdvM/KD/glf/wAHHuqftB/tK3fwF/aI8O6L8M/iwmp3OjWNxYOItLu72Fyr2EySSytDc5jZI3ErRSuWXareUknvH/Bbj4h/Fz9nT9jrxx8Zvhf8bvEvgfUfCFtaPBosWiaFqWj3itdCJmZbqye5EriTHy3O0GIYjySD+Ef/AAcUQH4K/wDBan4g6r4euLvS7y7m0rXIpI2MUsMxsoHDqRzkSKGDdR2xX7af8FmvHNx8Vv8Ag3v8Y+LZxKbvxF4N0fU5ieoNzNYytn0+/wDhzWWJm6mEjjIe627WX9bHVRp8uLWG+y1df8E+Uv8Aggl/wUA/ab/4Kl/ED4gw/EH9pHxdoei+DtOsri2h0Lwv4WiurmS5klwXmm0yRQqJEwI8kBs7w6KpB/bPS7+6hEMU85lyNu9tgaViQcYAAO1ASxGAc8D0/A7/AIMpCbz4gfHUSHhtO0mQ4+X5vtF6O38ulfv5b2kUM0snlqGZixPqx7n1PvXdjKXLUgl/Ld/M8yhNxlUUtbuy+RcdiYiFOGI4Poa8L/4KKftmad+wZ+xx4/8AijqTb/8AhG9Lkl06B2VBeXkgVLaHJH8U7Ip9AxPavap3Z4nVGUOV+XPrX5xf8FPvgVc/8FaPjBqvwBs76SHwt8P/AAvdeKNauYHJibX7qOWHQLWXGCPLH2i7liIwyrHnO5McdeVSKUYLX9D0cPC8pTmtI2/E7j/ggR/wUv1r/gpV+w1pGueLdSt7z4ieFtRl0LxTMkcMP22VcSxXSxxIqRiWJlGAqjckmMYFfc892FtJmLxxhATvf7qe55HH4j61/MH/AMGx37XOofsRf8FJtQ+GHjCW50jTfiHK/ha/t7kkCy1e3kY2xbng+YJ4Tu5zOg/hNf0tfEfxOPAnw81/XJXPl6dp9zfLGByBHCXIJ68FG6eta4uShGNWO1jlwybq1KT3vp6H53/8Fj/+DjHw9/wTs8SSfDXwDo9p8QPjLLBEz2Fw7DTfDfmKxje7aHMk0zExEWiCJjFvdpot0QeL4DfAL/go/wDtCeE5PF/j79qTw58DL/xA326z8H6P8MdK12PR4pFV0tnkuxFIkqksuwz3PC5abPFfih/wSB+0ftw/8Fx/h/q3je5TVb/xH4zuvFWo/a3/AHbzQxTX46cffiWMLjaAQMYAFf1tSW8cNuz28cQYvnKnYCxJOd3uSc4HfvWjiqdKE3rKZVaXJUnyfZ6H5Wfth/8ABQb9tv8A4Jj/AAxk074i+FvAfxY0TW57XRdB+KnhuNtOuNIkllMa3Gr6aySQG6lUoVWFo7SNzt8ycusdfa37emlfES8/Zo8UeKvh98WfFPw11/wf4e1DUrUabpuj6lY6nNHEZIjdLeWM7Mo8s5EEkOBKck4BHpfxS+Gfhf4+/D3U/C/inSbLX9C1e3EF1bXKGMSRFo5VP8DKd6RvuQ5V0UjlRjC/a5tUk/ZA+KcbLGuPB2q/LKTmM/Y5MAgEsAN4OQxx7VyTlajNrc0jH/aoX2STZ+F//BJD/gqP+3B/wVM/akuPhnD+1BF4MktNEn1yTUf+FfaBqaAQPEpiaM29uUDGUZYM5GDjNfWv7fHg3/gol+yp8CfEvjbwF+17pHxXbwJZteeJdGHw00HSdStIEIYzQokV0shEQkd0kaHCJuTzCdlflP8A8G7P7UR/ZR/4KEah4sb4efFb4oOfCt9b/wBieBNC/tzVAZZrY+abXzI/3a7WDNv4MkXHNftN/wAEDv2uL7/gpl4W/aA+IXijSLSwtPFXjo2UWi+cLhbWxh0y2to4HkCRmYCMHMhVdzu/yjjHXo1SktrakV/3cqsI73svmfef7OHjLUvHHwF8C63qt497qet+HbC+vJBHHH5k72sUkjYUBQXZ84AAHYAVP8aPD+reLfh/cWen+ONe8BXRMZGraFFpr3kJRgz7TqNvcW2xlyG3QlgM7SDg10fhfwRpngvwppuiabarb6Xo9tFZ2kBdpBFFHGI0XLElsIAPmJJxzk81eWyjRWwDlhgsWJYj0z1xXHVkvfcdmOmmox5t0fzbfEr/AILMftp+Ff8Agqdd/s8WX7Rl2bCP4hp4Kh1SfwN4f+0CNr/7N5zJ9iwWCurYGA5Xjbmv3R8ffBn4hSfCB9C039oD4n6V4ktJLu4tvER0jw1Nd3TGNjHBcQtpXkPDGw3AW8MchAIeU9a/mX/ao8Wj4Z/8HGeveIf7I17xH/Yfxuj1H+ytGtftupaiY9UDmC1gJHm3Em3aiZG52UZGc1+7l9/wXPSa0Ns37Gn7efyKYxJ/wqUhAdpG8/6Xx1OT1rblbwMJreVzolT5cSoPbRn5g/8ABKT/AILGftmft8/8FD/CPwk8SftC3+jeHtYub06hdad4S8OpcLDbWs0xWF5NOkVWZogoZkcBWLFTjFfuf8e/2p/Df/BP39le78b/ABP8X63qWj+FLJPtupajbWp1XU5iNqRCKzhiha6lcqoRIoo8tn5VRzX823/BtLdtf/8ABar4essckQkbWWCtHyn+gXXy9eMHOO4BYEsBX2V/wenfFLUYNR+B3gGCaaPS7mHUNduULnZPKpgt4MnqWjDTkHPSZvWrm2o0l3WphRTrYirSlsnoeh/se/8ABS79tv8A4Lj/ABn1ef4Q694V/Zp+EHhK7WO81o6DbeIL6bz0Ajtm+2I8VxODE8n7oWyxpKRI0jGBX6/9uj42/t/f8EhvB5+Kt/8AF/wn+058MrRIbLXrTVfAVr4cn0ZpZgPtKLYhWKHCRLcNO6K1x81s6jzV9c/4NY/hxY+Ff+CP/gfVoIUS68Talqur3TBdhlm+0va72Ycs4S3A3HJwa+oP+CoHw707x5/wTr+NmkXcERSTwVq7oFT/AFbJZSum3HI2uqkYxyKxx0vq6caOvLa/oxYNxrz97Zuxy/8AwS9/4Kr+Cf8Agqb8Cv8AhL/CSNpGuaTOLLXvD08gnuNDuDEXUll+ae2k2uYpwkYl2OpWN0eNfmD9oD/gpx8av2yP+ClGs/sp/sy+JfDHw9bwLbfbfGPxE1XTYtYubExTWySw2do48h3VpxCyyKd8u5VaJUEkn5h/8Gmfxe1HwT/wVBi8JxXU7aR448OX+nXdvGW2q9sou45WU5ww8pgrDkea2PvHPT/8Fnf2Qfj1/wAEuf8AgpDr37S/won8Q2fhfxZqcviGHxPpdmL6HQbu9dFubK7jKtH5byu2wyp5M8bois8iuq6VXS9vSknaMk9+6HT5nTqJtc8HZeh+pHxM8b/txf8ABNXwLrXjvV/GfhH9sfwdpVnPqOtaVP4etfAev6XFGjbTYPa+dBOmBJLKsqvKEjTYuWIPvfwV8S/EL9sb9gH4U+KYvih4p+HXizxL4X0/X73V9A0jSWaeW5sVlAlg1C0uYxCWkDhUWJztX51ViK/MT9gX/g7q0zxdqGn+HP2ivCo0CO6cwSeKfDdtLPZswCqqy2I3zBAn+se3eVmdlHlJGNtfsX8D7Hwfov7P3hjTPALabP4Ks9Ft7bw+dGukmsprBYlFuYZQ7q8Zi8t1cSPuXa2TkAmKi6dLnezW/QmjVUp8rVtT8B/+Ce//AAVs/be/bz/b00r4JN+03/wiA1a41G3GsN8OtAvljFpBJMri3aCBjvELKf3p2ls4wK9j/bH/AOCs/wC2t/wRG/a00vw98XvGvgr9oHwN4ltjqOlzz6Hb6DNd2cVwY5I42tYY/s19gZbf9rhCSRlAzfK35+/8EhP2qPBf7G3/AAV80z4hfEnXn8PeCtF1HXft18ltcXQhMsNxAjBLdGmdmlcL8qkgHLEqMV77/wAFiv2kdX/4L9ftieF/DX7MPg7xl8RdC8A2UmnPrVjpktnHczXsqhriYzbUsrZhEqB7sw7mWTICbSKsnTpW7amsUpSqU6j0T0P1a/bX/bX8WfHL/gk3dftKfAj4reJvh7cWPhRvEVhYDR9Ju7a7bzY/Nivo7y0nYPGqzRjyJ4wrhmPnKuB8kf8ABvb/AMFEP2o/+CoHx28XWfxF+O/iK28P+B7GDURZaR4X0CCXU5HuFTyJnOmv+5CJJv8AKaOQmQBXU4I+if2k/wBlSf8AYZ/4NuPGfwuu7uPUdQ8MfD+7+3XSI4t3vppmnuvL81EfyxNLKELjeRtL5Zlz8Rf8GXBXV/jZ8bkuYoJ0k8P6erK0SkMBcTDnjnO0Z9SMnJrWlCFSeKhHpFW9epyqbjhKU5b87v6dD7Q8C6f+3V+2T+1d8a5vDH7Stp8GvhL4R8U3mh+H0l8CaHr9xdPEyBo4o5Iom+zx72UzyXDs0iFQrAMw/Pf9pb/gr9+2/wDs+/8ABRi9/Z9P7Tq6ktt4mtPDQ8Qn4e6BAC0zQ7pjbm1I+VZwdol52dec1/SDovhLTfDdnImn2cNkrySTSCEbPNkdi7yPj7zsxLMxyWLMSSSc/wAo3/BS1B/xEj+IF5x/wtPS+M/7dpXJTi1WhBm8Iy9hWnLdao/Vf9ufwn/wUD/Y0+EMXxD8P/tcWfxU8O+H7yB/FGnn4Y6Fot1ZWbSRI0iERT+ZtDM0uTEY0BcB/u1+sNjdTzyhVbCxybfmGASNvy++QSQRTbvwxp3irw8+n6nY2uo6fdq8E9rcxiWC4jYEFHRsq6kHowIq4lhBp8bOqkCFCV5Z9gAPQZ9zwK6Kk4xlKT6HNBczp1erWp+dH/Bw3/wV38Qf8EyfgN4THgW/tovHni/W4fshnhgmVdNtGSS8kZJFZQJtyQA4yPMJUqwBH2v+zF+0bpP7T/wJ8LfELQZ9+g+L9Jt9Ytd5UNAsqljE3oY2/dtnkMGr8Sf+C3P7Jmsf8FFP2Yfiv+1Jp73N9p3w+8QR6F4StRLut5/DWns9rf3igYBE2os8ocg7YrUnOwqK9Y/4NCv24n+JHwG8TfA3U7ySTU/Ak513RTOpDz6ZdSr5gHzdI52JOOAbmP0ascNB1sLVv8Ss/vOnGSUZ0pUtnoz9V/2g/Afi34l+EUs/CnxJ8V/DrULWQz/b9D0/S72a4PklVheO+s7iJ4txEp8vy5Dt2h1Br8BP+Cfn/BZv9sv9rv8A4KL+FPgvrX7Qeo6XpGsa3c2F3qFn4L8P+csFvFNI5TdYMEkP2cpuZXVTJkqwGD/R/q+mW/8AZ0mYkf5DneN2eOpz3r+S3/ghVO0n/Bev4bREgxp4o1NFUgYUC2vCAPTnrjr3p4C0sYqcui1+4KumGl3vof1b/CXw3rHgLwlbaZrvjDXvGV3byN5mra3b2Nve3JkkZkRxZ21vbkICsS+XEpIQFssST2NV7eyitoz5aLGCd2F4GfXA96lDEkc1UpJDjqPooopgfCP/AAX2/b01H9iP/gl3468S6PrqaD4t8WxweFfDtzbrcvJBdX4/ePBNbFGiuIbNLyaC4MscYlhXO5gqP+W+lajrX7AfhH4efs+/Cb4C/FH4veIPhkbXU/2h/h9qPhmLxZ4Q8YT6/p2lakwkngknht5rF7BIbNvswTMTyq8488XX1j/wUhtNR/bP/wCDgr9nj4f6VYp4p0L9mfw3dfFTV7LwxqdimtW18JI54LOZ7u4SEebc2WhRpAwidkvpJWljjZJovl3/AIJ1/tJ/tcfAb4zfEH4y/tAS/HXRPA3gP4d694s8R6Z8SbK5sPD3jvWBcsLPStNN7awwaQ801zZmKKBJZDNbShVMcwgSZSsjSm0lqj6B+Cn7O3w7/bg/4KHa98BPg3pcfg79kz4Qatp3xK+JuiWtiEh8beMLx4JbfSbqzvZne1sYo7RIprA2sYgm0+7tpYklFvLF+zE2n28Fuxw0SrHtLIxDbRkjkcnGT+Z9a/Dv/gmT/wAFQPHX7In/AAT2S+sv2Q/2qPix8YPiH4gv/iDrWtWHg68h8N+ML3U7wSrqEd6qzmBX0826qILTY5j4AExnr1Uf8HF37Us5CD/gmb8fULcBmn1ZQvuSdEwPrUzlOUFfczNz/g56+P8ApX7Pvwe/Zy8Uvq2k6frvhj42aN4shtDGuoXM0emQ3jzzJZrPbG7jgZoUZRPDtNxGjyw+cHX8xP8Agol+3b8U/wBplfi3+1bpXw7svD/w08a2Ft8GvAmr+LtFM+s6Xo11DqrXX9kyopsma4W31FLqZllktVv/ALNbz7d0g5r4u/8ABKP/AIKJ/wDBTjxZH8WPHvw58X+JNX1W1jtbd9e1LSPDVxZxW7SJFCNLuJrVrSMN5rCMQoGaV5x5gn3yfrx/wcH/APBMzxR+0z/wTh8GfC79n34daJFH4f8AHVtrUui6OdO0Gz02yNpqQnuI0nltrdGe4ul3KCHYzMcEbmNON9zSEopan6J/A39nrwX+zb8IPD3gHwN4esvDnhDwvbJaabptsWMcCKS+4sxLSSNIzSNK5aR5GaRmZ2LHb8XfDzQfGvg/VNC1nRdK1bRNXsZdOvtPvbNLm1vbaRSskEsTArJGyswZCCGBII5r8u/iV+29/wAFVbzxneXfhT9kv4RaJ4Xk2NaWWseKbDUby2ICiSKW6TWLZJDuD4dbdBgoMH759D8PeNf+CoviHTma50n9iDSv9JmtFivh4llkuQkjosy/Z2lQxTKgmjJIYRTIJFjlWSOMjK5meHfADwLrH7F3ib4m/AHWvhB4f/aY+M3w/wBNkvvhhH4pvtMt9S1n4T3lyLCO1XW5LNwr2k893FcWVysMflzEWxkiREPE/wDBTT9q3xJ4W0n4LeLLX9kn9o34Y/Eb9lDVLHxPbL4e0uJvB1voPkQHVNETWdOuJLaPTXtY4g1wbMqsdo1u0MQkm2dt+1Xrf7Zn7N/xj+EH7UPx08W/s0aD8MPghqo0vxV/wgM/iN5dQ0bXr7TtOv2nt5FP2kwbLe4jQOCssG8RzsipXjv7Efx5+Kuo/wDBZr4ueDfiL8VPjP8AtRfDvSbq1+GniTTdJ8DTN4ZnvtQC2dxNq+ktC2madYWbW96lxLDLHLOyxPGLmGS9JtWa0Wo3zPRn7cfCT4xaf8d/h74Y8Z+F75b3wl4usbbV9JvGtpIXv7K5hWeCQI4Dxloyr7ZEU7ZACEcEDt6/Nb/g3rvz8D/Cfxy/Zu1K9124vv2ffinqmk6HYa9phh1O18N3sjXem3Mk620SXP2iX7fOJFySJVf5IZIRX6U0hBRRRQAUUUUAFFFFABRRRQAUUUUAFBGRRRSYEN7J9nspH3KmxCdzNtAwOpPOB74r5S0LUWuf+CxOq/u3JHwcsoyTGcEnWLrPHcDnIB4IYZJIA+q9Vhe40y4SOaW3keJgksSh3iJBwygggkdQCCPY18PSf8Egp7f9p5/jE/7TH7Sa/Ec6MuiPdpfeG1tzp6qw8n7D/Zf2NYjIwdV8rCy/vT+8y9Yqk/aqT2QHdf8ABTP/AIJy+Df+Cl/7MuofDzxNHbWl47m+0HWFjBn0S9UEQXKJx5iFP3UibgWQkAg4YfhP/wAESP2V/HH7En/Bf7wt8L/HNrPpWs6HPqkckUZeS2utmmXYhuom+XekiM7K4UExyMCI97of6ZdHtJLbToYmu5L0oo/fvGFaQYxvKrhC275m2BV/uqOleR/Ej9j3wH8Qf2mvh/8AFu70mODxz8NYbtdM1SGaO1mazu4poWtLglSXtgZDIi8NHKhGQksofopfu63PHsRWftqU6cutrfI8O/4OKcH/AII+fGhWngj32FtGGZj8rfb7djwAT90Gvzs/4Mp7lNM1T9oFGUx3E0Wg+XC7KoC/8TJiTlt3/PMdP4hX6s/t6/8ABOa0/wCCiPhTUPCfij4r/FPw34D1SOKK+8P+F5dKt7W+KSNIJpJJbCa5O47EIEwjIh5Q7mz8yfBj/g1u+FX7Nc93P8N/j7+1b4Cub+JYbl/Dvjay0hrxVOVWVrbTkZwD/eJ6mowMvZ+2b+2tB1veo06a3i7s+Wv+Dwj9rvSte8GfD34K6bqlrda5a3zeLdbt4879PgSN4LXzk2kxtILiV8MT+7jDELvQN9Af8ERP+CU8nwz/AOCP3jHwJ8Q9LvNE1/48wXk2oafOmb3SLG4s44bSKaMlRHMIsTlWwyNcAOE2PGnvv7Jn/BAL9nT9lD4oQeOm8Oa38TPiJDq8usJ4t8eak2tarJdO/mm6dWEduJ1m/eCcQ+dv+bfuOa+3YNNgtIPLijVItgjEYHyBRnAC9B1pOMYUpQW7uxyqOUotLSOvzP5a/wDgjbrF9/wR7/4Lbx+Efi80HhYTm78I313dSmCxVbkxtaXhlOV8iaSGHDsdirJuLJiv6dNT8Rpo+lzXVxd2Vnb20DXF4ZmCC3hAcmR9zfKMISScAYYZ/iXyb9uT/gmL8Ev+Chvhm1tPiv4C07xNd6WjDTtRW4mstQssqw2pcwMsnlktuMTFomZVZkYqMfKF9/wbi+Dtc+E1p4F1X9o79sDVfAtpa21gfDd78RIP7HuraEKYojaNZ+X5SGOJI4+NgjQKVwDWVatKpB0p6tLf1JVlWdToz8dPjz8PdS/4Lr/8F2vFlt8Opze+Gtc1iKGHXPLLQWejWIitJL456RlULRowV3eaGMjc/H7h/wDBe7wvY+Bv+CInxX8P6dDHZ2WleHtPs7OJWcLBBHfWUaJhizHACrliTgZJ7175+yF+wr8Jv2EPBUujfC3wPoXg+xv5Gnu3tpnuru9lXcUFxeTs80vlo0mDK7BBIwXAOK5P9vT/AIJwWX/BRnwdf+FPE/xc+KPh7wFrEUC3nh/wu2j21tdGKV3Esks2nzXR3ZVCvnCP90DsJJzeIjBU44an2/EvDVHLEOvV6bH5P/8ABl1Ktr8Q/js8QWBZtN0fYrKx2ZmvPlBfbuw2MgZPzAZr9u/2kf2jPDP7LvwK8TfEHxhq8Wm+GvC1kdQvrt2QqYyyhAgzgu7ssUabtzswADGvgn4O/wDBq58IP2ddVutQ+H3x4/ap8Dahfw/Zrm68P+M7DTZ7iLcG8t5IdOVmXI+6TjqOhNXvjb/wbE/Dj9o3TrGD4hftE/tc+P4dId57GHxH49s9TS1kdVV2jE2ntsZwihmXk45zzXXWnGrVi3typMwpr2b5nvds+hP2RP2yL3X/APgmT4a+N/xOu4UTUPDdx4s1h2jjQWlm4kuVTbGiqPLhKIFILlQATLJkv8ofszfsF/ta/EPwtq3xa0n9qSP4K3vxuun8Z6r4Yg+FemeIrnSftCRpBave3M3nSPFarboV2osZVgAAK+iP2ov+CPng79qv9lnwZ8EtX+KHxa8N/DbwZo9npUeg+H73TbX+3YbRFSCe832UskwQIhEY2xB4kk2B0Vh7l+zV+zTdfsw/D2HwlP8AEHxv8RNL09IINPl8YjTZ7uxggiCRoj2dpbtcHKrl5/MlJ/izXEowcue73tH08zSNScKSS+07y7+Vj+Zv/gu1/wAE/fiT/wAE5P21dF8YeIfH0PxB1v4jMfE6eKrHQLfw8JNWhnxODawlokdWMUu4EAmckgkOT/Rf/wAE9v2utN/4KFfsO+CfiLG1pNF4t0NV1m0GCLS6AMVzC2OCpmjnBPB2BflGWI8t/wCCiH/BEjwf/wAFLtZtR8TPib8XZdA0a/lvNK0HR5tGtLDS3uFCN5ZGlPO2SAS00z7TlsjJNbX/AATa/wCCOXg7/glpq9/b/Dn4nfGPUdA1l1nvfDGvX2nXWlTT7dn2vyorCOSGbaEUyRyJ5ixRK+5Y0C7UY82HlQq/IrEJxqqtT+JJNn4A3Hwj1X/ggd/wWp8LX3i/TdUPhHw14oXUdO1eWAyjWdAkaWCSeIJtVpUikKuoIImTbsHSv6hYNX8FftI/CiETP4c8feB/GWnkxecINR07WLSWIZwrny5opYWLYAZCjMWwpC1S/ap/Yz+GP7aHwru/B3xR8HaJ4t8Pn99DFfxvvsJQhUTQSxsk0D7cqWhdGKllLYYg/F/gT/g3R8C/CC41lPhx8fP2svhHo2s6lNqh8OeE/iUmnabbSOFAAUW0skhijWOLzJ5JJGWFAzNikqi9koT6Im6lKVZb9T8L/wDgsn8AfCnwg/4LOfELwb4S0qPw34bfxBZz2em6epghsDdW9pcTLHGv+qBa4lcRoAIwyBdqjav9NPxP/Z78C/sz/sF/E/wx4B8E+GvCOgReFNWni0zSdPjtrYymxmTzGVFALMkSFnbLMSSSSTny/wDZs/4IQ/s6fs6+Kl8Y6j4W1L4k/EF7ia7u/F3xB1OXX9QubhrgzJcTJNi1M8bYjS5SATYUEvl3Le2ftZ/sn3v7Xfw4n8Iv8UPiL8PvDep6fcaTq1p4VXS1k1e3uAi/vLm6sbmSJlQSBfszx/67k8Ajnn/B9lT1v16Gs5XxPtXtZL7j+ez/AINKyJf+CpuqvC5eGPwdfjHIAU3VkM7SGBZsKcBT06rjI/f74LfA/wAP/slfEv4z+OZbnRdG8PeO9XXxlqMzz/Z4LF00+GG7uJXlIRQXt3mLswAUyMcZJr45+Fn/AAac/BX4EeK013wP8aP2oPBuuRRNBFqeh+KtO068hiYEPGk0OnK6IwY7lBAPcHArofi//wAG0XgX9oTw1Ho3j79pL9sHxxpFtL9pg0/X/H9rqVrDKFKh0im091VgCQCoyM8V1c6UYwj00InH2leVWWzaf4H17+wb+1pN+2l8DLT4jWkdqnhjxTq2pHw28cZikuNJt7yW2t55FLOTJKIxJj5NqSAMiMMH3C7byInZQCVUnBOM8eteS/sefs0aH+xp+zp4T+GnhuTUbnw/4NsksLO5vpo5bm6UMz+bIyxxJvMjtwqAZYkDnNdH8WPCut/EDwYdL03xxr3ga7uZI0GraBb6dLfRsrgts+321zbFGXIYNAzYztIOKyr6e7DqOOup/Lf8YUST/g5vkG54wPj9aAylSrY/tdMMEIz1/PtX9VFzdO2guMKv+jlg6oSg4fcMD5sgAA89/WvzV8U/8GpfwR8T/F67+IupfGX9pG98a3Gptrkurf29pJupLzzfO80bdL+8JOVVRhTgKB0r7M8a/sreJ/GvwXHgu4+PHxggklnnhvfENlB4dg1a9hmTa9vI6aR9niRASUeGGKUtgNJWjl/slOit4t/iVKV8X7d/DZLz0P5uf+DbQ7v+C2/gNCqs4Osb8ruAYafdZxjkgKBgdF9Otfqt/wAHXn/BOvW/2rv2VfD3xI8K6fdap4h+E7yz39rbjdNLpU6f6RKiAEs0MkUUhUAnyzLjkCus/Z+/4NZfgr+yx8Z9H+IPw++Ln7QPh7xl4fnNxp+oR6voUzQuUZDlJNLdGDKzBlZGDAnINfob4F8NXXh7wPpuna1rup+JrjToY7eTW9RNtFd6iVPMsvkQwQqzH5SscaqQSMYJqaz5qMIw3SJhJQxE6j2kflr/AMGln7Xuj/ET9h67+Eb6lZ2/iz4eajc3K6XJOgaTTrqVZVmi+8WRLiR42+8VLKGIMkYP0v8A8F3/ANs7Rv2WP+CY3xHub/VrK217xpo8/hjQrTzUjmvb28UwERpluY4jNKcsoAjwTuIrN+P/APwb6/AX4o/GFfiB4O/4Tz4H/EH7bNfyeI/hrr8mg3s7yxNFIiIYpoY8xl9z26RvIHcMzB2zk+Gf+Ddb4Ka/8X9P8X/FLxn8aP2hb7TbeO1sYPiX40k1mHToxOsygCFLdmifDIYJfMhbed0bZOVi+XE2ctNEnbuicPCNC0obpt/efAH/AAaG/wDBPbXU+IOv/tE+IrV7TR/7Nn0Lwsk+6KTU7jzVS8uI8jLLAsRhLKWUvMy8NG2P0y/4Jrf8FGvDX7Yviv4qfDPXNX0e4+Jvw08Za7pV/ps0MVtNq+kxX8sNtewwjImiEPlW0rBiUkQCXiRPM+yNC8E6L4J8M2mmaVptlpWlaVbpb2lraQrFDZwxLtjijRRhY414RANqgAAAACvj3xX/AMENP2cviP4Z1NdS8BLY+L7jX7/xPD4vsLw2Pimx1G6mmlS5W+hAdxbvJiCKQSRBYoy0bHOdate8+WUVypK3k+pPs1zOqvie58Lf8HFH/BGL9n3wZ+yV4l+NHgrTPD3ww8WeH0tkMGiyR6bpHiMPcW8P2YWvMcNxsRzG0O3e6yiQMDvj+qf+DcTw54n8Of8ABIP4eJ4miY3F4dRn00XSMGOnSXEk8DIGG7y5CzyqpHKShwAoCVa0j/ggF8K9b8eeHtc+KnxH+PP7QltoQeTTtL+KHjmTWdP0yYvCxuo440gfkReW6PuicSgyI+xcfVfxj+BWr/E/wfZ+HvDnj3xd8L7S3kEMl34Sh0tbtoTCYvIQ3ljcxxIARIGiCOuxQGA4rBqXsZq9+bZdEXUblUjL+RK/n6H81v8AwbvxBv8AgvX4XZJFKy3mv7XZyvmD7JecKxBL/KMEHr355r+qj+zoUKOsW+aJSykkli23bnJ6kgAZPUV+aHwG/wCDVv4NfsufGzTfiT4E+MH7ROgeNtGuXvLTVP7W0OZklZXVi6y6U6OrK7hldWUhjkGv0R8D6JqvgXwnZWGoeIdW8V3dnAIZdW1e3toLq8cHLSzfZ4YLdeOAI4VHtWsfdpRhHoR7O9WVSXU+Yv8AguDBDH/wSR+PS24VIE8KXe1Io/vAKvIUdvUnIGM1+U//AAZXAj46fG4LwRoem/dIKqDczkD1yQGz6YHrX7Jft5fsJWv7fnwzvvBGt/ET4k+C/CGrWwsNYsfCh0yBNYgaVJEWW4urK4mUAR9IHjVvNxIGFeB/sI/8G8nw0/4Ju/Fe58YfCb4xfHXRNS1KJbXUoLq98P3tpqlsJkmMEscmlkhWaMAtEUkALBXXJNGBiqDqt/bSsVVvOlGm+jbP0Editq5ADEZOCcZ/Gv5Ov+CkWx/+DjDW7ksJD/wszRZOVZGlLtZn7hGRkHPvlcYzX9THxb8P6t4z8MT6LpPirXPBN1qDrEmsaTDYyX1mFkVy8K3sFxbtuH7oiSJuHJUBgDX50fFf/g1D+Cnxj+LusfETxJ8Yf2kdU8bazevql3q76zpKzyXJfzPMAj0xcbWxtVAAgVQgUKoCSft41OiNOb9zOP8AMkj9MoriS30hiZo4mhj/ANa5CRltpZicg7cDnnj2r5y/4KffHvxF8LP2bbnwp4Ou7c/En4uahF4G8ICVzC8F5fLIJblmX7qWlutxcFgT8tuASGILeu/Af4Yaz8GfDM+ka3478XfEMx3KSRap4kh02G9iBjjQQD7DaWsbxgKWBeNpWaVwXIVQPmr9p/8A4I3aT+1V+1D4Z+LniH44fHvSvEngm8+0eGLPRNZ0uHS/Cr5zst4DpkgO75fMefzGkVFWQuiqoVaEeZt63ZnQjyQjGXQ8W8O/8EiP2pPCfwIt/hfYftuWFv8AD+30U+G00d/gdoTIbBoRC0LmWYM6vGCpdiWkI3MdxJr8YP2W9e8Qf8EJf+Cy9jZ+Irp7y08BeIn0LV7qGLCano1wQr3McQZm2SQSxTohyBIFG8YY1/Vj4R8Fap4R8BWuiTeJNc1+/t7Zrb+2b61tYL2eVjlp3WC3itQw7BLcLx905IP58ftOf8Gxfwn/AG0Pi3N45+J3xc+PXiXxZdw28E+otf6DZmZI4iiMIItJWNVHCqoUdyfmJJ0oTlTqqp3vzehXJB06kZbv4fJn6Jf8JX/wkGiRX1ncW82m3KeYs0Z85GQoxBVlGCpXa4YZBJABIYEfylf8EObGW2/4Lx/DuULHEV8VasqtgsC4tboEEFlx1BHT7wFf0UfB/wD4JuX/AOz/APs16Z8KfC/7QPx20/w9pEq2Gn3s91oF5qel2gieNbCK4n0iQLAFkJjwA8TLHFG6RgIPlnwz/wAGhXwB8E+NrbxNo/xb/aY0vxJZXIvbfVrbxLpcN7bzggiVZhp29XyAdwOaMPali3XWzVjNxbw/snufqLc6zeMqGBHYMnmoBFuEi+g5Aye2XXqOtfMv/BPT9vj/AIbx+Nfxy1Pwxrljrfwx8GeILPwx4buLdIjHevDbo95eRuo3SxSyzbY5C5RkhDRg5YnxX4if8G5fhf4r+EdQ0PxX+1X+2r4o0LU4hFfadrHxJgvrO9ReQJYpLErJj3GfQ19Bf8E1P+CZPgX/AIJd/Bm/8AeBNS8Uazo2oa6+vzS+IJbW4uxcyQ28O3zLeGBCqrAj4ZGbLZzkDGcI2nd7Gk/htHc+n6KKKsD+eH4paBrv7ffxr/4Kha74B0+TSfirc6p4R+FPh8jxUmlw3ls2rLpGoQRzzywWzLqK6NAojkIZhJ9lVpTKyz+M+Pf+Ce/x18Ifsg6PovjvwvN4e8Z/Eq88L/s4aJfa18QLfxLpl5C/iC+1SXyxbz3TaeLGbS9KsRbKBFFHHeSFJJ5WMW9L+xH8Sf27db/4Kb+DfhddX0viux+N+mas2gQ3sVlH4nt01rxJGbKeeaeKBIkaVL3MjsrS6eibPMaNx7B4O/4JQfEz/gmN4F/Z9sPHviXwvPH4o/an+GstpoPha81KfRtLktLG8hutQZ75lcXuoF1MyxxrHut4Qh2PHBDE4t7GkZKKsj7o8UaR/wAFPdW0DUrDT7/9iLQ7nUbea3t9WsV8RvdaYzqds0Jnhljdkchl86ORDIfnjdMqflT9srS/+CuH7F/7NPir4m6t+0R8NPF+leDbcXt/p/hfwxZ3+ovbiaKOWRYm0KNdkSyGaVi6iOGGZySFwP2p0FlvYkKwmNGbLYYkvg7SzbgHLhlUEsoI29an8ZeBNF8beFNW0jWdHsda0rWbKWwv9Pu7eO4t9Qt5EKPBJHJmORGRmUq4KkMwPBNOmrRtIzP5cfAX/Bfr9r3x38DfiF4pP7VGiaP4l8C/2fJpPh268FaMk3ia0uJntro2ky6eImlgla1lNsxV3t3nmHFmyN+3n/BwD+0B8Uv2Uv8Agmj4o+Knwg8ex+AfEXgnVdOvLuRNHs9S/teyubpbFrQpdo8cJMt3HL5ux2zalQoErlfxQ/4Lw/8ABvrr37At34i+K/wsWPUfgRIhvruC41ONrvwc0l1DBHbBZmE9zbtPdokciK8qB3jnxs+1XP6O/wDBSa383/gz38OKibnf4X/DlFCqCebrQxgfkOPYVQHuNp+wZ+25c3Eij/goI1u6rkCT4GaAzAFtqkBpdw+ZepUBuMAduS+K/wDwR8/bJ+M/h+y0zWf+Cinim0tLC8jvY20L4VWegXDuiFAkk1jeQyyQlSN0Ls0TFUZkLIpH6G+CPF2m+OtB0vVNC1TS9V0PVIItQ0+/sZVuLK/tJVUxTRSIdjrKrgo65jYAsuciuoNrER/q0/75FTFyfxAnbU/Er9qj/g33/ah8Nfsx/EbVD+3n8e/iY2m+GNSux4RTTdWuH8V+XZS/8S0J/a0hc3IHk7fLfcZPuMTg+J/8FHvBnxY0/wCI/wCx14p+LXx+0jRpfF3hTTb+x8IfE5/EfhnQ/DHiHTdO0uDzrq40doruLUDfale3X22YWotfLkjefy4hEf3V/bP8Y6R8J/2O/ix4p1jwppvjbSPDXg3WNVvvDt8ita69BBYzSyWUoaOQGOZUMbZjcYc5Rvun8Jf2ovj3+0B4K/Ys+E3ge98T31l8MPEXwR8BRfDnw9pXwVs/Huk/ELXJbOEJouoXGoo0UGp/aYjPHFEZke3t4pY4luAsNzcajWjLdRy+I+w/+CfH7RyeKP8Ag5e/ax8OeHrnRPEPgn4heCvDHjGHVLOX7Ws7Wem6XHZmCaOXyTbtDqs0rZRiWEJVvlDS/rPX4af8Ea/DPiHwF/wccfEbRPGem6do/i3RvgH4c0/XrKwt7SC1stQi0jwhFPAkdoBawxpMjKsdv+7UqBHhNmf3LpEBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFAARkVAdOhWHYqmNAchUYqF4xxg8fh9etT0daAKstpGCZNpyozjJwSO5HQn9a8e/a0/af0P9kn4Ux+I9Zt9W1Ga+v7fQ9I0jTljfUdc1O7KpbWVuHZFMskjZLM6oqCRnYBdw9onIihdtpbapOB1NfCn/BZvwzPpll8APiZdSNH4O+DnxX0zX/FN20joNL0wxy2j3x2YbyoZJoy56KrMzZjDAzz2tcaStYwJ/wDgqP8AF2z/AG4tW+Bs3wm+F6a/o3g3/hP7rW7n4oXcOjx2BuUtmCyLojMXSST5cxhQgLMxbNbf7ZX/AAU++LH7HsHwhi1T4NeGtR1b4seIrXwadN/4TyWJNI1a5lkSPdcLpkiy2exY3EwRJCZQphGxifCfj1+xp4y8f/8ABZPXfi34u/Zc8Q/E34Xr4FXwjYI+p+GXu/7ThvPtA1G1FzqkLQkxA7Ji0U675fkUNl9b/gpb8O/jz+11N+z1rGmfs9+I49U+HPxSt/Gep6e/iHw4jQafZzz+XC5bU9hvZoI1udiOYUE/lmcsjbnQgv3ftOkm36dBTjzVaqi9HFJeq3Pur9oT9qXRP2UP2atX+KHxFeLSdE8J6QuoapDA5nKyAJiCMuqBpWmKxRligYupIXt5N8K/23Pjn47+G3h/x3qX7Pq2PhPxbJbS6Va6d4vj1HxDZWt1KvkXOoWLWsMMcYgPmOLS8uZojJGphfErx+T/ALcH7OHxY/4KyfBj4k/CzV/h7r3wW8HP4ftJ/C154g1nR559S8QQ3omKvFptxeNBBGkEabvMOfNkYRMyrj1v9ln4q/G+w+HPgrwN4m+Bl74V8W6La29jr3iKbW9Ln8KSRWx8qWe0FtdPqDedGolghltItpfZI48ssZV3KpHvt6EU6jUKbl21OA+O3/BUr43fBr9pn4U/C69+B3gK11v4zzainh57n4jXaR2cdmqu0l6y6K4iZ1dMRRGb75BkQjNe76b+0R4o0KX4X+HfFHgi10nx147uL5dV0u21tb+30W2tY5GkuxOsCi5BY2qhfLiO66j+VG/dn5n/AOCgnwQ+KXxH/wCCoP7NXxL8IfCDxR4m8DfBd9Vl1nVbLU9Bg+1x30EaILWO51CGXMbDdJ5iRjavBk+6fp/w78P9XvP2xfFnjjxBpSWWk6D4etfD3huWR4PLYTM13qVyF8wyIksotYsOE3GxdjlSr0sNC8Vzb319Ogq6afunif7f3/BVvWv+Cdvxs8NW/jb4anUfg/q93bQ6v430nxC81z4WSf5EnvdP+y7lgNxlEdLklhGc7XeKKXpvi9/wUbb9nvwv8UfHvjnTPCVl8JfAWm2ut6X4l0bX5NVu/EqXYJsoorJraCFTLghCl5IrSPES6Rs7p0PxX8GT/GT9pLVfCHiH4Qa3rfwt8R+FrnRb3XLm50p9IvmlH2loRD9tW82sBtLrb585VO7aDKPkfRf+CD/i2X9hL4+/s63Xjeebwtq+p2t18MtavZllk0eygke5h0+5VV3FYrnzImYDDxz71CH9zHPMnG/XdeiZvVSbjJdFr6nr3jP/AIK9+N/gH8FPAvxa+K3was/CPwc8Z3tnbT6zpvjP+3ta8NW94xW0u76wjsYoTE/7rzFtbu4liMuFjlICm/4z/wCCm3xZh/bl8U/BPwt8KPhvq1xoHhQ+NYNZ1b4lz6baXumNcG3iLKukSeTO0g+4zmLGMXBOSPIP2sv2c/jv/wAFFP2FvA/7OGs/Cmf4b3Ml3pVt4v8AGOpeItMv9GtLbTXTzZNLS0uZL2aafyY2jjuLWBU83Ejo4BHC/tJ/8Ek9Q/bF/wCCgXjWLxj8GvFEfwkvPhjb+BPDnif7dobzeHtRtZ0eDVo4V1AXYg2IUXbbiZwzRvFsZs05Wm30M1C9NVH8b+4+p/2W/wDgpt44/bV/Yu8I/Fb4Z/CPS2TWJr231208UeNTpVjohs96Syw3VrY3j3cUkqmOMpAm7DMVQCuX/Yn/AOCnnxs/bf8A2doPih4Y+BfgfT/D18uoxWqXHxInSa3uLOSVWS4iGkoqrJJC65hluHB8oOiq7Srt/sL+Mf2jfBv7FWreFPjR8Odd1rx/4G0+TTLLV9K1nSLy38d22TFbXEDSXkbRyvHh3+0+VuIVh87tAvC/8EifgX8Wf2M/+CX9/wDD3xp8H/FUfjLw1NetbaNFquhXDeIRd3VzKqW0635gXylkUt57REEcB8ClVjCPtLdIpr5ip1L8kuspOP3bmBb/APBe74gf8OxLz9qdvgX4Yk8GWWof2fJpQ+Id09++2+FhJIMaL5GftDAKvnBWTLAhhsP1/e/tAfEq1/Ywb4gW3w78O6749i0f+2h4V07xJM1re4G9UivpLFG3tFgr/ohXzMIW2Ezr+Ri/8EcviNff8EWb74Xzfsn2c37Rk+rNJD4va48JvOto9954I1T7a1wYxb5h2KNxYhAnlkuP2V/Zqk1Q/A/wlb694X1bwtq2l6TbWsmlalJaPd20qI0PzNbTT26hjH+7MVw2Q4zg/LVYiScLx35l+Wv4hK8aqS1V/wAOh8WaJ/wXz8Ra9+zL8C/izbfCvwZ/wivxs8UJ4SuGuPiDMsvha/e4mh2TINJ3ywgQsWlRfkOTho9ksne/t7/8FU/Hn7C3we+IXxBPwr8I+L/Avw71y00KS5j8d3Vrq1+8qW26UW39kvCrLJcRBgty+B5nIZGWvnbWf+CK/j5PgB+2L8L9JtIIvDOv65J4s+EGm2t7BHJFfzpBeOAzS4ghFxZwQxGURENHM5JBU13f/BUP9i/4z/GL/gkPo3wi8N+Cb34g/FLXpbTWfEN/Y3unWFlb6gLtb6+kea5uojGjzTTiFYlmIRRv8sHcKkouEZea08upoopVbdLfifTdz+2f44+Ffw+8feMfif8ADzwTp/hrwH4dutbmvfBPjxvEE8s9siSS2JinsrFo5GidXViSDkAgfIXwf2fP+Cg3xC/aK+Eun+J/C/wo8L+Jxregy69ol/oHjuK+0G5k8y1jGm3l41nHNZX6+dI80LWrqqwgRyTSebDDyngrSvG3gP4L+JvBHwl/Zhn+CsN74f1DUkOu3nhtNIvNdaG2t4LKOzsdQuI0WZRLLJIyoiJAw8svcBl4b9hf/gntd/s/f8FBLvx/8NPhfrP7P/gG/wDDd1pvjfw4/iG11DQvF+qNdrJbXWnwQTStFFGGuGSaWK1byniijtYRLMyxKHM6lNvlW6sZU1enF1DrfhV/wVR+NvxY/ap+J/wh0/4D+ALrxL8IE06XW5Lf4pzsuoper5kZsd+iRiV1iDMVn+zrlcbsc034df8ABVD4y/Fr9pb4x/C7Tvgx8M7PV/g5eafpuqzaj8Wby0i1GbUEkks0tGXQ2YvIkZUCQJl3jCk53VjfsKfBT4qeAP8AgrN+0P8AEzxR8IvFnhnwR8XINKj0bVLzWtCuBD/Z9q8T+fFbX8sqeYxBjMay44D+UM48q+BP7LHj3wh/wU1+PXxu8cfsleNPFWmeMtX0fWPA1/FqXhSTVvD8tjCY55AJdYQ2wmYKf3cx3oNsiquVp0bSnS9p1i7+vQTly06vJ0eh9G/G3/gpB8U/hp+1x8L/AIQaZ8HfAuo658W9JvtQtZr/AMfT266TNZWaXF9aXZt9KulDrIWRHi84SFOVj3DGP8JP+CuHjX43/s1/FjxP4Y+DljdeOvg14uu/C2saBJ413afq0lu2Cmn6olk3nuS0XySW8Sr5vzSJgZ8g/bq/Y7+In/BQP9tL9nvX/iP+zzr2pfD7Q/C+q6V42sl1nQpo9PvtUtY8R2pl1KKWR7FwS1ykaZ+yiSAShkStb9mDwP8AtKfsefsz+K/gH4i+EvjH4waL4R1VbP4e+KdH17QF+26IskMlta3sF3qFvPDJbr+7wqlViwo2pDGzkYN0pQlu3oWpKNVS6JanrP7N3/BX/Uf2yfgXoXiH4deBtBuPFNt4hj8MeOPCPiXxbJpGpeD7x5fI8t1jsJ2uRuDNuSOL5ccErIqdh/wVL/4KFar/AME1P2Wbj4n6d4J0n4gWOj3MFrq9qviX+y7myEzxQKYAtpcKzCSTLK5jVVG7LH5K8v8Aiv8A8E2dWu/2/vhh+0d8OJr3wdfa1eWsHxQ8NXE0EaeILOOPzYLqURuYTe29wsSSSRNIskYLxuSjrcXv+C937OvxO/bK/wCCfurfDn4beBdb8X+KPEGpWE8ZtdS0+ytLJba4hmZ5mu7iIoGCnyxCJHLY3qq1c1ySjGO10FJXknP7Suje8Ef8FT/EK/tR6b8EviT4D0XwL468faBLrfw+vtO8Uz6r4c8VkRSSPaveNaW89rLGqE8253qrOuSYo5eH+B//AAW+PiT4O/Hj4j/E34d6L8OvCnwD1i88N62NP8VPr99qGqwShBDbW72FmjxTsyeXI86neQGRFJeufuv2Svin+1B+3r8Gfi/4x8Aa14B8K/s3+G7n+zPDt/qelza/4y1R7TYxSO3u5bWC3GUAlmuc+YixlERnmXx34Qf8Ep/i1+0b+yn+1f8ACb4h+Btb+FV98YPHt3478K67f6lpOo2ts4uLaW3tpxYXs8kcpMZMiiMxhCSJGYbazpQglVv0dl94nG9HmnufTvjP/grV41+A3wW8C/Fz4ufBaz8H/B3xpdWUNxrWm+NzrGteGbe8LC0u76zFjDbLE/7rzRbXkskXm4CSEBT3vj7/AIKN6zqH7WSfBb4Q+CrL4oeNtM0eLxL4iu73XRoGgeH7CdC1sstwsF1K13MTEyQLC+Y5VmMkaBlr50/a0/Zu+PP/AAUU/YZ8D/s5ax8JpvhrfvcaXa+L/GmpeItMvdHtbTTXTzZNMW0uZL6aWfyY2jjubWFE83Ejq4BHT+Cf2KfHf7Bf/BSzxr8Yvhz4I1H4sfDf4reH7DSNU0rTdT0yz8SeHb2ygSKKeNdQuLa2mtmSHy2DTRzJI7YjaOLDONOPtPIzafLdfF+B1n7bf/BWv4l/sUfs3eI/iRr/AMCEax8IajZaZqlrceLfsy6h54jUXmkzxWcy3Vozs20z/ZZlRC0kUbN5aemz/tp+O/hJ8O/iF4q+JngDwVpvhnwH4cu9cmuPBfjo+I5ZZrZFkksjBPY2LxuYnV1Yhlw2DtGzf8//APBZT4SfG/8Abx/4J5654I8MfBfVW8T+LNYtb200yHX9Jjm0a0tpLScvqby3qRJNI4mCpYteIu1HMpYlT2/w/wBD8a+Dfgn4n8C/CT9l+b4JQ3vh7UNTU65ceHE0e714wW0ENmllp9/cIqzKJZZJHSJFS3YFN9wGXDnvhqn8yeh0Ts6kJLZbm/8AAz/go344/aD+D0fjLQPh34G1e0v/AA/c+ItL1fS/iE2o+Gz5Zt1bTbnUItP+0Wt8qvLI9v8AY3VTGqiVm3pHkf8ABP7/AIKTfEv9vL4f+AfiDpPwj8CaV4R8ZahcJNbt8SWuPEdhZQXsllNeNZvpMUcixyqNyLc7grx/MWYofEvgb/wTl139mP8Aap8bfET4S/Azxx8LPDOqeCr/AErxN4O07xLpV9YeN9amuAbeXSLd71Ugt7YNM4ku2sdkbrHHbL5s0Yh/4I1fs2fE7/gnt+zN4L8M6l+yhq9h8WzO2h6341s9e8MCwvdPn1gz+fcXEOotdXH2WycMiNau7GDyU8tWLHsjy8809nFJevUzn8Gm99fQ/V9reN1IKIQeCCOtNFlEE2hAOMZ7/n1qKG4kkQZZdwOG29jjkVMztuXHrzxUijNMb9gh85ZDEjSLnaxGWXJycE9OtSkYFLRQWV0sUR9yqyncXOGIDEjHPr+NSNCJFKsisrDBBAIIqSigCBNOhjZ2VArOACQSCAOgB7D2HHX1pW06F9u6NG2jA3DJ/X/JqailYCNLOKMfKij371Guk26MSsQQkknaSNxPrjr7enarFFMCE6dC1yJvLXzQMbhwSPQ+o9j3561IIlB+6v5U6igBssSzxsjqro4KsrDIYHqCKiGnQrJvCYfj5gTng9M+nH49KnooAKKKKAPyV/4Janwz+zX/AMF/f25vhVZtrk+peMRpPxDju5xBNHamRftd9E0gEbIPP8QJ5I8o/uoiJH/cky+E/tp/sar8AP8Ag2of9nHx1/wj+sfHn4MC68cWWg6HrPn30Vgvii5gm1yO3iYSPpx069vN0s6FI0dnlSKWJ1h9p/4K5eK9W/YR/wCC137J37Skuo6lbeAPFUMvwo8VJeXMmk6LpkMtxO0dxe35LRCJW1JrtbaZEH/ElLq+F8yD50/bCtPjf47/AGvfBPxa/adsPAH7O+ifEVtU+CUfg7wpqeoeJfH3i3w1qyPD9nFnZXE1rdvaHUGlWfbG8dwBI1ndyLY2cwB+6Pw68T6d440XS9T0nUtL1rRdUtLe/wBOv9PuY7m01CCUGaGeF42ZTCyMWj5K42lWbkjsHQSKVYBlYYIIyCK+CP8AggR8fbvxb+x5cfB/xJrenar8Sf2ZPEN98MvEyw+UFC6fcSW9jLbxoFf7J9kjjgSWWOJ5JbS4yp8tmb72iYsOalSV7Afn5/wdD2yRf8EMfjmy7gX/ALBJ+Y4JPiDTO3/AR9PxNeEf8FKAD/wZ6eHAQhB+F/w4yHJCn/S9E645x9K9/wD+Dn+EXP8AwQ5+NkbNsWRvD6luOAfEGm88kD8yB7ivx8/4Kaf8FvfCXxb/AOCRXwH/AGYfhrf3+ptb+DvDUHxCvptONtaW7adawKdJRbiIPLIl9DHM88UiRhLaNFecTS+XQHB/8G2X/BTLxZ+xr+3f4K+HUl/quq/Cj4x+JrbRNQ0Yxi4Nlqd3+4stRhJlRYJzMYY5yC2+18wNHI8cAX+rqcFoXAZkJU4ZQCy+4BB5/A1/LT/wa7f8E0b/APa4/bs0T4sa9pGoN8M/g/dJqy3zxt5Gp65E6NYWiyrKjtJExS9coHRVs40miVbqMn+pO9fy7KZtyJtRjuf7q8dT7UAfL/8AwVq+I9h4T/4JeftA3Ws6zpOmW9/8OtbsIJLyeGG0kubnTp4ILfzJCFkeaZ44441JdnlVQDkCvmT9nT4yxeCPFP7D/wAPf+GhfhD4QHwd0M+E/if8O5/iDY/27q+u/wBg2+lWdglvA8jXEltfPMGgLhdxjf8AeSRoEk/4L1fGnw74g0PwX8HfFd1p9n8Nrmyufib8Xo11OybUf+ET0S5tHtdLEEk9vJv1bVJbOzglt7iNxNG6RiVTKyfnX+wJ8Tbn47f8FSvEv7Snxk8b3Hxftvgp4FuvHMuh+O/Cd1o/jBNDstItr6z1XSdOtt2irturqEwBrwq4uZr5Y4pJI50znTjJ3YH37/wTy+GmkfEn/g5D/bP+Kmk+JNNvLbwhofh7wY9nZCK4hkuLmzs/PQzo+2N7WXRGhkhCsfNkZdyNC6t+qdfmF/wa/fBLxZ4Y/Yo174s/ETTrKPx3+0N4yvfHV1qY0v7BfalaSqEgM6CCFTE8v2q6t44t8CxXolTb5hFfp7WgBRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAN0NU7jTIZkOU5C7RgkYHoPT09xx0q51pNgPaolFt3Qmr6HO+E/BOj+CvD1npWj6bZaNpun28dhaWthELaG1t41CRwxqmAiIgCqq4CgALgVpyaTZmVSLeENhkJ2DJVmDMv0JAJFXDaRn+HvnqaDapuBxyPer31kTCFlyvYjj0qCOAR7WZQoQ7nZiw9yTk9TyfWiTSreUsWiU7+oPIPrx79/XvVgDFGOaXmVboZ9vpcH2vcY8suOSxJJGME56tgAZPOB1pYtPhB2KnlpEMIEJQKMYHT0HT07VdWJVYkDk0CFVYkDk9aGroe+5nDRLMSIwt41KPvAUYG7JOSBwfmO7n+LDdQDUsVhBDKHSKNXCCPcBg7QcgfQEn8z61b+zp6UCBQc4rL2bFqVntIpVZXRXVxhgwyGHv60i2UYZThhtGAA5wPfGevv1q35S+lHlKO1W4XjYeuxUXSbdGBESAqMLx931x6Z7+vell02CWORZEDxyLh1blXHuOh/GrewelBjBGCOtHLrr1/Ijktt0KC6bbRqNsSoy7sMhKsNzBm5HPzMAT645zUgtIogNqKnU/Lxye/19+tWTboe360phUgcdKzUJXv/AFfoXstNzPXSYIFG1CFTlVLEqvJPAJwPvHp246ACnfYYLiRHaNWdAVBPoRgj6HA46cD0FXWgR1II4PvSLbIgwFwPqa05ftdSORblRdLgiLMqhHbqyEqxwcjkc+v5n1NI1rC8iMVBZG3KSckEnP8An61c+zJ6H8zQLVAfu/rRyX917Byu1ioY44mzyD2+Y8fSkNlBOBujB2sXX/ZbHUeh+lXHtI3OSufxNC2yL0X9aOVvV7rYIx5dF13Ki6VbCcSeTH5gJIYj5hltxwfQnnHSg6XbTeYjxl0l++rsWEnUcgnng9/QegxdMKntSLAinIHP1qy+hXi0+GObKxqmGL4UYBY8k47nk8+9JHpkEMARIwqqMDBxjjHX6Vb2DOcUgiUZ460bbC1er6FT7DCxP7pPnfzG4+83GGPvwOfYUraZbsVPlqCq7QQcHA6DPpz09eateUvpQYlPasowcZe7s9xv3viKlxaxsxyCcrtOSeR7+tFnp0MIOxNoOAAGOEwNuFHReAOmKtGBT2pViVegrXp5laFY6NarEqCCMRIoRY8fIq4xtC9AMdulKul20b70iVHyDleDx06duv4EjuatYpNoxWap2fkSULnTbd595iXOwoccAg+o6HqcHqNzYxk5lS1hcHcpYli2WYkg8/4kfSrRRT1AP4UBFHQAfhQoPmuBGLZSwbGCDnjipQMCiitBKKQUUUUDCiiigAooooAKKQjOOSP60YOetAmxaKKKBhRRRQAUUUUAfHH/AAWc/YjuP+Cgf/BPvxx8NtAtrSXxgbRNX8KzfZbd501CyKTwJbtNJCsMtzGk9p9oVwsUd5LuDKXB/H/9nf4K/szf8FY/2GNQ/aS/ak+OHxh8P+Ifg9baX4K8Yf2drMV9b6bbIsFnp91Bavb3uoCK9EgeV2BE9+1/KAI1IH9Ht5psb2M6KuPMjKnvn5cdCCDx2IP0Nfir/wAFtv2Y4v2Afj4/7Q/h7QbbW/2fvitd2HhX9oHwDZ6bcSw62Jp5XXXwPPWKG6yf3N2sts8WopbSedJJeXOBAecfCf8AbR1T9lf9p7wx8VtQn8eeDfGfwmksfhr+1D4K8aa1aahr99o7Xi22h+KDNZ6dGdcEH263ia9jjN08UVpEGkiu2up/3M8F/ES08e+HtJ1jRdX0vVtJ1u1F9ZXdndQ3FvdwSxrJBJDJGWWVJI2DpIhZWU528/L/ADzeCdB8HfsPXviXU/2f5PDPg/8AZy1S601fEfx4+LlgvitPiFoWoQwNJ4W0zS4tOR5rcY1L7XY2yLceZZMLq5shbmJffv2Nv+Cgngb/AIJv+LvEGqfCjxprHxi/YW1DxEmmarZm0v5fEPwB1e8mLL5trcRLeS6LdS7lEoV90yygGS7Vxe5R/iWsB7p/wdzfG7Vvh9/wSMudBs7a1uLT4h+LdJ8P6m08e5o4USbVFMLLINkol06IZIkXy2lBQbkkX8wv+Cdf/BDfw38Z/wDgk38av2ofiPda9Jc6L4d8U6p4H0jT76G3066aw066VdSuXiZ5GxeowjgDQMGsSz+fBcBV/cr/AIK76d4O/aR/4JC/Haa4udN8T+Fr34eal4m0m6stQL293Pa2I1LTrmGWBsTJvggnUq7I6IdwaN2Wvkn/AIJqQtff8GfOuStukuG+FnxGUMSxJzc63xxyR049h6CtQPvL/glZGif8EtP2b5Cq5X4WeF5skgBW/se35GeB/IV6J8eP2lvCn7M/wn13xv458R6f4a8LeFLQ3mr6hebljtogyANgIWZ3ZgkcKqZJmdVjDOyBvkj/AIIgf8FCvhn8fv8AgmF8PzoXi7SNKT4IeDtO8O+N4dbnjtJfD507Tlie8uMtiOzkS3kmjnZlRkyC6PDLFXxJ/wAFcP22bP8AaUl+FXj7Xvh1q3izwTqmrz2/7P3wt1ayugPjNrbm3iHiXWrXeI10q3EsCWdkdt3ffbJHYxWt08ZARxf7SP7VGq+GvBnxbl+Of7Pd38QPiv8AHv4daj4x8UQeJvGGl+Hbj4OfD9tTNloujWk15DKr3sV6qXexLO3na8uLNpIp7mM+Xh/tGfDDwH8Z/wDhW/7MX7P3j/4meOPiH+27ceHPGvxQ8U+P9dg1/wAT6H4btbKLUNMt72NxFB50UEjXvlw3C3PlWSIWliuYCOt/aY/Ym8Y/sD2fgP4o/ti+Ifhh+1B8PfAvhi4ePUfGVxqNlr519PNurXw1bTozz6xb3d68ciyalBdwJb212Gt7JFcXH1//AMEI/wBgjxt4S8YeOf2rPjvo11Y/Hj483k8q2l1qk7x+HNBlmgnisWt5mZoJTJHFtgkkuHghtbWEtBIJ0qOdDas7H3/8CvhxoPwg+Ffhfwh4XsE0rw94U0600bS7ISNci3tbaMQwRGVmcybII0wzSO425ZnOSfQKiFnGJN+DuyDnccnGf8TUtWIKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACg8CijrQAUUDpRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRTXDbuDxSbsrgOoqNmYUiTlmxQnd2J5iWiq7TuGPPf0oqrBzlgjIx61keLfAWjeOvC2qaLrOlabrGk61aT2F/Y6hbLdWt9bzqVlhmifKyROpKsjAqVOCMVr0kiCRGVgGVhgg8g0ij+eX/gqp+yhon/BPBIfgH8Rbrxlc/sQfE/V11vwX4nSOTUpvgb4qH2otCiiYvfWJjkmklsp9sk0NzJLbyS3kFxNP8yaz8Q5/+CTfwe0r4kaVqN98WvjD+0JOL3UvGusWdvrvw58c+E5Gl/tPRDJdt9puLuS6W3GordRWt1A0RiQxpukuv6mvGHgnRfFXgzV9I12xstV0PVLCWx1Gz1FFurW7tXjKSxSxy7keNkJDK4KsCd2cmvxl/bx/4IZa5+zH4W1WX4GeFtP+J37OUt5beJvG/wACtf1K8ee8v7SOO3NxoF9ua5t7lrd3LbLmKd3txGXv4nWxTJ1GnqgPmz9i74w+O/iB/wAE3fiD4M+BF14n1SHxV8N/E11q3wE8Q6ZNexQafdvPYXXiDwhrWxnmtILyWDGlTSyzGSPUVL3N0UuJfQv2Q/8AgqF8J/gL/wAEDdS/ZR1G78W3n7Q19oXizwDF8OYPB2pyawNW1TU9St7e1ceUIw6/ao3liEjSAJKiK0y+SfE9W/ZO8DftZroHxN/Zm8efEG1/ba8GeK9Ov9W+GXxK1LSLDU9IudPM6SWOmWz2FpbzmxayhdYFCxW9hZlJYIv3FsP2+/Zb/wCCdei+Df2SNC8BfFn7P8V/Ht34Fm8DeN/Fut3cmpa5q1pePJcXmn/2oyxXh05J7mRYEYq2zyThGTfWt0B/Pz8I/Dkf7EP7M+q+LvH/APwjXiXU/B+hWFvcfBDQNSnaHU7sa7qstlqnxDSNo4pV028kWE2g3SLJJpVncrYtIon9a1nS/Dv/AAUy/YJ1j9sn4qfHn4lfBPxr8PPiLd2WlWOnandeJNIS9+z21+sPhzTLq5jubG+uHkt32JfSQxCznf8A0eBVS081179iz4Tf8Erf25NalufHb/HLxHYeK10n4f8Awt+H9+11qGvQy3s9nNpniW+/s2W1XNlFLZXWlW8c893JfGPZDE2+T9Cv+Cfv/BFHxp+2P4p+HPxl/a/tfD+leHfDc1ze+D/gTpvhy30bQfDa3c4uohdW0YWMBmkleS1kV7mURWa3k8pSW2UY07O5hf8ABOT9mfxZ/wAF/v2lI/2rP2k7JLf4XeDNRfTfhz8Lmjnj02REeKR7uXzEAubcSbFkkXd9tuIHjkMNvbC2f9tbTToLKBEhjWJIhtRV4CD0A7D26cD0FFvpFtaIFigjjCsGAUY2kDAx6YHH046VZAAFZcjCTvK4UUUVqIKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooYB1quLJLdP3aklVwuWLHp0yT7D8eetFFTF3WoHyN/wUZ/4JO/BP/gqT4RGm+PfDf2fX7G3t/sXi3SVitfENjbRzvIIkunjkDwlmuFaGaORM3DSKvmKrD4o1X/gkB+3xrt9p3wv/AOG5tTj+E2iXL3kHiC1N5aeNWnMEsm2Yx4luI/tMsyCKbVnQII5NwZI4VKKlQQH2x/wTc/4I7fBL/gmD57/DnRL2fxbqtktjq3ijW746hrGoWKzySQxeYqRRQLlolKQRRqy2sG8O0SSj65h0yC2H7uMR85+Qle4Pb3H8/U0UVoBPRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQB//Z');

-- --------------------------------------------------------

--
-- Table structure for table `payment_modes`
--

DROP TABLE IF EXISTS `payment_modes`;
CREATE TABLE IF NOT EXISTS `payment_modes` (
`id` int(11) NOT NULL,
  `name` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `paymentmodecode` int(3) NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `sponsor_id` int(11) NOT NULL,
  `status` int(1) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) NOT NULL,
  `modifiedon` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=7 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `payment_modes`
--

INSERT INTO `payment_modes` (`id`, `name`, `paymentmodecode`, `description`, `sponsor_id`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 'Instant Cash', 301, 'Instant payment', 201, 1, 1, '2016-03-11 12:04:06', 0, '0000-00-00 00:00:00'),
(2, 'Prepaid Bill', 302, 'All payments are done before hand', 201, 1, 1, '2016-03-16 08:11:22', 0, '0000-00-00 00:00:00'),
(3, 'Postpaid Bill', 303, 'All payments are done at the end of all services', 201, 1, 1, '2016-03-16 08:12:08', 0, '0000-00-00 00:00:00'),
(4, 'Mpesa', 304, 'Payments are done through the M-pesa', 201, 1, 1, '2016-03-16 08:13:16', 0, '0000-00-00 00:00:00'),
(5, 'Package', 305, 'Pre defined payments for some of the services for a predefined fixed amount', 201, 1, 1, '2016-03-16 08:16:25', 0, '0000-00-00 00:00:00'),
(6, 'Credit Bill', 306, '', 202, 1, 1, '2016-03-18 10:05:53', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `sponsors`
--

DROP TABLE IF EXISTS `sponsors`;
CREATE TABLE IF NOT EXISTS `sponsors` (
`id` int(11) NOT NULL,
  `sponsorcode` int(3) NOT NULL,
  `shortname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `fullname` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `status` int(1) NOT NULL DEFAULT '1',
  `createdby` int(11) NOT NULL,
  `createdon` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `modifiedby` int(11) NOT NULL,
  `modifiedon` datetime NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=3 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sponsors`
--

INSERT INTO `sponsors` (`id`, `sponsorcode`, `shortname`, `fullname`, `description`, `status`, `createdby`, `createdon`, `modifiedby`, `modifiedon`) VALUES
(1, 201, 'Private', 'Private', 'Private sponsor', 1, 1, '2016-03-11 11:03:56', 0, '0000-00-00 00:00:00'),
(2, 202, 'NHIF', 'National Health Insurance Fund', '', 1, 1, '2016-03-18 10:03:39', 0, '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
CREATE TABLE IF NOT EXISTS `users` (
`id` int(11) unsigned NOT NULL,
  `first_name` varchar(50) NOT NULL,
  `middle_name` varchar(50) DEFAULT NULL,
  `last_name` varchar(50) NOT NULL,
  `username` varchar(100) NOT NULL,
  `email` varchar(100) DEFAULT NULL,
  `msisdn` varchar(20) NOT NULL,
  `password` varchar(40) NOT NULL,
  `ip_address` int(10) unsigned NOT NULL,
  `salt` varchar(40) DEFAULT NULL,
  `activation_code` varchar(40) DEFAULT NULL,
  `forgotten_password_code` varchar(40) DEFAULT NULL,
  `remember_code` varchar(40) DEFAULT NULL,
  `last_login` int(11) unsigned DEFAULT NULL,
  `login_status` int(11) NOT NULL DEFAULT '0' COMMENT '1=logged in,0=logged out',
  `active` int(1) NOT NULL DEFAULT '1',
  `photo` varchar(255) DEFAULT NULL,
  `createdon` datetime DEFAULT NULL,
  `createdby` int(11) NOT NULL,
  `modifiedon` datetime NOT NULL,
  `modifiedby` int(11) NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=209 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `first_name`, `middle_name`, `last_name`, `username`, `email`, `msisdn`, `password`, `ip_address`, `salt`, `activation_code`, `forgotten_password_code`, `remember_code`, `last_login`, `login_status`, `active`, `photo`, `createdon`, `createdby`, `modifiedon`, `modifiedby`) VALUES
(1, 'Emmanuel', NULL, 'Mfikwa', 'manuel', 'kemixenock@gmail.com', '0719186759', '5aa71ea18d28e45658a1873fbb8875e34a3743c7', 0, '0', NULL, NULL, NULL, 1467827889, 0, 1, NULL, '0000-00-00 00:00:00', 0, '2016-03-11 12:33:38', 1),
(208, 'User', NULL, 'Allsee', 'portal', 'emmmanuel.mfikwa@gmobile.co.tz', '255719186759', '4bf8a46fe3ae78da0d7b45769e96701490f70d44', 0, '0', NULL, NULL, NULL, 1467827937, 0, 1, NULL, '2016-07-05 10:30:35', 1, '0000-00-00 00:00:00', 0);

-- --------------------------------------------------------

--
-- Table structure for table `users_groups`
--

DROP TABLE IF EXISTS `users_groups`;
CREATE TABLE IF NOT EXISTS `users_groups` (
`id` mediumint(8) unsigned NOT NULL,
  `user_id` mediumint(8) unsigned NOT NULL,
  `group_id` mediumint(8) unsigned NOT NULL
) ENGINE=MyISAM AUTO_INCREMENT=210 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `users_groups`
--

INSERT INTO `users_groups` (`id`, `user_id`, `group_id`) VALUES
(1, 1, 1),
(209, 208, 2);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `api_credentials`
--
ALTER TABLE `api_credentials`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `api_requests`
--
ALTER TABLE `api_requests`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `api_response_codes`
--
ALTER TABLE `api_response_codes`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bills`
--
ALTER TABLE `bills`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `transactionid` (`transactionid`);

--
-- Indexes for table `groups`
--
ALTER TABLE `groups`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `institutions`
--
ALTER TABLE `institutions`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `code` (`code`);

--
-- Indexes for table `payment_modes`
--
ALTER TABLE `payment_modes`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `paymentmodecode` (`paymentmodecode`);

--
-- Indexes for table `sponsors`
--
ALTER TABLE `sponsors`
 ADD PRIMARY KEY (`id`), ADD UNIQUE KEY `sponsorcode` (`sponsorcode`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
 ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users_groups`
--
ALTER TABLE `users_groups`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `api_credentials`
--
ALTER TABLE `api_credentials`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `api_requests`
--
ALTER TABLE `api_requests`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `api_response_codes`
--
ALTER TABLE `api_response_codes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=9;
--
-- AUTO_INCREMENT for table `bills`
--
ALTER TABLE `bills`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `groups`
--
ALTER TABLE `groups`
MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=14;
--
-- AUTO_INCREMENT for table `institutions`
--
ALTER TABLE `institutions`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `payment_modes`
--
ALTER TABLE `payment_modes`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT for table `sponsors`
--
ALTER TABLE `sponsors`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
MODIFY `id` int(11) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=209;
--
-- AUTO_INCREMENT for table `users_groups`
--
ALTER TABLE `users_groups`
MODIFY `id` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=210;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
